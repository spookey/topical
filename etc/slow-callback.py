#!/usr/bin/env python3

from time import sleep
from flask import Flask


APP = Flask(__name__)


@APP.route("/wait", methods=["GET", "POST", "PUT"])
def wait():
    sleep(5)

    return "ok"


if __name__ == "__main__":
    APP.run(debug=True, threaded=True)
