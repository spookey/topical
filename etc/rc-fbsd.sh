#!/bin/sh
#
# PROVIDE: topical
# REQUIRE: LOGIN mysql
# KEYWORD: shutdown
#

. /etc/rc.subr

name=topical
rcvar=topical_enable

load_rc_config ${name}

: ${topical_enable:="NO"}
: ${topical_user:="duke"}
: ${topical_group:="duke"}
: ${topical_base_folder:="/usr/home/${topical_user}/run/${name}"}
: ${topical_application_jar:="${topical_base_folder}/${name}.jar"}
: ${topical_application_conf:="file:${topical_base_folder}/application.yaml"}
: ${topical_application_profiles:=""}
: ${topical_logging_conf:="classpath:/logback-spring.xml"}
: ${topical_logging_file:="/var/log/${topical_user}/${name}.log"}

logfile="/var/log/${name}.log"
pidfile="/var/run/${name}.pid"

procname="/usr/local/openjdk21/bin/java"
command="/usr/sbin/daemon"
command_args="\
  -f -T ${name} -t ${name} -o ${logfile} -p ${pidfile} \
  ${procname} -server \
  -Djava.net.preferIPv4Stack=false \
  -Djava.net.preferIPv6Addresses=true \
  -Dlogging.file.name=${topical_logging_file} \
  -Dlogging.config=${topical_logging_conf} \
  -Dspring.profiles.active=${topical_application_profiles} \
  -jar ${topical_application_jar} \
  --spring.config.additional-location=${topical_application_conf} \
"

extra_commands="reload"
start_precmd="topical_start_precmd"

topical_start_precmd() {
  install -o ${topical_user} -g ${topical_group} -m 644 -- /dev/null ${logfile}
  install -o ${topical_user} -g ${topical_group} -m 600 -- /dev/null ${pidfile}
}

run_rc_command "$1"
