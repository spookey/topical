# topical

This service deals with `MQTT` messages.

It's primary goal is to listen on (configurable) topics,
collect messages and store them inside a database.

Later, they are accessible through some `REST` interface.

Further, the `REST` interface can be used to send `MQTT` messages,
as well as `HTTP` callbacks can be configured if messages arrive at
a specific topic.

Apart from writing a backlog of messages, this service can also be
used as a bridge between `HTTP` and `MQTT` worlds.

## Configuration

- It is possible to send *Birth-*, *Will-* and regular
  *Status-Report-Messages* via `MQTT`.

- Old messages can be automatically deleted from the database after
  a certain amount of days.

- Pagination sizes for the `REST` interface can be configured.

## Database

Bootstrap database with `mysql -u root -p`:

Create schema:

```sql
CREATE SCHEMA `topical` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin;
```

Create user:

```sql
CREATE USER `topical`@`localhost` IDENTIFIED BY 'very-secure';
```

Grant rights:

```sql
GRANT ALL PRIVILEGES ON `topical` . * TO `topical`@`localhost`;

FLUSH PRIVILEGES;
```
