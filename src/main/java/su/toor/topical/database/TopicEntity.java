package su.toor.topical.database;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Index;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import org.springframework.core.style.ToStringCreator;
import su.toor.topical.shared.misc.Require;

@Entity
@Table(
        name = "topic",
        indexes = {
            @Index(name = "idx_uuid", columnList = "uuid", unique = true),
            @Index(name = "idx_name", columnList = "name", unique = true)
        })
public class TopicEntity extends AbstractEntity<TopicEntity> {

    @Column(name = "name", updatable = false, nullable = false)
    private String name;

    public String getName() {
        return name;
    }

    @Transient
    public static TopicEntity with(final String name) {
        final var entity = new TopicEntity();
        entity.name = Require.validMessageTopic(name);
        entity.update();
        return entity;
    }

    @Override
    protected TopicEntity get() {
        return this;
    }

    @Override
    @Transient
    protected ToStringCreator toStringCreator() {
        return super.toStringCreator().append("name", getName());
    }
}
