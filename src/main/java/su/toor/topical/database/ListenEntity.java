package su.toor.topical.database;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Index;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import org.springframework.core.style.ToStringCreator;
import su.toor.topical.shared.Qos;
import su.toor.topical.shared.misc.Require;

@Entity
@Table(
        name = "listen",
        indexes = {
            @Index(name = "idx_uuid", columnList = "uuid", unique = true),
            @Index(name = "idx_filter", columnList = "filter"),
        })
public class ListenEntity extends AbstractEntity<ListenEntity> {

    @Column(name = "filter", nullable = false)
    private String filter;

    @Column(name = "qos", nullable = false)
    @Enumerated(EnumType.ORDINAL)
    private Qos qos;

    @Column(name = "enabled", nullable = false)
    private Boolean enabled;

    @Transient
    public static ListenEntity with(final String filter, final Qos qos, final Boolean enabled) {
        final var entity = new ListenEntity();
        return entity.setFilter(filter).setQos(qos).setEnabled(enabled);
    }

    @Override
    protected ListenEntity get() {
        return this;
    }

    public String getFilter() {
        return filter;
    }

    public ListenEntity setFilter(final String filter) {
        this.filter = Require.validListenTopic(filter);
        return update();
    }

    public Qos getQos() {
        return qos;
    }

    public ListenEntity setQos(final Qos qos) {
        this.qos = Require.notNull("qos", qos);
        return update();
    }

    public Boolean isEnabled() {
        return enabled;
    }

    public ListenEntity setEnabled(final Boolean enabled) {
        this.enabled = Require.notNull("enabled", enabled);
        return update();
    }

    @Override
    @Transient
    protected ToStringCreator toStringCreator() {
        return super.toStringCreator()
                .append("filter", getFilter())
                .append("qos", getQos())
                .append("enabled", isEnabled());
    }
}
