package su.toor.topical.database;

import java.util.Optional;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ListenRepository extends JpaRepository<ListenEntity, Long> {

    Optional<ListenEntity> findByUuid(final UUID uuid);
}
