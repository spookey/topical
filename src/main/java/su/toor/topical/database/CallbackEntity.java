package su.toor.topical.database;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.ForeignKey;
import jakarta.persistence.Index;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedEntityGraph;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import java.net.URL;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.core.style.ToStringCreator;
import org.springframework.http.HttpMethod;
import su.toor.topical.shared.misc.Require;

@Entity
@Table(
        name = "callback",
        indexes = {@Index(name = "idx_uuid", columnList = "uuid", unique = true)})
@NamedEntityGraph(name = "CallbackEntity.topicEntity")
public class CallbackEntity extends AbstractEntity<CallbackEntity> {

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "topic_id", nullable = false, foreignKey = @ForeignKey(name = "fk_callback_topic_id"))
    @OnDelete(action = OnDeleteAction.CASCADE)
    private TopicEntity topicEntity;

    @Column(name = "method", nullable = false)
    private HttpMethod method;

    @Column(name = "url", nullable = false)
    private URL url;

    @Column(name = "enabled", nullable = false)
    private Boolean enabled;

    @Transient
    public static CallbackEntity with(
            final TopicEntity topicEntity, final HttpMethod method, final URL url, final Boolean enabled) {
        final var entity = new CallbackEntity();
        entity.topicEntity = Require.notNull("topicEntity", topicEntity);
        return entity.setMethod(method).setUrl(url).setEnabled(enabled);
    }

    @Override
    protected CallbackEntity get() {
        return this;
    }

    public TopicEntity getTopicEntity() {
        return topicEntity;
    }

    public HttpMethod getMethod() {
        return method;
    }

    public CallbackEntity setMethod(final HttpMethod method) {
        this.method = Require.notNull("method", method);
        return update();
    }

    public URL getUrl() {
        return url;
    }

    public CallbackEntity setUrl(final URL url) {
        this.url = Require.notNull("url", url);
        return update();
    }

    public Boolean isEnabled() {
        return enabled;
    }

    public CallbackEntity setEnabled(final Boolean enabled) {
        this.enabled = Require.notNull("enabled", enabled);
        return update();
    }

    @Override
    @Transient
    protected ToStringCreator toStringCreator() {
        return super.toStringCreator()
                .append("topicEntity", getTopicEntity())
                .append("method", getMethod())
                .append("url", getUrl())
                .append("enabled", isEnabled());
    }
}
