package su.toor.topical.database;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.ForeignKey;
import jakarta.persistence.Index;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedEntityGraph;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.core.style.ToStringCreator;
import org.springframework.lang.Nullable;
import su.toor.topical.shared.Qos;
import su.toor.topical.shared.misc.Require;

@Entity
@Table(
        name = "message",
        indexes = {@Index(name = "idx_uuid", columnList = "uuid", unique = true)})
@NamedEntityGraph(name = "MessageEntity.topicEntity")
public class MessageEntity extends AbstractEntity<MessageEntity> {

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(
            name = "topic_id",
            nullable = false,
            updatable = false,
            foreignKey = @ForeignKey(name = "fk_message_topic_id"))
    @OnDelete(action = OnDeleteAction.CASCADE)
    private TopicEntity topicEntity;

    @Column(name = "payload", nullable = false, updatable = false, columnDefinition = "BLOB")
    private byte[] payload;

    @Column(name = "qos", updatable = false)
    @Enumerated(EnumType.ORDINAL)
    private Qos qos;

    @Column(name = "retained", updatable = false)
    private Boolean retained;

    @Column(name = "duplicate", updatable = false)
    private Boolean duplicate;

    @Column(name = "message_id", updatable = false)
    private Integer messageId;

    @Transient
    public static MessageEntity with(
            final TopicEntity topicEntity,
            final byte[] payload,
            final Qos qos,
            final Boolean retained,
            final Boolean duplicate,
            final Integer messageId) {
        final var entity = new MessageEntity();
        entity.topicEntity = Require.notNull("topicEntity", topicEntity);
        entity.payload = Require.notNull("payload", payload);
        entity.qos = qos;
        entity.retained = retained;
        entity.duplicate = duplicate;
        entity.messageId = messageId;
        entity.update();
        return entity;
    }

    @Override
    protected MessageEntity get() {
        return this;
    }

    public TopicEntity getTopicEntity() {
        return topicEntity;
    }

    public byte[] getPayload() {
        return payload;
    }

    @Nullable public Qos getQos() {
        return qos;
    }

    @Nullable public Boolean isRetained() {
        return retained;
    }

    @Nullable public Boolean isDuplicate() {
        return duplicate;
    }

    @Nullable public Integer getMessageId() {
        return messageId;
    }

    @Override
    @Transient
    protected ToStringCreator toStringCreator() {
        return super.toStringCreator()
                .append("topicEntity", getTopicEntity())
                .append("payload", getPayload())
                .append("qos", getQos())
                .append("retained", isRetained())
                .append("duplicate", isDuplicate())
                .append("messageId", getMessageId());
    }
}
