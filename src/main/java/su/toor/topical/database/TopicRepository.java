package su.toor.topical.database;

import java.util.Optional;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TopicRepository extends JpaRepository<TopicEntity, Long> {

    Optional<TopicEntity> findByUuid(final UUID uuid);

    Optional<TopicEntity> findByName(final String name);
}
