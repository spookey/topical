package su.toor.topical.database;

import jakarta.persistence.Column;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.MappedSuperclass;
import jakarta.persistence.Transient;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.UUID;
import org.hibernate.annotations.JdbcTypeCode;
import org.springframework.core.style.ToStringCreator;

@MappedSuperclass
public abstract class AbstractEntity<T extends AbstractEntity<T>> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    protected Long id;

    @Column(name = "uuid", nullable = false, updatable = false, columnDefinition = "VARCHAR(36)")
    @JdbcTypeCode(java.sql.Types.VARCHAR)
    private UUID uuid = UUID.randomUUID();

    @Column(name = "created", updatable = false, nullable = false, columnDefinition = "TIMESTAMP(6)")
    private OffsetDateTime created = OffsetDateTime.now(ZoneOffset.UTC);

    @Column(name = "updated", nullable = false, columnDefinition = "TIMESTAMP(6)")
    protected OffsetDateTime updated = OffsetDateTime.now(ZoneOffset.UTC);

    protected abstract T get();

    public Long getId() {
        return id;
    }

    public UUID getUuid() {
        return uuid;
    }

    public OffsetDateTime getCreated() {
        return created;
    }

    public OffsetDateTime getUpdated() {
        return updated;
    }

    public T update() {
        this.updated = OffsetDateTime.now(ZoneOffset.UTC);
        return get();
    }

    @Transient
    protected ToStringCreator toStringCreator() {
        return new ToStringCreator(this)
                .append("id", getId())
                .append("uuid", getUuid())
                .append("created", getCreated())
                .append("updated", getUpdated());
    }

    @Override
    @Transient
    public String toString() {
        return toStringCreator().toString();
    }
}
