package su.toor.topical.database;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CallbackRepository extends JpaRepository<CallbackEntity, Long> {

    @EntityGraph("CallbackEntity.topicEntity")
    Optional<CallbackEntity> findByUuid(final UUID uuid);

    @EntityGraph("CallbackEntity.topicEntity")
    List<CallbackEntity> findAllByTopicEntityAndEnabled(final TopicEntity topicEntity, final Boolean enabled);
}
