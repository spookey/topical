package su.toor.topical.database;

import java.time.OffsetDateTime;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MessageRepository extends JpaRepository<MessageEntity, Long> {

    @EntityGraph("MessageEntity.topicEntity")
    Optional<MessageEntity> findByUuid(final UUID uuid);

    @EntityGraph("MessageEntity.topicEntity")
    Page<MessageEntity> findAllByCreatedAfter(final OffsetDateTime created, final Pageable pageable);

    @EntityGraph("MessageEntity.topicEntity")
    Set<MessageEntity> findAllByCreatedBefore(final OffsetDateTime created);
}
