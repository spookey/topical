package su.toor.topical.resource.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import java.time.OffsetDateTime;
import java.util.UUID;
import java.util.function.Function;
import org.springframework.lang.Nullable;
import su.toor.topical.database.MessageEntity;
import su.toor.topical.shared.Qos;
import su.toor.topical.shared.dto.PayloadDto;
import su.toor.topical.shared.misc.Require;

public record MessageDto(
        @JsonProperty("topic") @Schema(name = "topic", description = "Mqtt topic") TopicDto topic,
        @JsonProperty("payload") @Schema(name = "payload", description = "Message payload") PayloadDto payload,
        @JsonProperty("qos") @Schema(name = "qos", description = "Quality of service", nullable = true) @Nullable Qos qos,
        @JsonProperty("retained") @Schema(name = "retained", description = "Is retained", nullable = true) @Nullable Boolean retained,
        @JsonProperty("duplicate") @Schema(name = "duplicate", description = "Is duplicate", nullable = true) @Nullable Boolean duplicate,
        @JsonProperty("messageId") @Schema(name = "messageId", description = "Message Id", nullable = true) @Nullable Integer messageId,
        @JsonProperty("uuid")
                @Schema(name = "uuid", description = "Unique identifier", accessMode = Schema.AccessMode.READ_ONLY)
                UUID uuid,
        @JsonProperty("created")
                @Schema(name = "created", description = "Creation time", accessMode = Schema.AccessMode.READ_ONLY)
                OffsetDateTime created) {

    public MessageDto {
        Require.notNull("topic", topic);
        Require.notNull("payload", payload);
        Require.notNull("uuid", uuid);
        Require.notNull("created", created);
    }

    @JsonIgnore
    public static MessageDto from(final MessageEntity messageEntity, final Function<byte[], PayloadDto> parser) {
        return new MessageDto(
                TopicDto.from(messageEntity.getTopicEntity()),
                parser.apply(messageEntity.getPayload()),
                messageEntity.getQos(),
                messageEntity.isRetained(),
                messageEntity.isDuplicate(),
                messageEntity.getMessageId(),
                messageEntity.getUuid(),
                messageEntity.getCreated());
    }
}
