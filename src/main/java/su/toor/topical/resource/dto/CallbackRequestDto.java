package su.toor.topical.resource.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.swagger.v3.oas.annotations.media.Schema;
import java.net.URL;
import java.util.Optional;
import org.springframework.http.HttpMethod;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import su.toor.topical.shared.misc.Require;
import su.toor.topical.shared.support.HttpMethodToString;
import su.toor.topical.shared.support.StringToHttpMethod;

public record CallbackRequestDto(
        @JsonProperty("topic") @Schema(name = "topic", description = "Topic name") String topic,
        @JsonProperty("method")
                @Schema(name = "method", description = "Http method")
                @JsonSerialize(converter = HttpMethodToString.class)
                @JsonDeserialize(converter = StringToHttpMethod.class)
                @Nullable HttpMethod method,
        @JsonProperty("url") @Schema(name = "url", description = "Callback url") URL url,
        @JsonProperty("enabled") @Schema(name = "enabled", description = "Enable", defaultValue = "true") @Nullable Boolean enabled) {

    public CallbackRequestDto {
        Require.validListenTopic(topic);
        Require.notNull("url", url);
    }

    @Override
    @NonNull public HttpMethod method() {
        return Optional.ofNullable(method).orElse(HttpMethod.POST);
    }

    @Override
    @NonNull public Boolean enabled() {
        return Optional.ofNullable(enabled).orElse(true);
    }
}
