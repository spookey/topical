package su.toor.topical.resource.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import java.util.Optional;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import su.toor.topical.shared.Qos;
import su.toor.topical.shared.misc.Require;

public record SendRequestDto(
        @JsonProperty("topic") @Schema(name = "topic", description = "Topic name") String topic,
        @JsonProperty("qos") @Schema(name = "qos", description = "Quality of service", defaultValue = "ZERO") Qos qos,
        @JsonProperty("retained")
                @Schema(name = "retained", description = "Retain message", nullable = true, hidden = true)
                @Nullable Boolean retained,
        @JsonProperty("value") @Schema(name = "value", description = "Value as string") String value) {

    public SendRequestDto {
        Require.validMessageTopic(topic);
        Require.notNull("qos", qos);
        Require.notEmpty("value", value);
    }

    @Override
    @NonNull public Boolean retained() {
        return Optional.ofNullable(retained).orElse(false);
    }
}
