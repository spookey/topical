package su.toor.topical.resource.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import java.time.OffsetDateTime;
import java.util.UUID;
import su.toor.topical.database.TopicEntity;
import su.toor.topical.shared.misc.Require;

public record TopicDto(
        @JsonProperty("name") @Schema(name = "name", description = "Topic name or filter") String name,
        @JsonProperty("uuid")
                @Schema(name = "uuid", description = "Unique identifier", accessMode = Schema.AccessMode.READ_ONLY)
                UUID uuid,
        @JsonProperty("created")
                @Schema(name = "created", description = "Creation time", accessMode = Schema.AccessMode.READ_ONLY)
                OffsetDateTime created) {

    public TopicDto {
        Require.notBlank("name", name);
        Require.notNull("uuid", uuid);
        Require.notNull("created", created);
    }

    public static TopicDto from(final TopicEntity topicEntity) {
        return new TopicDto(topicEntity.getName(), topicEntity.getUuid(), topicEntity.getCreated());
    }
}
