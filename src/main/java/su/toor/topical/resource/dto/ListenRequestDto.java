package su.toor.topical.resource.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import java.util.Optional;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import su.toor.topical.shared.Qos;
import su.toor.topical.shared.misc.Require;

public record ListenRequestDto(
        @JsonProperty("filter") @Schema(name = "filter", description = "Topic filter") String filter,
        @JsonProperty("qos") @Schema(name = "qos", description = "Quality of service", defaultValue = "ZERO") @Nullable Qos qos,
        @JsonProperty("enabled") @Schema(name = "enabled", description = "Enable", defaultValue = "true") @Nullable Boolean enabled) {

    public ListenRequestDto {
        Require.validListenTopic(filter);
    }

    @Override
    @NonNull public Qos qos() {
        return Optional.ofNullable(qos).orElse(Qos.ZERO);
    }

    @Override
    @NonNull public Boolean enabled() {
        return Optional.ofNullable(enabled).orElse(true);
    }
}
