package su.toor.topical.resource.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import java.time.OffsetDateTime;
import java.util.UUID;
import su.toor.topical.database.ListenEntity;
import su.toor.topical.shared.Qos;
import su.toor.topical.shared.misc.Require;

public record ListenDto(
        @JsonProperty("filter") @Schema(name = "filter", description = "Mqtt topic filter") String filter,
        @JsonProperty("qos") @Schema(name = "qos", description = "Quality of service") Qos qos,
        @JsonProperty("enabled") @Schema(name = "enabled", description = "Disable temporarily") Boolean enabled,
        @JsonProperty("uuid")
                @Schema(name = "uuid", description = "Unique identifier", accessMode = Schema.AccessMode.READ_ONLY)
                UUID uuid,
        @JsonProperty("created")
                @Schema(name = "created", description = "Creation time", accessMode = Schema.AccessMode.READ_ONLY)
                OffsetDateTime created,
        @JsonProperty("updated")
                @Schema(name = "updated", description = "Last update time", accessMode = Schema.AccessMode.READ_ONLY)
                OffsetDateTime updated) {

    public ListenDto {
        Require.validListenTopic(filter);
        Require.notNull("qos", qos);
        Require.notNull("enabled", enabled);
        Require.notNull("uuid", uuid);
        Require.notNull("created", created);
        Require.notNull("updated", updated);
    }

    @JsonIgnore
    public static ListenDto from(final ListenEntity listenEntity) {
        return new ListenDto(
                listenEntity.getFilter(),
                listenEntity.getQos(),
                listenEntity.isEnabled(),
                listenEntity.getUuid(),
                listenEntity.getCreated(),
                listenEntity.getUpdated());
    }
}
