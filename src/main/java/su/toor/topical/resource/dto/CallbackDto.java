package su.toor.topical.resource.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.swagger.v3.oas.annotations.media.Schema;
import java.net.URL;
import java.time.OffsetDateTime;
import java.util.UUID;
import org.springframework.http.HttpMethod;
import su.toor.topical.database.CallbackEntity;
import su.toor.topical.shared.misc.Require;
import su.toor.topical.shared.support.HttpMethodToString;
import su.toor.topical.shared.support.StringToHttpMethod;

public record CallbackDto(
        @JsonProperty("topic") @Schema(name = "topic", description = "Mqtt topic subscription") TopicDto topic,
        @JsonProperty("method")
                @Schema(name = "method", description = "Http method")
                @JsonSerialize(converter = HttpMethodToString.class)
                @JsonDeserialize(converter = StringToHttpMethod.class)
                HttpMethod method,
        @JsonProperty("url") @Schema(name = "url", description = "Address to call") URL url,
        @JsonProperty("enabled") @Schema(name = "enabled", description = "Disable temporarily") Boolean enabled,
        @JsonProperty("uuid")
                @Schema(name = "uuid", description = "Unique identifier", accessMode = Schema.AccessMode.READ_ONLY)
                UUID uuid,
        @JsonProperty("created")
                @Schema(name = "created", description = "Creation time", accessMode = Schema.AccessMode.READ_ONLY)
                OffsetDateTime created,
        @JsonProperty("updated")
                @Schema(name = "updated", description = "Last update time", accessMode = Schema.AccessMode.READ_ONLY)
                OffsetDateTime updated) {

    public CallbackDto {
        Require.notNull("topic", topic);
        Require.notNull("method", method);
        Require.notNull("url", url);
        Require.notNull("enabled", enabled);
        Require.notNull("uuid", uuid);
        Require.notNull("created", created);
        Require.notNull("updated", updated);
    }

    @JsonIgnore
    public static CallbackDto from(final CallbackEntity callbackEntity) {
        return new CallbackDto(
                TopicDto.from(callbackEntity.getTopicEntity()),
                callbackEntity.getMethod(),
                callbackEntity.getUrl(),
                callbackEntity.isEnabled(),
                callbackEntity.getUuid(),
                callbackEntity.getCreated(),
                callbackEntity.getUpdated());
    }
}
