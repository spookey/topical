package su.toor.topical.resource;

import static net.logstash.logback.argument.StructuredArguments.keyValue;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import io.micrometer.core.annotation.Timed;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import su.toor.topical.component.mqtt.PayloadParser;
import su.toor.topical.resource.dto.MessageDto;
import su.toor.topical.service.EntityService;
import su.toor.topical.shared.dto.PageDto;

@Timed
@Tag(name = "Message", description = "Message api")
@RestController
@RequestMapping(value = "/api/messages")
public class MessageApiResource {

    private static final Logger LOG = LoggerFactory.getLogger(MessageApiResource.class);

    static final String FIELD_PAGE = "page";
    static final String FIELD_UUID = "uuid";

    private final EntityService entityService;
    private final PayloadParser payloadParser;

    @Autowired
    public MessageApiResource(final EntityService entityService, final PayloadParser payloadParser) {
        this.entityService = entityService;
        this.payloadParser = payloadParser;
    }

    @Timed
    @Operation(
            summary = "View all messages",
            tags = {"Message"})
    @ApiResponse(responseCode = "200", description = "Ok")
    @ApiResponse(responseCode = "500", description = "Not ok")
    @GetMapping(produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<PageDto<MessageDto>> allMessages(
            @Parameter(name = "page", description = "Page number") @RequestParam(name = "page", defaultValue = "0")
                    final Integer page,
            @Parameter(name = "size", description = "Page size") @RequestParam(name = "size", defaultValue = "0")
                    final Integer size) {
        final var messageEntities = entityService.allMessages(page, size);
        final var response = PageDto.with(messageEntities.map(entity -> MessageDto.from(entity, payloadParser::parse)));
        LOG.info("messages requested [{}]", keyValue(FIELD_PAGE, response));

        return ResponseEntity.ok(response);
    }

    @Timed
    @Operation(
            summary = "View one message",
            tags = {"Message"})
    @ApiResponse(responseCode = "200", description = "Ok")
    @ApiResponse(responseCode = "404", description = "Not found")
    @ApiResponse(responseCode = "500", description = "Not ok")
    @GetMapping(value = "/{uuid}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<MessageDto> oneMessage(
            @Parameter(name = "uuid", description = "Unique identifier") @PathVariable(name = "uuid") final UUID uuid) {
        LOG.info("one message requested [{}]", keyValue(FIELD_UUID, uuid));
        final var optionalMessageEntity = entityService.findMessage(uuid);
        if (optionalMessageEntity.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        final var response = MessageDto.from(optionalMessageEntity.get(), payloadParser::parse);
        return ResponseEntity.ok(response);
    }

    @Timed
    @Operation(
            summary = "Delete existing message",
            tags = {"Message"})
    @ApiResponse(responseCode = "200", description = "Ok")
    @ApiResponse(responseCode = "404", description = "Not found")
    @ApiResponse(responseCode = "500", description = "Not ok")
    @DeleteMapping(value = "/{uuid}")
    public ResponseEntity<Void> deleteMessage(
            @Parameter(name = "uuid", description = "Unique identifier") @PathVariable(name = "uuid") final UUID uuid) {
        LOG.info("deleting message [{}]", keyValue(FIELD_UUID, uuid));

        if (!entityService.deleteMessage(uuid)) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().build();
    }
}
