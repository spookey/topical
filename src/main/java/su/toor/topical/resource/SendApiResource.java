package su.toor.topical.resource;

import static net.logstash.logback.argument.StructuredArguments.keyValue;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import io.micrometer.core.annotation.Timed;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import su.toor.topical.component.mqtt.PayloadParser;
import su.toor.topical.resource.dto.SendRequestDto;
import su.toor.topical.service.ClientService;

@Timed
@Tag(name = "Send", description = "Send api")
@RestController
@RequestMapping(value = "/api/send")
public class SendApiResource {

    private static final Logger LOG = LoggerFactory.getLogger(SendApiResource.class);

    static final String FIELD_TOPIC = "topic";
    static final String FIELD_VALUE = "value";

    private final ClientService clientService;
    private final PayloadParser payloadParser;

    @Autowired
    public SendApiResource(final ClientService clientService, final PayloadParser payloadParser) {
        this.clientService = clientService;
        this.payloadParser = payloadParser;
    }

    @Timed
    @Operation(
            summary = "Send mqtt message",
            tags = {"Send"})
    @ApiResponse(responseCode = "201", description = "Sent")
    @ApiResponse(responseCode = "400", description = "Bad")
    @ApiResponse(responseCode = "424", description = "Not sent")
    @ApiResponse(responseCode = "500", description = "Not ok")
    @PostMapping(consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> sendMessage(@Valid @RequestBody final SendRequestDto body) {
        final var topic = body.topic();
        final var value = body.value();
        final var message = payloadParser.compose(value, body.qos(), body.retained());
        LOG.info("sending mqtt message [{}] [{}]", keyValue(FIELD_TOPIC, topic), keyValue(FIELD_VALUE, value));

        if (!clientService.send(topic, message)) {
            return ResponseEntity.status(HttpStatus.FAILED_DEPENDENCY).build();
        }
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }
}
