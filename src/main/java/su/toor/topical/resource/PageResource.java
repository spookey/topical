package su.toor.topical.resource;

import static net.logstash.logback.argument.StructuredArguments.keyValue;
import static org.springframework.http.MediaType.TEXT_HTML_VALUE;

import io.micrometer.core.annotation.Timed;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.thymeleaf.ThymeleafProperties;
import org.springframework.boot.info.BuildProperties;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.ModelAndView;
import su.toor.topical.application.setting.BasicSetting;
import su.toor.topical.component.mqtt.PayloadParser;
import su.toor.topical.resource.dto.MessageDto;
import su.toor.topical.resource.model.CoreModel;
import su.toor.topical.service.EntityService;
import su.toor.topical.shared.dto.PageDto;

@Timed
@Controller
@RequestMapping(value = "/", produces = TEXT_HTML_VALUE)
public class PageResource {

    private static final Logger LOG = LoggerFactory.getLogger(PageResource.class);

    static final String FIELD_CONTENT = "content";
    static final String FIELD_CORE = "core";
    static final String FIELD_MESSAGE_AMOUNT = "messageAmount";
    static final String FIELD_PAGE = "page";
    static final String FIELD_SIZE = "size";
    static final String FIELD_TOPIC_AMOUNT = "topicAmount";
    static final String VIEW_INDEX = "index";

    private final BuildProperties buildProperties;
    private final EntityService entityService;
    private final PayloadParser payloadParser;
    private final BasicSetting basicSetting;
    private final ThymeleafProperties thymeleafProperties;

    @Autowired
    public PageResource(
            final BuildProperties buildProperties,
            final EntityService entityService,
            final PayloadParser payloadParser,
            final BasicSetting basicSetting,
            final ThymeleafProperties thymeleafProperties) {
        this.buildProperties = buildProperties;
        this.entityService = entityService;
        this.payloadParser = payloadParser;
        this.basicSetting = basicSetting;
        this.thymeleafProperties = thymeleafProperties;
    }

    @ModelAttribute(FIELD_CORE)
    public CoreModel emitCoreModel() {
        final var applicationName = buildProperties.getName();
        return CoreModel.from(basicSetting, applicationName, thymeleafProperties);
    }

    @Timed
    @GetMapping(
            value = {"/", "/page/{page}", "/page/{page}/size/{size}"},
            produces = TEXT_HTML_VALUE)
    public ModelAndView getIndex(
            @PathVariable(name = "page", required = false) final Integer page,
            @PathVariable(name = "size", required = false) final Integer size) {
        final var shiftPage = CoreModel.shiftPage(page);
        final var messageEntities = entityService.recentMessages(shiftPage, size);
        final var content = PageDto.with(messageEntities.map(entity -> MessageDto.from(entity, payloadParser::parse)));

        final var showPage = CoreModel.showPage(page);
        final var showSize = CoreModel.showSize(size, basicSetting.getPaginationSize());

        LOG.info("page requested [{}] [{}]", keyValue(FIELD_PAGE, page), keyValue(FIELD_SIZE, size));

        if (showPage > content.getPages()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        return new ModelAndView(
                VIEW_INDEX,
                Map.of(
                        FIELD_TOPIC_AMOUNT, entityService.countTopics(),
                        FIELD_MESSAGE_AMOUNT, entityService.countMessages(),
                        FIELD_CONTENT, content,
                        FIELD_PAGE, showPage,
                        FIELD_SIZE, showSize));
    }
}
