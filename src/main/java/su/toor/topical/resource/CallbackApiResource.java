package su.toor.topical.resource;

import static net.logstash.logback.argument.StructuredArguments.keyValue;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import io.micrometer.core.annotation.Timed;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import java.util.Map;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;
import su.toor.topical.resource.dto.CallbackDto;
import su.toor.topical.resource.dto.CallbackRequestDto;
import su.toor.topical.service.EntityService;
import su.toor.topical.shared.dto.PageDto;

@Timed
@Tag(name = "Callback", description = "Callback api")
@RestController
@RequestMapping(value = "/api/callbacks")
public class CallbackApiResource {

    private static final Logger LOG = LoggerFactory.getLogger(CallbackApiResource.class);

    static final String FIELD_PAGE = "page";
    static final String FIELD_BODY = "body";
    static final String FIELD_UUID = "uuid";

    private final EntityService entityService;

    @Autowired
    public CallbackApiResource(final EntityService entityService) {
        this.entityService = entityService;
    }

    @Timed
    @Operation(
            summary = "Create new callback",
            tags = {"Callback"})
    @ApiResponse(responseCode = "201", description = "Created")
    @ApiResponse(responseCode = "400", description = "Bad")
    @ApiResponse(responseCode = "500", description = "Not ok")
    @PostMapping(consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> createCallback(@Valid @RequestBody final CallbackRequestDto body) {
        LOG.info("creating callback [{}]", keyValue(FIELD_BODY, body));

        final var callbackEntity =
                entityService.createCallback(body.topic(), body.method(), body.url(), body.enabled());

        return ResponseEntity.created(UriComponentsBuilder.fromPath("/api/callbacks/{uuid}")
                        .build(Map.of("uuid", callbackEntity.getUuid())))
                .build();
    }

    @Timed
    @Operation(
            summary = "View all callbacks",
            tags = {"Callback"})
    @ApiResponse(responseCode = "200", description = "Ok")
    @ApiResponse(responseCode = "500", description = "Not ok")
    @GetMapping(produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<PageDto<CallbackDto>> allCallbacks(
            @Parameter(name = "page", description = "Page number") @RequestParam(name = "page", defaultValue = "0")
                    final Integer page,
            @Parameter(name = "size", description = "Page size") @RequestParam(name = "size", defaultValue = "0")
                    final Integer size) {
        final var callbackEntities = entityService.allCallbacks(page, size);
        final var response = PageDto.with(callbackEntities.map(CallbackDto::from));
        LOG.info("callbacks requested [{}]", keyValue(FIELD_PAGE, response));

        return ResponseEntity.ok(response);
    }

    @Timed
    @Operation(
            summary = "View one callback",
            tags = {"Callback"})
    @ApiResponse(responseCode = "200", description = "Ok")
    @ApiResponse(responseCode = "404", description = "Not found")
    @ApiResponse(responseCode = "500", description = "Not ok")
    @GetMapping(value = "/{uuid}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<CallbackDto> oneCallback(
            @Parameter(name = "uuid", description = "Unique identifier") @PathVariable(name = "uuid") final UUID uuid) {
        LOG.info("one callback requested [{}]", keyValue(FIELD_UUID, uuid));
        final var optionalCallbackEntity = entityService.findCallback(uuid);
        if (optionalCallbackEntity.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        final var response = CallbackDto.from(optionalCallbackEntity.get());
        return ResponseEntity.ok(response);
    }

    @Timed
    @Operation(
            summary = "Modify existing callback",
            tags = {"Callback"})
    @ApiResponse(responseCode = "200", description = "Ok")
    @ApiResponse(responseCode = "400", description = "Bad")
    @ApiResponse(responseCode = "404", description = "Not found")
    @ApiResponse(responseCode = "500", description = "Not ok")
    @PutMapping(value = "/{uuid}", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<CallbackDto> modifyCallback(
            @Parameter(name = "uuid", description = "Unique identifier") @PathVariable(name = "uuid") final UUID uuid,
            @Valid @RequestBody final CallbackRequestDto body) {
        LOG.info("modifying callback [{}] [{}]", keyValue(FIELD_UUID, uuid), keyValue(FIELD_BODY, body));

        final var optionalCallbackEntity =
                entityService.modifyCallback(uuid, body.method(), body.url(), body.enabled());
        if (optionalCallbackEntity.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        final var response = CallbackDto.from(optionalCallbackEntity.get());
        return ResponseEntity.ok(response);
    }

    @Timed
    @Operation(
            summary = "Delete existing callback",
            tags = {"Callback"})
    @ApiResponse(responseCode = "200", description = "Ok")
    @ApiResponse(responseCode = "404", description = "Not found")
    @ApiResponse(responseCode = "500", description = "Not ok")
    @DeleteMapping(value = "/{uuid}")
    public ResponseEntity<Void> deleteCallback(
            @Parameter(name = "uuid", description = "Unique identifier") @PathVariable(name = "uuid") final UUID uuid) {
        LOG.info("deleting callback [{}]", keyValue(FIELD_UUID, uuid));

        if (!entityService.deleteCallback(uuid)) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().build();
    }
}
