package su.toor.topical.resource;

import static net.logstash.logback.argument.StructuredArguments.keyValue;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import io.micrometer.core.annotation.Timed;
import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import su.toor.topical.resource.dto.TopicDto;
import su.toor.topical.service.EntityService;
import su.toor.topical.shared.dto.PageDto;

@Timed
@Tag(name = "Topic", description = "Topic api")
@RestController
@RequestMapping(value = "/api/topics")
public class TopicApiResource {

    private static final Logger LOG = LoggerFactory.getLogger(TopicApiResource.class);

    static final String FIELD_PAGE = "page";
    static final String FIELD_UUID = "uuid";

    private final EntityService entityService;

    @Autowired
    public TopicApiResource(final EntityService entityService) {
        this.entityService = entityService;
    }

    @Timed
    @Operation(
            summary = "View all topics",
            tags = {"Topic"})
    @ApiResponse(responseCode = "200", description = "Ok")
    @ApiResponse(responseCode = "500", description = "Not ok")
    @GetMapping(produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<PageDto<TopicDto>> allTopics(
            @Parameter(name = "page", description = "Page number") @RequestParam(name = "page", defaultValue = "0")
                    final Integer page,
            @Parameter(name = "size", description = "Page size") @RequestParam(name = "size", defaultValue = "0")
                    final Integer size) {
        final var topicEntities = entityService.allTopics(page, size);
        final var response = PageDto.with(topicEntities.map(TopicDto::from));
        LOG.info("topics requested [{}]", keyValue(FIELD_PAGE, response));

        return ResponseEntity.ok(response);
    }

    @Timed
    @Operation(
            summary = "View one topic",
            tags = {"Topic"})
    @ApiResponse(responseCode = "200", description = "Ok")
    @ApiResponse(responseCode = "404", description = "Not found")
    @ApiResponse(responseCode = "500", description = "Not ok")
    @GetMapping(value = "/{uuid}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<TopicDto> oneTopic(
            @Parameter(name = "uuid", description = "Unique identifier") @PathVariable(name = "uuid") final UUID uuid) {
        LOG.info("one topic requested [{}]", keyValue(FIELD_UUID, uuid));
        final var optionalTopicEntity = entityService.findTopic(uuid);
        if (optionalTopicEntity.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        final var response = TopicDto.from(optionalTopicEntity.get());
        return ResponseEntity.ok(response);
    }

    @Timed
    @Hidden
    @Operation(
            summary = "Delete existing topic - and all it's messages & callbacks",
            tags = {"Topic"})
    @ApiResponse(responseCode = "200", description = "Ok")
    @ApiResponse(responseCode = "404", description = "Not found")
    @ApiResponse(responseCode = "500", description = "Not ok")
    @DeleteMapping(value = "/{uuid}")
    public ResponseEntity<Void> deleteTopic(
            @Parameter(name = "uuid", description = "Unique identifier") @PathVariable(name = "uuid") final UUID uuid) {
        LOG.info("deleting topic [{}]", keyValue(FIELD_UUID, uuid));

        if (!entityService.deleteTopic(uuid)) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().build();
    }
}
