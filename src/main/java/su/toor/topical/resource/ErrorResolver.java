package su.toor.topical.resource;

import jakarta.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.thymeleaf.ThymeleafProperties;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorViewResolver;
import org.springframework.boot.info.BuildProperties;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import su.toor.topical.application.setting.BasicSetting;
import su.toor.topical.resource.model.CoreModel;

@Component
public class ErrorResolver implements ErrorViewResolver {

    static final String FIELD_CORE = "core";
    static final String VIEW_ERROR = "error";

    private final BuildProperties buildProperties;
    private final BasicSetting basicSetting;
    private final ThymeleafProperties thymeleafProperties;

    @Autowired
    public ErrorResolver(
            final BuildProperties buildProperties,
            final BasicSetting basicSetting,
            final ThymeleafProperties thymeleafProperties) {
        this.buildProperties = buildProperties;
        this.basicSetting = basicSetting;
        this.thymeleafProperties = thymeleafProperties;
    }

    @Override
    public ModelAndView resolveErrorView(
            final HttpServletRequest request, final HttpStatus status, final Map<String, Object> model) {
        final var applicationName = buildProperties.getName();
        final var core = CoreModel.from(basicSetting, applicationName, thymeleafProperties);
        final var errorModel = new HashMap<>(model);
        errorModel.put(FIELD_CORE, core);

        return new ModelAndView(VIEW_ERROR, errorModel, status);
    }
}
