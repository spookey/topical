package su.toor.topical.resource;

import static net.logstash.logback.argument.StructuredArguments.keyValue;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import io.micrometer.core.annotation.Timed;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import java.util.Map;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;
import su.toor.topical.resource.dto.ListenDto;
import su.toor.topical.resource.dto.ListenRequestDto;
import su.toor.topical.service.ClientService;
import su.toor.topical.service.EntityService;
import su.toor.topical.shared.dto.PageDto;

@Timed
@Tag(name = "Listen", description = "Listen api")
@RestController
@RequestMapping(value = "/api/listen")
public class ListenApiResource {

    private static final Logger LOG = LoggerFactory.getLogger(ListenApiResource.class);

    static final String FIELD_PAGE = "page";
    static final String FIELD_BODY = "body";
    static final String FIELD_UUID = "uuid";

    private final EntityService entityService;
    private final ClientService clientService;

    @Autowired
    public ListenApiResource(final EntityService entityService, final ClientService clientService) {
        this.entityService = entityService;
        this.clientService = clientService;
    }

    @Timed
    @Operation(
            summary = "Create new listener",
            tags = {"Listen"})
    @ApiResponse(responseCode = "201", description = "Created")
    @ApiResponse(responseCode = "400", description = "Bad")
    @ApiResponse(responseCode = "500", description = "Not ok")
    @PostMapping(consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> createListener(@Valid @RequestBody final ListenRequestDto body) {
        LOG.info("creating listener [{}]", keyValue(FIELD_BODY, body));

        final var listenEntity = entityService.createListener(body.filter(), body.qos(), body.enabled());

        clientService.refreshSubscriptions();
        return ResponseEntity.created(UriComponentsBuilder.fromPath("/api/listen/{uuid}")
                        .build(Map.of("uuid", listenEntity.getUuid())))
                .build();
    }

    @Timed
    @Operation(
            summary = "View all listeners",
            tags = {"Listen"})
    @ApiResponse(responseCode = "200", description = "Ok")
    @ApiResponse(responseCode = "500", description = "Not ok")
    @GetMapping(produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<PageDto<ListenDto>> allListeners(
            @Parameter(name = "page", description = "Page number") @RequestParam(name = "page", defaultValue = "0")
                    final Integer page,
            @Parameter(name = "size", description = "Page size") @RequestParam(name = "size", defaultValue = "0")
                    final Integer size) {
        final var listenEntities = entityService.allListeners(page, size);
        final var response = PageDto.with(listenEntities.map(ListenDto::from));
        LOG.info("listeners requested [{}]", keyValue(FIELD_PAGE, response));

        return ResponseEntity.ok(response);
    }

    @Timed
    @Operation(
            summary = "View one listener",
            tags = {"Listen"})
    @ApiResponse(responseCode = "200", description = "Ok")
    @ApiResponse(responseCode = "404", description = "Not found")
    @ApiResponse(responseCode = "500", description = "Not ok")
    @GetMapping(value = "/{uuid}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<ListenDto> oneListener(
            @Parameter(name = "uuid", description = "Unique identifier") @PathVariable(name = "uuid") final UUID uuid) {
        LOG.info("one listener requested [{}]", keyValue(FIELD_UUID, uuid));
        final var optionalListenEntity = entityService.findListener(uuid);
        if (optionalListenEntity.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        final var response = ListenDto.from(optionalListenEntity.get());
        return ResponseEntity.ok(response);
    }

    @Timed
    @Operation(
            summary = "Modify existing listener",
            tags = {"Listen"})
    @ApiResponse(responseCode = "200", description = "Ok")
    @ApiResponse(responseCode = "400", description = "Bad")
    @ApiResponse(responseCode = "404", description = "Not found")
    @ApiResponse(responseCode = "500", description = "Not ok")
    @PutMapping(value = "/{uuid}", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<ListenDto> modifyListener(
            @Parameter(name = "uuid", description = "Unique identifier") @PathVariable(name = "uuid") final UUID uuid,
            @Valid @RequestBody final ListenRequestDto body) {
        LOG.info("modifying listener [{}] [{}]", keyValue(FIELD_UUID, uuid), keyValue(FIELD_BODY, body));

        final var optionalListenEntity = entityService.modifyListener(uuid, body.filter(), body.qos(), body.enabled());
        if (optionalListenEntity.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        final var response = ListenDto.from(optionalListenEntity.get());
        clientService.refreshSubscriptions();
        return ResponseEntity.ok(response);
    }

    @Timed
    @Operation(
            summary = "Delete existing listener",
            tags = {"Listen"})
    @ApiResponse(responseCode = "200", description = "Ok")
    @ApiResponse(responseCode = "404", description = "Not found")
    @ApiResponse(responseCode = "500", description = "Not ok")
    @DeleteMapping(value = "/{uuid}")
    public ResponseEntity<Void> deleteListener(
            @Parameter(name = "uuid", description = "Unique identifier") @PathVariable(name = "uuid") final UUID uuid) {
        LOG.info("deleting listener [{}]", keyValue(FIELD_UUID, uuid));

        if (!entityService.deleteListener(uuid)) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().build();
    }
}
