package su.toor.topical.resource.model;

import java.nio.charset.StandardCharsets;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.Optional;
import org.springframework.boot.autoconfigure.thymeleaf.ThymeleafProperties;
import org.springframework.core.style.ToStringCreator;
import su.toor.topical.application.setting.BasicSetting;
import su.toor.topical.shared.misc.Require;

public class CoreModel {

    static final Locale LOCALE = Locale.ENGLISH;

    private final BasicSetting basicSetting;
    private final String applicationName;
    private final String charset;
    private final String language = LOCALE.getLanguage();

    CoreModel(final BasicSetting basicSetting, final String applicationName, final String charset) {
        this.basicSetting = Require.notNull("basicSetting", basicSetting);
        this.applicationName = Require.notBlank("applicationName", applicationName);
        this.charset = Require.notBlank("charset", charset);
    }

    public static CoreModel from(
            final BasicSetting basicSetting,
            final String applicationName,
            final ThymeleafProperties thymeleafProperties) {
        return new CoreModel(
                basicSetting,
                applicationName,
                Optional.ofNullable(thymeleafProperties)
                        .map(ThymeleafProperties::getEncoding)
                        .orElse(StandardCharsets.UTF_8)
                        .name());
    }

    public String getApplicationName() {
        return applicationName;
    }

    public String getCharset() {
        return charset;
    }

    public String getLanguage() {
        return language;
    }

    @Override
    public String toString() {
        return new ToStringCreator(this)
                .append("applicationName", getApplicationName())
                .append("charset", getCharset())
                .append("language", getLanguage())
                .toString();
    }

    public static int shiftPage(final Integer page) {
        return page == null || page <= 0 ? 0 : page - 1;
    }

    public static int showPage(final Integer page) {
        return page == null || page <= 1 ? 1 : page;
    }

    public static int showSize(final Integer size, final int fallback) {
        return size == null || size <= 0 ? fallback : size;
    }

    public int showSize(final Integer size) {
        return showSize(size, basicSetting.getPaginationSize());
    }

    public String pageLink(final Integer page, final Integer size) {
        return "/page/%d/size/%d".formatted(showPage(page), showSize(size));
    }

    public static String formatTime(final OffsetDateTime stamp, final String pattern) {
        final var formatter = DateTimeFormatter.ofPattern(pattern);
        return stamp.format(formatter);
    }

    public String formatTime(final OffsetDateTime stamp) {
        return formatTime(stamp, basicSetting.getTimeFormat());
    }
}
