package su.toor.topical.shared.misc;

import static net.logstash.logback.argument.StructuredArguments.value;

import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;
import org.eclipse.paho.mqttv5.common.util.MqttTopicValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.lang.NonNull;

public final class Require {
    private Require() {}

    private static final Logger LOG = LoggerFactory.getLogger(Require.class);

    public static class RequireException extends RuntimeException {
        public RequireException(final String message) {
            super(message);
        }
    }

    static <T> T condition(final Predicate<T> check, final String message, final T value)
            throws IllegalArgumentException, RequireException {
        if (Objects.isNull(check)) {
            throw new IllegalArgumentException("check is null");
        }
        if (Optional.ofNullable(message).filter(Predicate.not(String::isBlank)).isEmpty()) {
            throw new IllegalArgumentException("message is null, empty or blank");
        }
        if (!check.test(value)) {
            final var exception = new RequireException(message);
            LOG.error("{} [{}]", value("message", message), value("value", value), exception);
            throw exception;
        }
        return value;
    }

    @NonNull public static <T> T notNull(final String fieldName, final T value) throws RequireException {
        return condition(Objects::nonNull, fieldName + " is null", value);
    }

    @NonNull public static String notEmpty(final String fieldName, final String value) throws RequireException {
        return condition(Predicate.not(String::isEmpty), fieldName + " is empty", notNull(fieldName, value));
    }

    @NonNull public static String notBlank(final String fieldName, final String value) throws RequireException {
        return condition(Predicate.not(String::isBlank), fieldName + " is blank", notEmpty(fieldName, value));
    }

    @NonNull public static <T extends Number> T greaterEqualThan(final String fieldName, final T value, final T bound)
            throws RequireException {
        final var lower = notNull(fieldName + ".bound", bound);
        return condition(
                val -> val.doubleValue() >= lower.doubleValue(),
                fieldName + " is not greater or equal than " + lower,
                notNull(fieldName, value));
    }

    private static boolean validTopic(final String name, final boolean forListening) {
        try {
            MqttTopicValidator.validate(name, forListening, forListening);
        } catch (final RuntimeException ex) {
            return false;
        }
        return true;
    }

    @NonNull public static String validListenTopic(final String name) throws RequireException {
        return condition(topic -> validTopic(topic, true), "listen topic is invalid", notBlank("listen topic", name));
    }

    @NonNull public static String validMessageTopic(final String name) throws RequireException {
        return condition(
                topic -> validTopic(topic, false), "message topic is invalid", notBlank("message topic", name));
    }
}
