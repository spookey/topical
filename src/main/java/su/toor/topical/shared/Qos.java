package su.toor.topical.shared;

import java.util.EnumSet;
import java.util.Optional;
import org.springframework.lang.Nullable;

public enum Qos {
    ZERO,
    ONE,
    TWO;

    @Nullable public static Qos of(final Integer value) {
        return Optional.ofNullable(value)
                .flatMap(val -> EnumSet.allOf(Qos.class).stream()
                        .filter(qos -> val == qos.ordinal())
                        .findFirst())
                .orElse(null);
    }
}
