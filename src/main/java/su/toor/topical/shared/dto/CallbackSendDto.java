package su.toor.topical.shared.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.UUID;
import su.toor.topical.shared.misc.Require;

public record CallbackSendDto(
        @JsonProperty("topic") String topic,
        @JsonProperty("uuid") UUID uuid,
        @JsonProperty("payload") PayloadDto payload) {

    public CallbackSendDto {
        Require.notEmpty("topic", topic);
        Require.notNull("uuid", uuid);
        Require.notNull("payload", payload);
    }
}
