package su.toor.topical.shared.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.time.Duration;
import su.toor.topical.shared.misc.Require;

public record ReportDto(
        @JsonProperty("title") String title,
        @JsonProperty("sent") Long sent,
        @JsonProperty("collected") Long collected,
        @JsonProperty("purged") Long purged,
        @JsonProperty("uptime") Duration uptime) {

    public ReportDto {
        Require.notBlank("title", title);
        Require.greaterEqualThan("sent", sent, 0L);
        Require.greaterEqualThan("collected", collected, 0L);
        Require.greaterEqualThan("purged", purged, 0L);
        Require.notNull("uptime", uptime);
    }

    @JsonIgnore
    public ReportDto(
            final String title, final Long sent, final Long collected, final Long purged, final Long uptimeMillis) {
        this(
                title,
                sent,
                collected,
                purged,
                Duration.ofMillis(Require.greaterEqualThan("uptimeMillis", uptimeMillis, 0L)));
    }
}
