package su.toor.topical.shared.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import org.springframework.lang.Nullable;
import su.toor.topical.shared.misc.Require;

public record PayloadDto(
        @JsonProperty("base64") @Schema(name = "base64", description = "Raw value in base64") String base64,
        @JsonProperty("string")
                @Schema(name = "string", description = "Value as string - if parseable", nullable = true)
                @Nullable String string,
        @JsonProperty("json")
                @Schema(name = "json", description = "Value as json - if parseable", nullable = true)
                @Nullable Object json,
        @JsonProperty("number")
                @Schema(name = "number", description = "Value as integer - if parseable", nullable = true)
                @Nullable Integer number,
        @JsonProperty("fraction")
                @Schema(name = "fraction", description = "Value as float - if parseable", nullable = true)
                @Nullable Double fraction,
        @JsonProperty("bool")
                @Schema(name = "bool", description = "Value as boolean - if parseable", nullable = true)
                @Nullable Boolean bool) {

    public PayloadDto {
        Require.notNull("base64", base64);
    }
}
