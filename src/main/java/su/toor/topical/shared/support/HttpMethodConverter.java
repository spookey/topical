package su.toor.topical.shared.support;

import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;
import org.springframework.http.HttpMethod;
import org.springframework.lang.Nullable;

@Converter(autoApply = true)
public class HttpMethodConverter implements AttributeConverter<HttpMethod, String> {

    private static final HttpMethodToString HTTP_METHOD_TO_STRING = new HttpMethodToString();
    private static final StringToHttpMethod STRING_TO_HTTP_METHOD = new StringToHttpMethod();

    @Override
    @Nullable public String convertToDatabaseColumn(final HttpMethod attribute) {
        return HTTP_METHOD_TO_STRING.convert(attribute);
    }

    @Override
    @Nullable public HttpMethod convertToEntityAttribute(final String text) {
        return STRING_TO_HTTP_METHOD.convert(text);
    }
}
