package su.toor.topical.shared.support;

import com.fasterxml.jackson.databind.util.StdConverter;
import java.util.Optional;
import org.springframework.http.HttpMethod;
import org.springframework.lang.Nullable;

public class HttpMethodToString extends StdConverter<HttpMethod, String> {

    @Override
    @Nullable public String convert(final HttpMethod attribute) {
        return Optional.ofNullable(attribute).map(HttpMethod::name).orElse(null);
    }
}
