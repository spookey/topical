package su.toor.topical.shared.support;

import com.fasterxml.jackson.databind.util.StdConverter;
import java.util.Optional;
import java.util.function.Predicate;
import org.springframework.http.HttpMethod;
import org.springframework.lang.Nullable;

public class StringToHttpMethod extends StdConverter<String, HttpMethod> {

    @Override
    @Nullable public HttpMethod convert(final String text) {
        return Optional.ofNullable(text)
                .filter(Predicate.not(String::isEmpty))
                .map(HttpMethod::valueOf)
                .orElse(null);
    }
}
