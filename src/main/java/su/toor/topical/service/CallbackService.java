package su.toor.topical.service;

import static net.logstash.logback.argument.StructuredArguments.keyValue;

import java.net.URISyntaxException;
import java.net.URL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import su.toor.topical.component.mqtt.PayloadParser;
import su.toor.topical.database.MessageEntity;
import su.toor.topical.shared.dto.CallbackSendDto;

@Service
public class CallbackService {

    private static final Logger LOG = LoggerFactory.getLogger(CallbackService.class);

    static final String FIELD_MESSAGE = "message";
    static final String FIELD_METHOD = "method";
    static final String FIELD_RESPONSE = "response";
    static final String FIELD_STATUS_CODE = "statusCode";
    static final String FIELD_TOPIC = "topic";
    static final String FIELD_URL = "url";

    private final RestTemplate restTemplate;
    private final EntityService entityService;
    private final PayloadParser payloadParser;

    @Autowired
    public CallbackService(
            final RestTemplate restTemplate, final EntityService entityService, final PayloadParser payloadParser) {
        this.restTemplate = restTemplate;
        this.entityService = entityService;
        this.payloadParser = payloadParser;
    }

    public void emit(final MessageEntity messageEntity) {
        final var topicEntity = messageEntity.getTopicEntity();
        entityService.allActiveCallbacks(topicEntity).forEach(callbackEntity -> {
            final var topic = topicEntity.getName();
            final var url = callbackEntity.getUrl();
            final var method = callbackEntity.getMethod();

            final var callbackSendDto = new CallbackSendDto(
                    topic, callbackEntity.getUuid(), payloadParser.parse(messageEntity.getPayload()));
            LOG.info(
                    "sending callback [{}] [{}] [{}]",
                    keyValue(FIELD_TOPIC, topic),
                    keyValue(FIELD_METHOD, method),
                    keyValue(FIELD_URL, url));
            send(url, method, callbackSendDto);
        });
    }

    void send(final URL url, final HttpMethod method, final CallbackSendDto callbackSendDto) {
        final var httpEntity = new HttpEntity<>(callbackSendDto);

        try {
            final var response = restTemplate.exchange(url.toURI(), method, httpEntity, String.class);
            LOG.info(
                    "got callback response [{}] [{}] [{}] [{}]",
                    keyValue(FIELD_METHOD, method),
                    keyValue(FIELD_URL, url),
                    keyValue(FIELD_STATUS_CODE, response.getStatusCode()),
                    keyValue(FIELD_RESPONSE, response.getBody()));

        } catch (final RestClientException | URISyntaxException ex) {
            LOG.error(
                    "failed to send callback [{}] [{}] [{}]",
                    keyValue(FIELD_METHOD, method),
                    keyValue(FIELD_URL, url),
                    keyValue(FIELD_MESSAGE, ex.getMessage()));
        }
    }
}
