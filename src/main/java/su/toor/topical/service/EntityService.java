package su.toor.topical.service;

import static net.logstash.logback.argument.StructuredArguments.keyValue;

import java.net.URL;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpMethod;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import su.toor.topical.application.setting.BasicSetting;
import su.toor.topical.database.CallbackEntity;
import su.toor.topical.database.CallbackRepository;
import su.toor.topical.database.ListenEntity;
import su.toor.topical.database.ListenRepository;
import su.toor.topical.database.MessageEntity;
import su.toor.topical.database.MessageRepository;
import su.toor.topical.database.TopicEntity;
import su.toor.topical.database.TopicRepository;
import su.toor.topical.shared.Qos;

@Service
public class EntityService {

    private static final Logger LOG = LoggerFactory.getLogger(EntityService.class);

    static final String FIELD_CALLBACK = "callback";
    static final String FIELD_LISTEN = "listen";
    static final String FIELD_MESSAGE = "message";
    static final String FIELD_TOPIC = "topic";

    static final int MAX_PAGINATION_SIZE = 512;

    private final BasicSetting basicSetting;
    private final TopicRepository topicRepository;
    private final MessageRepository messageRepository;
    private final ListenRepository listenRepository;
    private final CallbackRepository callbackRepository;

    @Autowired
    public EntityService(
            final BasicSetting basicSetting,
            final TopicRepository topicRepository,
            final MessageRepository messageRepository,
            final ListenRepository listenRepository,
            final CallbackRepository callbackRepository) {
        this.basicSetting = basicSetting;
        this.topicRepository = topicRepository;
        this.messageRepository = messageRepository;
        this.listenRepository = listenRepository;
        this.callbackRepository = callbackRepository;
    }

    Pageable pagination(
            @Nullable final Integer page,
            @Nullable final Integer size,
            @NonNull final Direction direction,
            @NonNull final String... properties) {
        final var currPage = page == null || page <= 0 ? 0 : page;
        final var pageSize =
                size == null || size <= 0 ? basicSetting.getPaginationSize() : Math.min(size, MAX_PAGINATION_SIZE);

        return PageRequest.ofSize(pageSize).withPage(currPage).withSort(direction, properties);
    }

    public long countTopics() {
        return topicRepository.count();
    }

    public long countMessages() {
        return messageRepository.count();
    }

    public long countListeners() {
        return listenRepository.count();
    }

    public long countCallbacks() {
        return callbackRepository.count();
    }

    public Optional<TopicEntity> findTopic(final UUID uuid) {
        return topicRepository.findByUuid(uuid);
    }

    public Optional<TopicEntity> findTopic(final String name) {
        return topicRepository.findByName(name);
    }

    public Page<TopicEntity> allTopics(final Integer page, final Integer size) {
        return topicRepository.findAll(pagination(page, size, Direction.ASC, "name"));
    }

    public TopicEntity ensureTopic(final String name) {
        return findTopic(name).orElseGet(() -> {
            final var topicEntity = TopicEntity.with(name);
            LOG.info("creating new topic [{}]", keyValue(FIELD_TOPIC, topicEntity));
            return topicRepository.save(topicEntity);
        });
    }

    public boolean deleteTopic(final UUID uuid) {
        return findTopic(uuid)
                .map(topicEntity -> {
                    LOG.info("removing topic [{}]", keyValue(FIELD_TOPIC, topicEntity));
                    topicRepository.delete(topicEntity);
                    return true;
                })
                .orElse(false);
    }

    public Optional<MessageEntity> findMessage(final UUID uuid) {
        return messageRepository.findByUuid(uuid);
    }

    public Page<MessageEntity> allMessages(final Integer page, final Integer size) {
        return messageRepository.findAll(pagination(page, size, Direction.DESC, "created"));
    }

    public Page<MessageEntity> allMessagesAfter(final OffsetDateTime stamp, final Integer page, final Integer size) {
        return messageRepository.findAllByCreatedAfter(stamp, pagination(page, size, Direction.DESC, "created"));
    }

    public Page<MessageEntity> recentMessages(final Integer page, final Integer size) {
        final var now = OffsetDateTime.now(ZoneOffset.UTC);
        final var stamp = now.minusHours(basicSetting.getRecentHours());
        return allMessagesAfter(stamp, page, size);
    }

    public Set<MessageEntity> allMessagesBefore(final OffsetDateTime stamp) {
        return messageRepository.findAllByCreatedBefore(stamp);
    }

    public MessageEntity createMessage(
            final String topic,
            final byte[] payload,
            final Qos qos,
            final Boolean retained,
            final Boolean duplicate,
            final Integer messageId) {
        final var topicEntity = ensureTopic(topic);
        final var messageEntity = MessageEntity.with(topicEntity, payload, qos, retained, duplicate, messageId);
        LOG.info(
                "creating new message [{}] [{}]",
                keyValue(FIELD_TOPIC, topicEntity),
                keyValue(FIELD_MESSAGE, messageEntity));

        return messageRepository.save(messageEntity);
    }

    public boolean deleteMessage(final UUID uuid) {
        return findMessage(uuid)
                .map(messageEntity -> {
                    LOG.info("removing message [{}]", keyValue(FIELD_MESSAGE, messageEntity));
                    messageRepository.delete(messageEntity);
                    return true;
                })
                .orElse(false);
    }

    public Optional<ListenEntity> findListener(final UUID uuid) {
        return listenRepository.findByUuid(uuid);
    }

    public List<ListenEntity> allListeners() {
        return listenRepository.findAll();
    }

    public Page<ListenEntity> allListeners(final Integer page, final Integer size) {
        return listenRepository.findAll(pagination(page, size, Direction.ASC, "filter"));
    }

    public ListenEntity createListener(final String filter, final Qos qos, final Boolean enabled) {
        final var listenEntity = ListenEntity.with(filter, qos, enabled);
        LOG.info("creating new listener [{}]", keyValue(FIELD_LISTEN, listenEntity));

        return listenRepository.save(listenEntity);
    }

    public Optional<ListenEntity> modifyListener(
            final UUID uuid, final String filter, final Qos qos, final Boolean enabled) {
        return findListener(uuid)
                .map(listenEntity -> listenEntity.setFilter(filter).setQos(qos).setEnabled(enabled))
                .map(listenEntity -> {
                    LOG.info("modifying listener [{}]", keyValue(FIELD_LISTEN, listenEntity));
                    return listenRepository.save(listenEntity);
                });
    }

    public boolean deleteListener(final UUID uuid) {
        return findListener(uuid)
                .map(listenEntity -> {
                    LOG.info("removing listener [{}]", keyValue(FIELD_LISTEN, listenEntity));
                    listenRepository.delete(listenEntity);
                    return true;
                })
                .orElse(false);
    }

    public Optional<CallbackEntity> findCallback(final UUID uuid) {
        return callbackRepository.findByUuid(uuid);
    }

    public List<CallbackEntity> allActiveCallbacks(final TopicEntity topicEntity) {
        return callbackRepository.findAllByTopicEntityAndEnabled(topicEntity, true);
    }

    public Page<CallbackEntity> allCallbacks(final Integer page, final Integer size) {
        return callbackRepository.findAll(pagination(page, size, Direction.ASC, "url"));
    }

    public CallbackEntity createCallback(
            final String topic, final HttpMethod method, final URL url, final Boolean enabled) {
        final var topicEntity = ensureTopic(topic);
        final var callbackEntity = CallbackEntity.with(topicEntity, method, url, enabled);
        LOG.info("creating new callback [{}]", keyValue(FIELD_CALLBACK, callbackEntity));

        return callbackRepository.save(callbackEntity);
    }

    public Optional<CallbackEntity> modifyCallback(
            final UUID uuid, final HttpMethod method, final URL url, final Boolean enabled) {
        return findCallback(uuid)
                .map(callbackEntity ->
                        callbackEntity.setMethod(method).setUrl(url).setEnabled(enabled))
                .map(callbackEntity -> {
                    LOG.info("modifying callback [{}]", keyValue(FIELD_CALLBACK, callbackEntity));
                    return callbackRepository.save(callbackEntity);
                });
    }

    public boolean deleteCallback(final UUID uuid) {
        return findCallback(uuid)
                .map(callbackEntity -> {
                    LOG.info("removing callback [{}]", keyValue(FIELD_CALLBACK, callbackEntity));
                    callbackRepository.delete(callbackEntity);
                    return true;
                })
                .orElse(false);
    }
}
