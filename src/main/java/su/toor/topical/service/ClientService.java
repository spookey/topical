package su.toor.topical.service;

import static net.logstash.logback.argument.StructuredArguments.keyValue;

import org.eclipse.paho.mqttv5.client.MqttAsyncClient;
import org.eclipse.paho.mqttv5.common.MqttException;
import org.eclipse.paho.mqttv5.common.MqttMessage;
import org.eclipse.paho.mqttv5.common.MqttSubscription;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import su.toor.topical.application.setting.mqtt.BirthStatusSetting;
import su.toor.topical.component.mqtt.MessageComposer;
import su.toor.topical.database.ListenEntity;

@Service(value = "nonInitializedClient")
public class ClientService {

    private static final Logger LOG = LoggerFactory.getLogger(ClientService.class);

    static final String FIELD_FILTER = "filter";
    static final String FIELD_MESSAGE = "message";
    static final String FIELD_QOS = "qos";
    static final String FIELD_SUBSCRIPTION = "subscription";
    static final String FIELD_TOPIC = "topic";

    private final EntityService entityService;
    private final MqttAsyncClient mqttAsyncClient;

    ClientService(final EntityService entityService, final MqttAsyncClient mqttAsyncClient) {
        this.entityService = entityService;
        this.mqttAsyncClient = mqttAsyncClient;
    }

    @Bean
    public static ClientService clientService(
            final EntityService entityService,
            final MqttAsyncClient mqttAsyncClient,
            final BirthStatusSetting birthStatusSetting) {
        final var clientService = new ClientService(entityService, mqttAsyncClient);

        clientService.refreshSubscriptions();
        clientService.sendBirthMessage(birthStatusSetting);
        return clientService;
    }

    boolean subscribe(final MqttSubscription mqttSubscription) {
        if (!mqttAsyncClient.isConnected()) {
            LOG.error("cannot subscribe - not connected [{}]", keyValue(FIELD_SUBSCRIPTION, mqttSubscription));
            return false;
        }

        LOG.info("adding subscription [{}]", keyValue(FIELD_SUBSCRIPTION, mqttSubscription));

        try {
            final var token = mqttAsyncClient.subscribe(mqttSubscription);
            token.waitForCompletion();
        } catch (final MqttException ex) {
            LOG.error("subscription failed [{}]", keyValue(FIELD_SUBSCRIPTION, mqttSubscription), ex);
            return false;
        }

        return true;
    }

    boolean subscribe(final ListenEntity listenEntity) {
        final var filter = listenEntity.getFilter();
        final var qos = listenEntity.getQos();

        if (Boolean.FALSE.equals(listenEntity.isEnabled())) {
            LOG.info(
                    "ignoring disabled subscription [{}] [{}]",
                    keyValue(FIELD_FILTER, filter),
                    keyValue(FIELD_QOS, qos));
            return true;
        }

        return subscribe(new MqttSubscription(filter, qos.ordinal()));
    }

    public boolean refreshSubscriptions() {
        LOG.info("refreshing subscriptions");
        return entityService.allListeners().stream().allMatch(this::subscribe);
    }

    public boolean send(@Nullable final String topic, @Nullable final MqttMessage message) {
        if (topic == null || message == null) {
            LOG.warn(
                    "empty topic or message passed [{}] [{}]",
                    keyValue(FIELD_TOPIC, topic),
                    keyValue(FIELD_MESSAGE, message));
            return false;
        }

        if (!mqttAsyncClient.isConnected()) {
            LOG.error(
                    "cannot publish - not connected [{}] [{}]",
                    keyValue(FIELD_TOPIC, topic),
                    keyValue(FIELD_MESSAGE, message));
            return false;
        }

        LOG.info("publishing message [{}] [{}]", keyValue(FIELD_TOPIC, topic), keyValue(FIELD_MESSAGE, message));

        try {
            final var token = mqttAsyncClient.publish(topic, message);
            token.waitForCompletion();
        } catch (final MqttException ex) {
            LOG.error("publish failed [{}] [{}]", keyValue(FIELD_TOPIC, topic), keyValue(FIELD_MESSAGE, message), ex);
            return false;
        }

        return true;
    }

    boolean sendBirthMessage(final BirthStatusSetting birthStatusSetting) {
        if (!birthStatusSetting.isEnabled()) {
            return false;
        }

        final var topic = birthStatusSetting.getTopic();
        final var message = MessageComposer.make(birthStatusSetting);

        LOG.info("sending birth message [{}]", keyValue(FIELD_TOPIC, topic));

        return send(topic, message);
    }
}
