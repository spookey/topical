package su.toor.topical.component;

import java.util.concurrent.atomic.AtomicLong;
import org.springframework.stereotype.Component;

@Component
public class CounterBean {

    private final AtomicLong mqttConnects = new AtomicLong(0L);
    private final AtomicLong mqttDisconnects = new AtomicLong(0L);
    private final AtomicLong mqttErrors = new AtomicLong(0L);
    private final AtomicLong sentMessages = new AtomicLong(0L);
    private final AtomicLong collectedMessages = new AtomicLong(0L);
    private final AtomicLong purgedMessages = new AtomicLong(0L);

    public Long getMqttConnects() {
        return mqttConnects.get();
    }

    public Long incMqttConnects() {
        return mqttConnects.incrementAndGet();
    }

    public Long getMqttDisconnects() {
        return mqttDisconnects.get();
    }

    public Long incMqttDisconnects() {
        return mqttDisconnects.incrementAndGet();
    }

    public Long getMqttErrors() {
        return mqttErrors.get();
    }

    public Long incMqttErrors() {
        return mqttErrors.incrementAndGet();
    }

    public Long getSentMessages() {
        return sentMessages.get();
    }

    public Long incSentMessages() {
        return sentMessages.incrementAndGet();
    }

    public Long getCollectedMessages() {
        return collectedMessages.get();
    }

    public Long incCollectedMessages() {
        return collectedMessages.incrementAndGet();
    }

    public Long getPurgedMessages() {
        return purgedMessages.get();
    }

    public Long incPurgedMessages() {
        return purgedMessages.incrementAndGet();
    }
}
