package su.toor.topical.component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.json.JsonMapper;
import java.lang.management.ManagementFactory;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.concurrent.atomic.AtomicLong;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import su.toor.topical.application.setting.BasicSetting;
import su.toor.topical.application.setting.mqtt.ReportStatusSetting;
import su.toor.topical.component.mqtt.MessageComposer;
import su.toor.topical.service.ClientService;
import su.toor.topical.service.EntityService;
import su.toor.topical.shared.dto.ReportDto;
import su.toor.topical.shared.misc.Require;

@EnableScheduling
@Component
public class TimerBean {

    private static final Logger LOG = LoggerFactory.getLogger(TimerBean.class);

    static final long INTERVAL_RUN_INITIAL = 1000L * 10L;
    static final long INTERVAL_RUN_REGULAR = 1000L * 5L;

    private final BasicSetting basicSetting;
    private final ReportStatusSetting reportStatusSetting;
    private final ClientService clientService;
    private final EntityService entityService;
    private final CounterBean counterBean;
    private final JsonMapper jsonMapper;

    final AtomicLong reporting = new AtomicLong(0L);

    @Autowired
    public TimerBean(
            final BasicSetting basicSetting,
            final ReportStatusSetting reportStatusSetting,
            final ClientService clientService,
            final EntityService entityService,
            final CounterBean counterBean,
            final JsonMapper jsonMapper) {
        this.basicSetting = basicSetting;
        this.reportStatusSetting = reportStatusSetting;
        this.entityService = entityService;
        this.clientService = clientService;
        this.counterBean = counterBean;
        this.jsonMapper = jsonMapper;
    }

    boolean shouldReport() {
        final var interval = 1000L * reportStatusSetting.getIntervalSeconds();
        if (interval <= 0) {
            return false;
        }
        final var curr = System.currentTimeMillis();
        final var last = reporting.get();
        final var diff = curr - last;
        if (diff < interval) {
            return false;
        }
        reporting.set(curr);
        return true;
    }

    String reportJson() {
        final var title = reportStatusSetting.getMessage();
        final var runtime = ManagementFactory.getRuntimeMXBean();

        try {
            final var report = new ReportDto(
                    title,
                    counterBean.getSentMessages(),
                    counterBean.getCollectedMessages(),
                    counterBean.getPurgedMessages(),
                    runtime.getUptime());
            return jsonMapper.writeValueAsString(report);
        } catch (final JsonProcessingException | Require.RequireException ex) {
            LOG.error("failed to generate report", ex);
        }
        return title;
    }

    void sendReport() {
        if (!shouldReport()) {
            return;
        }
        final var status = reportJson();
        final var message = MessageComposer.make(reportStatusSetting, status);

        clientService.send(reportStatusSetting.getTopic(), message);
    }

    void purgeMessages() {
        final var keepDays = (int) basicSetting.getKeepDays();
        if (keepDays <= 0) {
            return;
        }

        final var now = OffsetDateTime.now(ZoneOffset.UTC);
        final var stamp = now.minusDays(keepDays);

        entityService.allMessagesBefore(stamp).forEach(messageEntity -> {
            entityService.deleteMessage(messageEntity.getUuid());
            counterBean.incPurgedMessages();
        });
    }

    @Scheduled(initialDelay = INTERVAL_RUN_INITIAL, fixedRate = INTERVAL_RUN_REGULAR)
    public void repeating() {
        purgeMessages();
        sendReport();
    }
}
