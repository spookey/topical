package su.toor.topical.component.mqtt;

import static net.logstash.logback.argument.StructuredArguments.keyValue;

import org.eclipse.paho.mqttv5.client.MqttAsyncClient;
import org.eclipse.paho.mqttv5.client.MqttConnectionOptions;
import org.eclipse.paho.mqttv5.client.persist.MemoryPersistence;
import org.eclipse.paho.mqttv5.common.MqttException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanInstantiationException;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;
import su.toor.topical.application.setting.MqttSetting;
import su.toor.topical.application.setting.mqtt.WillStatusSetting;

@Component
public class ClientCreator {

    private static final Logger LOG = LoggerFactory.getLogger(ClientCreator.class);

    static final String FIELD_USERNAME = "username";
    static final String FIELD_PASSWORD = "password";
    static final String FIELD_TOPIC = "topic";
    static final String FIELD_MESSAGE = "message";
    static final String FIELD_SERVER_URI = "serverUri";

    @Bean
    public MqttConnectionOptions mqttConnectionOptions(
            final MqttSetting mqttSetting, final WillStatusSetting willStatusSetting) {
        final var options = new MqttConnectionOptions();
        options.setCleanStart(mqttSetting.isCleanStart());
        options.setAutomaticReconnect(mqttSetting.isAutoReconnect());
        options.setUseSubscriptionIdentifiers(true);
        options.setRequestResponseInfo(true);
        options.setRequestProblemInfo(true);

        if (mqttSetting.hasCredentials()) {
            LOG.info(
                    "using credentials [{}] [{}]",
                    keyValue(FIELD_USERNAME, mqttSetting.getUsername()),
                    keyValue(FIELD_PASSWORD, mqttSetting.getPassword()));

            options.setUserName(mqttSetting.getUsername());
            options.setPassword(mqttSetting.getPasswordBytes());
        }

        if (willStatusSetting.isEnabled()) {
            final var willMessage = MessageComposer.make(willStatusSetting);
            final var willTopic = willStatusSetting.getTopic();

            LOG.info("register will [{}] [{}]", keyValue(FIELD_TOPIC, willTopic), keyValue(FIELD_MESSAGE, willMessage));

            options.setWill(willTopic, willMessage);
        }

        return options;
    }

    @Bean(value = "basicMqttAsyncClient")
    public MqttAsyncClient basicMqttAsyncClient(final MqttSetting mqttSetting, final RunnerCallback runnerCallback) {
        try {
            final var mqttAsyncClient =
                    new MqttAsyncClient(mqttSetting.getServerUri(), mqttSetting.getClientId(), new MemoryPersistence());
            mqttAsyncClient.setCallback(runnerCallback);

            return mqttAsyncClient;
        } catch (final IllegalArgumentException | MqttException ex) {
            throw new BeanInstantiationException(ClientCreator.class, "client creation failed", ex);
        }
    }

    @Bean
    @Primary
    public MqttAsyncClient mqttAsyncClient(
            final MqttAsyncClient basicMqttAsyncClient, final MqttConnectionOptions mqttConnectionOptions) {
        LOG.info("connecting to mqtt server [{}]", keyValue(FIELD_SERVER_URI, basicMqttAsyncClient.getServerURI()));

        try {
            final var token = basicMqttAsyncClient.connect(mqttConnectionOptions);
            token.waitForCompletion();

            return basicMqttAsyncClient;
        } catch (final MqttException ex) {
            throw new BeanInstantiationException(ClientCreator.class, "client connection failed", ex);
        }
    }
}
