package su.toor.topical.component.mqtt;

import static net.logstash.logback.argument.StructuredArguments.keyValue;

import java.util.Map;
import java.util.concurrent.CompletableFuture;
import org.eclipse.paho.mqttv5.client.IMqttToken;
import org.eclipse.paho.mqttv5.client.MqttCallback;
import org.eclipse.paho.mqttv5.client.MqttDisconnectResponse;
import org.eclipse.paho.mqttv5.common.MqttException;
import org.eclipse.paho.mqttv5.common.MqttMessage;
import org.eclipse.paho.mqttv5.common.packet.MqttProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import su.toor.topical.component.CounterBean;
import su.toor.topical.service.CallbackService;
import su.toor.topical.service.EntityService;
import su.toor.topical.shared.Qos;

@Component
public class RunnerCallback implements MqttCallback {

    private static final Logger LOG = LoggerFactory.getLogger(RunnerCallback.class);

    private final CounterBean counterBean;
    private final EntityService entityService;
    private final CallbackService callbackService;

    @Autowired
    public RunnerCallback(
            final CounterBean counterBean, final EntityService entityService, final CallbackService callbackService) {
        this.counterBean = counterBean;
        this.entityService = entityService;
        this.callbackService = callbackService;
    }

    @Override
    public void deliveryComplete(final IMqttToken token) {
        LOG.info("delivery complete [{}]", keyValue("token", token));
        counterBean.incSentMessages();
    }

    CompletableFuture<Void> collectMessage(final String topic, final MqttMessage mqttMessage) {
        counterBean.incCollectedMessages();

        return CompletableFuture.supplyAsync(() -> entityService.createMessage(
                        topic,
                        mqttMessage.getPayload(),
                        Qos.of(mqttMessage.getQos()),
                        mqttMessage.isRetained(),
                        mqttMessage.isDuplicate(),
                        mqttMessage.getId()))
                .thenAcceptAsync(callbackService::emit);
    }

    @Override
    public void messageArrived(final String topic, final MqttMessage mqttMessage) {
        LOG.info("message arrived [{}] [{}]", keyValue("topic", topic), keyValue("mqttMessage", mqttMessage));
        collectMessage(topic, mqttMessage);
    }

    @Override
    public void connectComplete(final boolean isReconnect, final String serverUri) {
        LOG.info("connect complete [{}] [{}]", keyValue("isReconnect", isReconnect), keyValue("serverUri", serverUri));
        counterBean.incMqttConnects();
    }

    @Override
    public void authPacketArrived(final int reasonCode, final MqttProperties properties) {
        final var reason = Map.of(
                        0, "Success",
                        24, "Continue authentication",
                        25, "Reauthenticate")
                .getOrDefault(reasonCode, "¯\\_(ツ)_/¯");

        LOG.info(
                "auth packet arrived [{}] [{}] [{}]",
                keyValue("reasonCode", reasonCode),
                keyValue("reason", reason),
                keyValue("properties", properties));
    }

    @Override
    public void disconnected(final MqttDisconnectResponse disconnectResponse) {
        LOG.error("disconnected [{}]", keyValue("response", disconnectResponse));
        counterBean.incMqttDisconnects();
    }

    @Override
    public void mqttErrorOccurred(final MqttException exception) {
        LOG.error("mqtt error occurred [{}]", keyValue("message", exception.getMessage()), exception);
        counterBean.incMqttErrors();
    }
}
