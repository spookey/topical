package su.toor.topical.component.mqtt;

import com.fasterxml.jackson.core.io.BigDecimalParser;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.json.JsonMapper;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.BufferOverflowException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.CodingErrorAction;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.function.Function;
import org.eclipse.paho.mqttv5.common.MqttMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import su.toor.topical.shared.Qos;
import su.toor.topical.shared.dto.PayloadDto;
import su.toor.topical.shared.misc.Require;

@Component
public class PayloadParser {

    private final JsonMapper jsonMapper;

    @Autowired
    public PayloadParser(final JsonMapper jsonMapper) {
        this.jsonMapper = jsonMapper
                .rebuild()
                .disable(
                        SerializationFeature.INDENT_OUTPUT,
                        SerializationFeature.WRITE_DATES_AS_TIMESTAMPS,
                        SerializationFeature.WRITE_DURATIONS_AS_TIMESTAMPS)
                .build();
    }

    static class Parser {
        private final JsonMapper jsonMapper;

        @NonNull private final byte[] payload;

        Parser(final JsonMapper jsonMapper, final byte[] payload) {
            this.jsonMapper = jsonMapper;
            this.payload = Require.notNull("payload", payload);
        }

        @NonNull public String asBase64() {
            return Base64.getEncoder().encodeToString(payload);
        }

        @Nullable public String asString() {
            final var decoder = StandardCharsets.UTF_8
                    .newDecoder()
                    .onMalformedInput(CodingErrorAction.REPORT)
                    .onUnmappableCharacter(CodingErrorAction.REPORT);
            try {
                return decoder.decode(ByteBuffer.wrap(payload)).toString();
            } catch (final CharacterCodingException ex) {
                return null;
            }
        }

        @Nullable public Object asJson() {
            try {
                return jsonMapper.readValue(payload, new TypeReference<>() {});
            } catch (final IOException ex) {
                return null;
            }
        }

        @Nullable private <T> T bytes(final int size, final Function<ByteBuffer, T> action) {
            if (payload.length == 0) {
                return null;
            }
            try {
                final var buffer = ByteBuffer.allocate(size)
                        .order(ByteOrder.BIG_ENDIAN)
                        .put(payload)
                        .rewind();
                return action.apply(buffer);
            } catch (final BufferOverflowException ex) {
                return null;
            }
        }

        @Nullable public Integer asNumber() {
            return bytes(Integer.BYTES, buffer -> buffer.asIntBuffer().get());
        }

        @Nullable public Double asFraction() {
            return bytes(Double.BYTES, buffer -> buffer.asDoubleBuffer().get());
        }

        @Nullable public Boolean asBool() {
            final var string = asString();
            if ("false".equalsIgnoreCase(string)) {
                return false;
            }
            if ("true".equalsIgnoreCase(string)) {
                return true;
            }

            final var fraction = asFraction();
            if (fraction != null) {
                return fraction == 0d;
            }

            return null;
        }
    }

    public PayloadDto parse(final byte[] payload) {
        final var parser = new Parser(jsonMapper, payload);
        return new PayloadDto(
                parser.asBase64(),
                parser.asString(),
                parser.asJson(),
                parser.asNumber(),
                parser.asFraction(),
                parser.asBool());
    }

    @Nullable private static <T> T decimal(final String content, final Function<BigDecimal, T> action) {
        if (content == null) {
            return null;
        }
        try {
            return action.apply(BigDecimalParser.parse(content));
        } catch (final NumberFormatException | ArithmeticException ex) {
            return null;
        }
    }

    @Nullable static Integer numberFrom(final String content) {
        return decimal(content, BigDecimal::intValueExact);
    }

    @Nullable static Double fractionFrom(final String content) {
        return decimal(content, BigDecimal::doubleValue);
    }

    public MqttMessage compose(final String content, final Qos qos, final Boolean retained) {
        final var number = numberFrom(content);
        if (number != null) {
            return MessageComposer.make(number, qos, retained);
        }
        final var fraction = fractionFrom(content);
        if (fraction != null) {
            return MessageComposer.make(fraction, qos, retained);
        }
        return MessageComposer.make(content, qos, retained);
    }
}
