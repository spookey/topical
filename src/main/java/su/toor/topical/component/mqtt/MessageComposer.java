package su.toor.topical.component.mqtt;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.StandardCharsets;
import java.util.function.Consumer;
import java.util.function.Function;
import org.eclipse.paho.mqttv5.common.MqttMessage;
import su.toor.topical.application.setting.mqtt.AbstractStatus;
import su.toor.topical.application.setting.mqtt.ReportStatusSetting;
import su.toor.topical.shared.Qos;

public final class MessageComposer {
    private MessageComposer() {}

    public static final Qos DEFAULT_QOS = Qos.ZERO;
    public static final boolean DEFAULT_RETAINED = false;

    static <T> MqttMessage make(
            final T content, final Qos qos, final Boolean retained, final Function<T, byte[]> convert)
            throws IllegalArgumentException {
        if (content == null) {
            throw new IllegalArgumentException("null content passed");
        }
        return new MqttMessage(
                convert.apply(content),
                (qos == null ? DEFAULT_QOS : qos).ordinal(),
                retained == null ? DEFAULT_RETAINED : retained,
                null);
    }

    static byte[] numeric(final int size, final Consumer<ByteBuffer> populate) {
        final var buffer = ByteBuffer.allocate(size).order(ByteOrder.BIG_ENDIAN);
        populate.accept(buffer);
        return buffer.rewind().array();
    }

    public static MqttMessage make(final String content, final Qos qos, final Boolean retained) {
        return make(content, qos, retained, con -> con.getBytes(StandardCharsets.UTF_8));
    }

    public static MqttMessage make(final Integer number, final Qos qos, final Boolean retained) {
        return make(number, qos, retained, num -> numeric(Integer.BYTES, buf -> buf.putInt(num)));
    }

    public static MqttMessage make(final Double fraction, final Qos qos, final Boolean retained) {
        return make(fraction, qos, retained, fra -> numeric(Double.BYTES, buf -> buf.putDouble(fra)));
    }

    public static <T extends AbstractStatus<T>> MqttMessage make(final AbstractStatus<T> setting) {
        return make(setting.getMessage(), setting.getQos(), setting.isRetained());
    }

    public static MqttMessage make(final ReportStatusSetting setting, final String message) {
        return make(message, setting.getQos(), setting.isRetained());
    }
}
