package su.toor.topical.component;

import io.micrometer.core.instrument.Gauge;
import io.micrometer.core.instrument.MeterRegistry;
import java.util.function.ToDoubleFunction;
import java.util.stream.Stream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import su.toor.topical.service.EntityService;

@Component
public class MetricsBean {

    private static final Logger LOG = LoggerFactory.getLogger(MetricsBean.class);

    static final String METER_COUNT_NAME = "mqtt_count";
    static final String METER_STORE_NAME = "mqtt_store";
    static final String METER_COUNT_DESCRIPTION = "Mqtt action counters";
    static final String METER_STORE_DESCRIPTION = "Mqtt storage counters";
    static final String METER_TAG_CATEGORY = "category";
    static final String METER_TAG_TYPE = "type";
    static final String METER_CATEGORY_CONNECTION = "connection";
    static final String METER_CATEGORY_DATABASE = "database";
    static final String METER_CATEGORY_DELIVERY = "delivery";

    private final MeterRegistry meterRegistry;
    private final CounterBean counterBean;
    private final EntityService entityService;

    @Autowired
    public MetricsBean(
            final MeterRegistry meterRegistry, final CounterBean counterBean, final EntityService entityService) {
        this.meterRegistry = meterRegistry;
        this.counterBean = counterBean;
        this.entityService = entityService;
    }

    record Hold<T>(T instance, String name, String category, String description) {
        Gauge.Builder<T> build(final String type, final ToDoubleFunction<T> func) {
            return Gauge.builder(name, instance, func)
                    .tag(METER_TAG_TYPE, type)
                    .tag(METER_TAG_CATEGORY, category)
                    .description(description);
        }
    }

    void fromCounters() {
        LOG.info("registering counter gauges");

        final var holdCn =
                new Hold<>(counterBean, METER_COUNT_NAME, METER_CATEGORY_CONNECTION, METER_COUNT_DESCRIPTION);
        final var holdDv = new Hold<>(counterBean, METER_COUNT_NAME, METER_CATEGORY_DELIVERY, METER_COUNT_DESCRIPTION);

        Stream.of(
                        holdCn.build("connects", CounterBean::getMqttConnects),
                        holdCn.build("disconnects", CounterBean::getMqttDisconnects),
                        holdCn.build("errors", CounterBean::getMqttErrors),
                        holdDv.build("sent", CounterBean::getSentMessages),
                        holdDv.build("collected", CounterBean::getCollectedMessages),
                        holdDv.build("purged", CounterBean::getPurgedMessages))
                .forEach(gauge -> gauge.register(meterRegistry));
    }

    void fromDatabase() {
        LOG.info("registering database gauges");

        final var hold = new Hold<>(entityService, METER_STORE_NAME, METER_CATEGORY_DATABASE, METER_STORE_DESCRIPTION);

        Stream.of(
                        hold.build("topics", EntityService::countTopics),
                        hold.build("messages", EntityService::countMessages),
                        hold.build("listeners", EntityService::countListeners),
                        hold.build("callbacks", EntityService::countCallbacks))
                .forEach(gauge -> gauge.register(meterRegistry));
    }

    @EventListener(ApplicationReadyEvent.class)
    public void registerMetrics() {
        fromCounters();
        fromDatabase();
    }
}
