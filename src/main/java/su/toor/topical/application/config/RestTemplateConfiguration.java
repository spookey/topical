package su.toor.topical.application.config;

import org.springframework.boot.info.BuildProperties;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.http.HttpHeaders;
import org.springframework.lang.NonNull;
import org.springframework.web.client.RestTemplate;

@Configuration
public class RestTemplateConfiguration {

    @Bean
    @Primary
    public RestTemplate restTemplate(
            @NonNull final BuildProperties buildProperties, @NonNull final RestTemplateBuilder restTemplateBuilder) {
        return restTemplateBuilder
                .defaultHeader(
                        HttpHeaders.USER_AGENT,
                        "%s - %s".formatted(buildProperties.getName(), buildProperties.getVersion()))
                .build();
    }
}
