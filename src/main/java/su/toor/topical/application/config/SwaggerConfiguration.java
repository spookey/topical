package su.toor.topical.application.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.boot.info.BuildProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.lang.NonNull;

@Configuration
public class SwaggerConfiguration {

    public OpenAPI openAPI(@NonNull final BuildProperties buildProperties) {
        final var info = new Info()
                .title(buildProperties.getName())
                .version(buildProperties.getVersion())
                .description(buildProperties.get("description"));
        return new OpenAPI().info(info);
    }
}
