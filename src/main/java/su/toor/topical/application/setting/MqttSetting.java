package su.toor.topical.application.setting;

import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.util.Objects;
import java.util.Optional;
import java.util.Random;
import java.util.function.Predicate;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.style.ToStringCreator;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.validation.annotation.Validated;

@Configuration
@ConfigurationProperties(prefix = "mqtt")
@Validated
public class MqttSetting {

    private static final Random RND = new SecureRandom();

    static final String DEFAULT_SERVER_URI = "tcp://localhost:1833";
    static final String DEFAULT_CLIENT_ID_PREFIX = "topical";
    static final boolean DEFAULT_CLEAN_START = false;
    static final boolean DEFAULT_AUTO_RECONNECT = true;

    private String serverUri;
    private String clientId;
    private Boolean cleanStart;
    private Boolean autoReconnect;
    private String username;
    private String password;

    @NonNull public String getServerUri() {
        return Optional.ofNullable(serverUri)
                .filter(Predicate.not(String::isBlank))
                .orElse(DEFAULT_SERVER_URI);
    }

    public MqttSetting setServerUri(final String serverUri) {
        this.serverUri = serverUri;
        return this;
    }

    @NonNull public String getClientId() {
        return Optional.ofNullable(clientId)
                .filter(Predicate.not(String::isEmpty))
                .orElseGet(() -> {
                    final var suffix = RND.nextInt(100, 1000);
                    return "%s-%03d".formatted(DEFAULT_CLIENT_ID_PREFIX, suffix);
                });
    }

    public MqttSetting setClientId(final String clientId) {
        this.clientId = clientId;
        return this;
    }

    @NonNull public Boolean isCleanStart() {
        return Optional.ofNullable(cleanStart).orElse(DEFAULT_CLEAN_START);
    }

    public MqttSetting setCleanStart(final Boolean cleanStart) {
        this.cleanStart = cleanStart;
        return this;
    }

    @NonNull public Boolean isAutoReconnect() {
        return Optional.ofNullable(autoReconnect).orElse(DEFAULT_AUTO_RECONNECT);
    }

    public MqttSetting setAutoReconnect(final Boolean autoReconnect) {
        this.autoReconnect = autoReconnect;
        return this;
    }

    @Nullable public String getUsername() {
        return Optional.ofNullable(username)
                .filter(Predicate.not(String::isEmpty))
                .orElse(null);
    }

    public MqttSetting setUsername(final String username) {
        this.username = username;
        return this;
    }

    @Nullable public String getPassword() {
        return Optional.ofNullable(password)
                .filter(Predicate.not(String::isEmpty))
                .map(String::length)
                .map("*"::repeat)
                .orElse(null);
    }

    @Nullable public byte[] getPasswordBytes() {
        return Optional.ofNullable(password)
                .filter(Predicate.not(String::isEmpty))
                .map(pass -> pass.getBytes(StandardCharsets.UTF_8))
                .orElse(null);
    }

    public MqttSetting setPassword(final String password) {
        this.password = password;
        return this;
    }

    public boolean hasCredentials() {
        return Objects.nonNull(username) && Objects.nonNull(password);
    }

    @Override
    public String toString() {
        return new ToStringCreator(this)
                .append("serverUri", getServerUri())
                .append("clientId", getClientId())
                .append("cleanStart", isCleanStart())
                .append("autoReconnect", isAutoReconnect())
                .append("username", getUsername())
                .append("password", getPassword())
                .toString();
    }
}
