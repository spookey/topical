package su.toor.topical.application.setting.mqtt;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

@Configuration
@ConfigurationProperties(prefix = "mqtt.will-status")
@Validated
public class WillStatusSetting extends AbstractStatus<WillStatusSetting> {

    public WillStatusSetting() {
        super(DEFAULT_STATUS_TOPIC, DEFAULT_WILL_MESSAGE);
    }

    @Override
    protected WillStatusSetting get() {
        return this;
    }
}
