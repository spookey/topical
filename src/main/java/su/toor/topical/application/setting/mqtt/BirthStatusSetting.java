package su.toor.topical.application.setting.mqtt;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

@Configuration
@ConfigurationProperties(prefix = "mqtt.birth-status")
@Validated
public class BirthStatusSetting extends AbstractStatus<BirthStatusSetting> {

    public BirthStatusSetting() {
        super(DEFAULT_STATUS_TOPIC, DEFAULT_BIRTH_MESSAGE);
    }

    @Override
    protected BirthStatusSetting get() {
        return this;
    }
}
