package su.toor.topical.application.setting.mqtt;

import java.util.Optional;
import java.util.function.Predicate;
import org.springframework.core.style.ToStringCreator;
import org.springframework.lang.NonNull;
import org.springframework.validation.annotation.Validated;
import su.toor.topical.shared.Qos;
import su.toor.topical.shared.misc.Require;

@Validated
public abstract class AbstractStatus<T extends AbstractStatus<T>> {

    static final boolean DEFAULT_ENABLED = false;
    static final String DEFAULT_STATUS_TOPIC = "status/topical";
    static final String DEFAULT_REPORT_TOPIC = "status/topical/report";
    static final Qos DEFAULT_QOS = Qos.ZERO;
    static final boolean DEFAULT_RETAINED = false;
    static final String DEFAULT_BIRTH_MESSAGE = "topical online";
    static final String DEFAULT_REPORT_MESSAGE = "topical status";
    static final String DEFAULT_WILL_MESSAGE = "topical offline";

    private final String defaultTopic;
    private final String defaultMessage;

    private Boolean enabled;
    private String topic;
    private Qos qos;
    private Boolean retained;
    private String message;

    AbstractStatus(final String defaultTopic, final String defaultMessage) {
        this.defaultTopic = Require.notEmpty("defaultTopic", defaultTopic);
        this.defaultMessage = Require.notEmpty("defaultMessage", defaultMessage);
    }

    protected abstract T get();

    @NonNull public Boolean isEnabled() {
        return Optional.ofNullable(enabled).orElse(DEFAULT_ENABLED);
    }

    public T setEnabled(final Boolean enabled) {
        this.enabled = enabled;
        return get();
    }

    @NonNull public String getTopic() {
        return Optional.ofNullable(topic).orElse(defaultTopic);
    }

    public T setTopic(final String topic) {
        if (topic != null) {
            this.topic = Require.validMessageTopic(topic);
        }
        return get();
    }

    @NonNull public Qos getQos() {
        return Optional.ofNullable(qos).orElse(DEFAULT_QOS);
    }

    public T setQos(final Qos qos) {
        this.qos = qos;
        return get();
    }

    @NonNull public Boolean isRetained() {
        return Optional.ofNullable(retained).orElse(DEFAULT_RETAINED);
    }

    public T setRetained(final Boolean retained) {
        this.retained = retained;
        return get();
    }

    @NonNull public String getMessage() {
        return Optional.ofNullable(message)
                .filter(Predicate.not(String::isEmpty))
                .orElse(defaultMessage);
    }

    public T setMessage(final String message) {
        this.message = message;
        return get();
    }

    protected ToStringCreator toStringCreator() {
        return new ToStringCreator(this)
                .append("enabled", isEnabled())
                .append("topic", getTopic())
                .append("qos", getQos())
                .append("retained", isRetained())
                .append("message", getMessage());
    }

    @Override
    public String toString() {
        return toStringCreator().toString();
    }
}
