package su.toor.topical.application.setting.mqtt;

import java.util.Optional;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.style.ToStringCreator;
import org.springframework.lang.NonNull;
import org.springframework.validation.annotation.Validated;

@Configuration
@ConfigurationProperties(prefix = "mqtt.report-status")
@Validated
public class ReportStatusSetting extends AbstractStatus<ReportStatusSetting> {

    static final int DEFAULT_INTERVAL_SECONDS = 600;

    private Integer intervalSeconds;

    public ReportStatusSetting() {
        super(DEFAULT_REPORT_TOPIC, DEFAULT_REPORT_MESSAGE);
    }

    @Override
    protected ReportStatusSetting get() {
        return this;
    }

    @NonNull public Integer getIntervalSeconds() {
        return Optional.ofNullable(intervalSeconds).orElse(DEFAULT_INTERVAL_SECONDS);
    }

    public ReportStatusSetting setIntervalSeconds(final Integer intervalSeconds) {
        this.intervalSeconds = intervalSeconds;
        return get();
    }

    @Override
    protected ToStringCreator toStringCreator() {
        return super.toStringCreator().append("intervalSeconds", intervalSeconds);
    }
}
