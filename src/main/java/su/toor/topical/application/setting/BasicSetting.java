package su.toor.topical.application.setting;

import java.util.Optional;
import java.util.function.Predicate;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.style.ToStringCreator;
import org.springframework.lang.NonNull;
import org.springframework.validation.annotation.Validated;

@Configuration
@ConfigurationProperties(prefix = "basic")
@Validated
public class BasicSetting {

    static final int DEFAULT_KEEP_DAYS = 28;
    static final int DEFAULT_RECENT_HOURS = 24;
    static final int DEFAULT_PAGINATION_SIZE = 64;
    static final String DEFAULT_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss.SSS Z";

    private Integer keepDays;
    private Integer recentHours;
    private Integer paginationSize;
    private String timeFormat;

    @NonNull public Integer getKeepDays() {
        return Optional.ofNullable(keepDays).orElse(DEFAULT_KEEP_DAYS);
    }

    public BasicSetting setKeepDays(final Integer keepDays) {
        this.keepDays = keepDays;
        return this;
    }

    @NonNull public Integer getRecentHours() {
        return Optional.ofNullable(recentHours).orElse(DEFAULT_RECENT_HOURS);
    }

    public BasicSetting setRecentHours(final Integer recentHours) {
        this.recentHours = recentHours;
        return this;
    }

    @NonNull public Integer getPaginationSize() {
        return Optional.ofNullable(paginationSize).orElse(DEFAULT_PAGINATION_SIZE);
    }

    public BasicSetting setPaginationSize(final Integer paginationSize) {
        this.paginationSize = paginationSize;
        return this;
    }

    @NonNull public String getTimeFormat() {
        return Optional.ofNullable(timeFormat)
                .filter(Predicate.not(String::isBlank))
                .orElse(DEFAULT_TIME_FORMAT);
    }

    public BasicSetting setTimeFormat(final String timeFormat) {
        this.timeFormat = timeFormat;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringCreator(this)
                .append("keepDays", getKeepDays())
                .append("recentHours", getRecentHours())
                .append("paginationSize", getPaginationSize())
                .append("timeFormat", getTimeFormat())
                .toString();
    }
}
