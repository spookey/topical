document.addEventListener('DOMContentLoaded', () => {
  function openModal(element) {
    element.classList.add('is-active');
  }
  function closeModal(element) {
    element.classList.remove('is-active');
  }
  function closeAllModals() {
    (document.querySelectorAll('.modal') || []).forEach((modal) => {
      closeModal(modal);
    });
  }

  (document.querySelectorAll('.modal-trigger') || []).forEach((trigger) => {
    const target = document.getElementById(trigger.dataset.target);
    trigger.addEventListener('click', () => { openModal(target); });
  });

  (document.querySelectorAll('.modal-background, .modal-card-head .delete') || []).forEach((trigger) => {
    const target = trigger.closest('.modal');
    trigger.addEventListener('click', () => { closeModal(target); });
  });

  (document.addEventListener('keydown', (event) => {
    if (event.key === 'Escape') { closeAllModals(); }
  }));
});
