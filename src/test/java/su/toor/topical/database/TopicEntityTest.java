package su.toor.topical.database;

import static org.assertj.core.api.Assertions.assertThat;
import static su.toor.topical.shared.misc.RequireAssert.assertThatRequireException;

import java.time.OffsetDateTime;
import org.junit.jupiter.api.Test;
import org.springframework.core.style.DefaultValueStyler;
import org.springframework.core.style.ValueStyler;

class TopicEntityTest {

    private static final ValueStyler STYLER = new DefaultValueStyler();

    @Test
    void createInvalid() {
        assertThatRequireException().isThrownBy(() -> TopicEntity.with(null));
        assertThatRequireException().isThrownBy(() -> TopicEntity.with(""));
        assertThatRequireException().isThrownBy(() -> TopicEntity.with("#"));
    }

    @Test
    void create() {
        final var start = OffsetDateTime.now();
        final var name = "some/topic";

        final var entity = TopicEntity.with(name);

        assertThat(entity.getId()).isNull();
        final var uuid = entity.getUuid();
        assertThat(uuid).isNotNull();
        final var created = entity.getCreated();
        assertThat(created).isAfterOrEqualTo(start).isBeforeOrEqualTo(OffsetDateTime.now());
        final var updated = entity.getUpdated();
        assertThat(updated)
                .isAfterOrEqualTo(start)
                .isBeforeOrEqualTo(OffsetDateTime.now())
                .isAfter(created);
        assertThat(entity.getName()).isEqualTo(name);

        assertThat(entity.get()).isSameAs(entity);

        assertThat(entity.toString())
                .isNotBlank()
                .contains(STYLER.style(uuid))
                .contains(STYLER.style(created))
                .contains(STYLER.style(updated))
                .contains(name);
    }
}
