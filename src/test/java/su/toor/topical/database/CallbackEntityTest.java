package su.toor.topical.database;

import static org.assertj.core.api.Assertions.assertThat;
import static su.toor.topical.shared.misc.RequireAssert.assertThatRequireException;

import java.net.MalformedURLException;
import java.net.URI;
import java.time.OffsetDateTime;
import org.junit.jupiter.api.Test;
import org.springframework.core.style.DefaultValueStyler;
import org.springframework.core.style.ValueStyler;
import org.springframework.http.HttpMethod;

class CallbackEntityTest {

    private static final ValueStyler STYLER = new DefaultValueStyler();

    private static final TopicEntity TOPIC_ENTITY = TopicEntity.with("some/topic");
    private static final HttpMethod METHOD = HttpMethod.PUT;
    private static final String ADDRESS = "https://example.org/api/call/back";
    private static final Boolean ENABLED = true;

    @Test
    void createInvalid() throws MalformedURLException {
        final var url = URI.create(ADDRESS).toURL();

        assertThatRequireException().isThrownBy(() -> CallbackEntity.with(null, METHOD, url, ENABLED));
        assertThatRequireException().isThrownBy(() -> CallbackEntity.with(TOPIC_ENTITY, null, url, ENABLED));
        assertThatRequireException().isThrownBy(() -> CallbackEntity.with(TOPIC_ENTITY, METHOD, null, ENABLED));
        assertThatRequireException().isThrownBy(() -> CallbackEntity.with(TOPIC_ENTITY, METHOD, url, null));
    }

    @Test
    void create() throws MalformedURLException {
        final var start = OffsetDateTime.now();
        final var url = URI.create(ADDRESS).toURL();

        final var entity = CallbackEntity.with(TOPIC_ENTITY, METHOD, url, ENABLED);

        assertThat(entity.getId()).isNull();
        final var uuid = entity.getUuid();
        assertThat(uuid).isNotNull();
        final var created = entity.getCreated();
        assertThat(created).isAfterOrEqualTo(start).isBeforeOrEqualTo(OffsetDateTime.now());
        final var updated = entity.getUpdated();
        assertThat(updated)
                .isAfterOrEqualTo(start)
                .isBeforeOrEqualTo(OffsetDateTime.now())
                .isAfter(created);
        assertThat(entity.getTopicEntity()).isEqualTo(TOPIC_ENTITY);
        assertThat(entity.getUuid()).isNotNull();
        assertThat(entity.getMethod()).isEqualTo(METHOD);
        assertThat(entity.getUrl()).isEqualTo(url);
        assertThat(entity.isEnabled()).isEqualTo(ENABLED);

        assertThat(entity.get()).isSameAs(entity);

        assertThat(entity.toString())
                .isNotBlank()
                .contains(STYLER.style(uuid))
                .contains(STYLER.style(created))
                .contains(STYLER.style(updated))
                .contains(STYLER.style(TOPIC_ENTITY))
                .contains(STYLER.style(METHOD))
                .contains(STYLER.style(url))
                .contains(STYLER.style(ENABLED));
    }

    @Test
    void method() throws MalformedURLException {
        final var url = URI.create(ADDRESS).toURL();
        final var method = HttpMethod.GET;
        final var entity = CallbackEntity.with(TOPIC_ENTITY, METHOD, url, ENABLED);

        final var updated = entity.getUpdated();
        assertThat(entity.getMethod()).isEqualTo(METHOD);

        assertThat(entity.setMethod(method)).isSameAs(entity);

        assertThat(entity.getMethod()).isNotEqualTo(METHOD).isEqualTo(method);
        assertThat(entity.getUpdated()).isAfter(updated);
    }

    @Test
    void url() throws MalformedURLException {
        final var url = URI.create(ADDRESS).toURL();
        final var newUrl = URI.create("https://example.net/some/where").toURL();
        final var entity = CallbackEntity.with(TOPIC_ENTITY, METHOD, url, ENABLED);

        final var updated = entity.getUpdated();
        assertThat(entity.getUrl()).isEqualTo(url);

        assertThat(entity.setUrl(newUrl)).isSameAs(entity);

        assertThat(entity.getUrl()).isNotEqualTo(url).isEqualTo(newUrl);
        assertThat(entity.getUpdated()).isAfter(updated);
    }

    @Test
    void enabled() throws MalformedURLException {
        final var url = URI.create(ADDRESS).toURL();
        final var entity = CallbackEntity.with(TOPIC_ENTITY, METHOD, url, ENABLED);

        final var updated = entity.getUpdated();
        assertThat(entity.isEnabled()).isEqualTo(ENABLED);

        assertThat(entity.setEnabled(!ENABLED)).isSameAs(entity);

        assertThat(entity.isEnabled()).isNotEqualTo(ENABLED).isEqualTo(!ENABLED);
        assertThat(entity.getUpdated()).isAfter(updated);
    }
}
