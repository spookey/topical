package su.toor.topical.database;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mockStatic;

import java.net.MalformedURLException;
import java.net.URI;
import java.time.OffsetDateTime;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpMethod;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import su.toor.topical.DataBase;
import su.toor.topical.shared.Qos;
import su.toor.topical.shared.misc.Require;

@ExtendWith(SpringExtension.class)
@DataJpaTest
class EntityHandlingTest extends DataBase {

    private static final String TOPIC_NAME = "some/topic";
    private static final byte[] MESSAGE_PAYLOAD = new byte[] {0x01, 0x03, 0x01, 0x02};
    private static final String LISTEN_FILTER = "some/#";
    private static final Qos LISTEN_QOS = Qos.ZERO;
    private static final boolean LISTEN_ENABLED = true;
    private static final HttpMethod CALLBACK_METHOD = HttpMethod.PUT;
    private static final String CALLBACK_ADDRESS = "https://example.org/api";
    private static final boolean CALLBACK_ENABLED = true;

    @Autowired
    private TopicRepository topicRepository;

    @Autowired
    private MessageRepository messageRepository;

    @Autowired
    private ListenRepository listenRepository;

    @Autowired
    private CallbackRepository callbackRepository;

    @BeforeEach
    public void setup() {
        callbackRepository.deleteAllInBatch();
        listenRepository.deleteAllInBatch();
        messageRepository.deleteAllInBatch();
        topicRepository.deleteAllInBatch();
    }

    @Test
    void topicUniqueName() {
        topicRepository.save(TopicEntity.with(TOPIC_NAME));
        final var topicEntity = TopicEntity.with(TOPIC_NAME);

        assertThatExceptionOfType(DataIntegrityViolationException.class)
                .isThrownBy(() -> topicRepository.save(topicEntity));
    }

    @Test
    void topicFindByUuid() {
        final var topicEntity = topicRepository.save(TopicEntity.with(TOPIC_NAME));

        assertThat(topicRepository.findByUuid(UUID.randomUUID())).isEmpty();

        assertThat(topicRepository.findByUuid(topicEntity.getUuid())).hasValue(topicEntity);
    }

    @Test
    void topicFindByName() {
        final var topicEntity = topicRepository.save(TopicEntity.with(TOPIC_NAME));

        assertThat(topicRepository.findByName("different")).isEmpty();

        assertThat(topicRepository.findByName(topicEntity.getName())).hasValue(topicEntity);
    }

    @Test
    void messageMissingTopic() {
        try (final var require = mockStatic(Require.class)) {
            require.when(() -> Require.notNull(anyString(), any(TopicEntity.class)))
                    .thenReturn(null);

            final var brokenEntity = MessageEntity.with(null, MESSAGE_PAYLOAD, null, null, null, null);
            assertThatExceptionOfType(DataIntegrityViolationException.class)
                    .isThrownBy(() -> messageRepository.save(brokenEntity));
        }
    }

    @Test
    void messageFindByUuid() {
        final var topicEntity = topicRepository.save(TopicEntity.with(TOPIC_NAME));
        final var messageEntity =
                messageRepository.save(MessageEntity.with(topicEntity, MESSAGE_PAYLOAD, null, null, null, null));

        assertThat(messageRepository.findByUuid(UUID.randomUUID())).isEmpty();

        assertThat(messageRepository.findByUuid(messageEntity.getUuid())).hasValue(messageEntity);
    }

    @Test
    void messageFindAllByCreatedAfter() {
        final var start = OffsetDateTime.now();

        final var topicEntity = topicRepository.save(TopicEntity.with(TOPIC_NAME));
        final var messageEntityOne =
                messageRepository.save(MessageEntity.with(topicEntity, MESSAGE_PAYLOAD, Qos.ONE, true, false, 1));
        final var messageEntityTwo =
                messageRepository.save(MessageEntity.with(topicEntity, MESSAGE_PAYLOAD, Qos.TWO, false, true, 2));

        assertThat(messageRepository.findAllByCreatedAfter(OffsetDateTime.now(), PageRequest.ofSize(2)))
                .isEmpty();

        assertThat(messageRepository.findAllByCreatedAfter(
                        start.minusDays(2),
                        PageRequest.ofSize(2).withSort(Sort.by("created").ascending())))
                .containsExactly(messageEntityOne, messageEntityTwo);
    }

    @Test
    void messageFindAllByCreatedBefore() {
        final var start = OffsetDateTime.now();

        final var topicEntity = topicRepository.save(TopicEntity.with(TOPIC_NAME));
        final var messageEntityOne =
                messageRepository.save(MessageEntity.with(topicEntity, MESSAGE_PAYLOAD, Qos.ONE, true, false, 1));
        final var messageEntityTwo =
                messageRepository.save(MessageEntity.with(topicEntity, MESSAGE_PAYLOAD, Qos.TWO, false, true, 2));

        assertThat(messageRepository.findAllByCreatedBefore(OffsetDateTime.now()))
                .containsExactlyInAnyOrder(messageEntityOne, messageEntityTwo);

        assertThat(messageRepository.findAllByCreatedBefore(start.minusDays(2))).isEmpty();
    }

    @Test
    void messageTopicCascade() {
        final var topicEntity = topicRepository.save(TopicEntity.with(TOPIC_NAME));
        final var messageEntity =
                messageRepository.save(MessageEntity.with(topicEntity, MESSAGE_PAYLOAD, null, null, null, null));

        assertThat(topicRepository.findAll()).containsExactly(topicEntity);
        assertThat(messageRepository.findAll()).containsExactly(messageEntity);

        assertThat(messageEntity.getTopicEntity()).isEqualTo(topicEntity);

        messageRepository.delete(messageEntity);

        assertThat(topicRepository.findAll()).containsExactly(topicEntity);
        assertThat(messageRepository.findAll()).isEmpty();
    }

    @Test
    void topicMessageCascade() {
        final var topicEntity = topicRepository.save(TopicEntity.with(TOPIC_NAME));
        final var messageEntity =
                messageRepository.save(MessageEntity.with(topicEntity, MESSAGE_PAYLOAD, null, null, null, null));

        assertThat(topicRepository.findAll()).containsExactly(topicEntity);
        assertThat(messageRepository.findAll()).containsExactly(messageEntity);

        assertThat(messageEntity.getTopicEntity()).isEqualTo(topicEntity);

        topicRepository.delete(topicEntity);

        assertThat(topicRepository.findAll()).isEmpty();
        assertThat(messageRepository.findAll()).isEmpty();
    }

    @Test
    void listenFindByUuid() {
        final var listenEntity = listenRepository.save(ListenEntity.with(LISTEN_FILTER, LISTEN_QOS, LISTEN_ENABLED));

        assertThat(listenRepository.findByUuid(UUID.randomUUID())).isEmpty();

        assertThat(listenRepository.findByUuid(listenEntity.getUuid())).hasValue(listenEntity);
    }

    @Test
    void callbackMissingTopic() throws MalformedURLException {
        final var url = URI.create(CALLBACK_ADDRESS).toURL();

        try (final var require = mockStatic(Require.class)) {
            require.when(() -> Require.notNull(anyString(), any(CallbackEntity.class)))
                    .thenReturn(null);

            final var brokenEntity = CallbackEntity.with(null, CALLBACK_METHOD, url, CALLBACK_ENABLED);
            assertThatExceptionOfType(DataIntegrityViolationException.class)
                    .isThrownBy(() -> callbackRepository.save(brokenEntity));
        }
    }

    @Test
    void callbackFindByUuid() throws MalformedURLException {
        final var url = URI.create(CALLBACK_ADDRESS).toURL();

        final var topicEntity = topicRepository.save(TopicEntity.with(TOPIC_NAME));
        final var callbackEntity =
                callbackRepository.save(CallbackEntity.with(topicEntity, CALLBACK_METHOD, url, CALLBACK_ENABLED));

        assertThat(callbackRepository.findByUuid(UUID.randomUUID())).isEmpty();

        assertThat(callbackRepository.findByUuid(callbackEntity.getUuid())).hasValue(callbackEntity);
    }

    @Test
    void callbackFindAllByTopicEntityAndEnabled() throws MalformedURLException {
        final var url = URI.create(CALLBACK_ADDRESS).toURL();

        final var topicEntity = topicRepository.save(TopicEntity.with(TOPIC_NAME));
        final var topicEntityWrong = topicRepository.save(TopicEntity.with("different"));
        final var callbackEntityEnabled =
                callbackRepository.save(CallbackEntity.with(topicEntity, CALLBACK_METHOD, url, CALLBACK_ENABLED));
        final var callbackEntityDisabled =
                callbackRepository.save(CallbackEntity.with(topicEntity, CALLBACK_METHOD, url, !CALLBACK_ENABLED));

        assertThat(callbackRepository.findAllByTopicEntityAndEnabled(topicEntityWrong, CALLBACK_ENABLED))
                .isEmpty();
        assertThat(callbackRepository.findAllByTopicEntityAndEnabled(topicEntityWrong, !CALLBACK_ENABLED))
                .isEmpty();

        assertThat(callbackRepository.findAllByTopicEntityAndEnabled(topicEntity, CALLBACK_ENABLED))
                .containsExactly(callbackEntityEnabled);
        assertThat(callbackRepository.findAllByTopicEntityAndEnabled(topicEntity, !CALLBACK_ENABLED))
                .containsExactly(callbackEntityDisabled);
    }

    @Test
    void callbackTopicCascade() throws MalformedURLException {
        final var url = URI.create(CALLBACK_ADDRESS).toURL();

        final var topicEntity = topicRepository.save(TopicEntity.with(TOPIC_NAME));
        final var callbackEntity =
                callbackRepository.save(CallbackEntity.with(topicEntity, CALLBACK_METHOD, url, CALLBACK_ENABLED));

        assertThat(topicRepository.findAll()).containsExactly(topicEntity);
        assertThat(callbackRepository.findAll()).containsExactly(callbackEntity);

        assertThat(callbackEntity.getTopicEntity()).isEqualTo(topicEntity);

        callbackRepository.delete(callbackEntity);

        assertThat(topicRepository.findAll()).containsExactly(topicEntity);
        assertThat(callbackRepository.findAll()).isEmpty();
    }

    @Test
    void topicCallbackCascade() throws MalformedURLException {
        final var url = URI.create(CALLBACK_ADDRESS).toURL();

        final var topicEntity = topicRepository.save(TopicEntity.with(TOPIC_NAME));
        final var callbackEntity =
                callbackRepository.save(CallbackEntity.with(topicEntity, CALLBACK_METHOD, url, CALLBACK_ENABLED));

        assertThat(topicRepository.findAll()).containsExactly(topicEntity);
        assertThat(callbackRepository.findAll()).containsExactly(callbackEntity);

        assertThat(callbackEntity.getTopicEntity()).isEqualTo(topicEntity);

        topicRepository.delete(topicEntity);

        assertThat(topicRepository.findAll()).isEmpty();
        assertThat(callbackRepository.findAll()).isEmpty();
    }
}
