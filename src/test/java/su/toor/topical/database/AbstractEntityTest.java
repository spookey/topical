package su.toor.topical.database;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import org.junit.jupiter.api.Test;
import org.springframework.core.style.DefaultValueStyler;
import org.springframework.core.style.ValueStyler;

class AbstractEntityTest {

    private static final ValueStyler STYLER = new DefaultValueStyler();

    private static final class PhonyEntity extends AbstractEntity<PhonyEntity> {
        @Override
        protected PhonyEntity get() {
            return this;
        }
    }

    @Test
    void fields() {
        final var start = OffsetDateTime.now();

        final var entity = new PhonyEntity();

        assertThat(entity.getId()).isNull();
        final var created = entity.getCreated();
        assertThat(created).isAfterOrEqualTo(start).isBeforeOrEqualTo(OffsetDateTime.now());
        final var updated = entity.getUpdated();
        assertThat(updated)
                .isAfterOrEqualTo(start)
                .isBeforeOrEqualTo(OffsetDateTime.now())
                .isAfter(created);

        assertThat(entity.get()).isSameAs(entity);

        assertThat(entity.toString())
                .isNotBlank()
                .contains("id")
                .contains(STYLER.style(created))
                .contains(STYLER.style(updated));
    }

    @Test
    void timestamps() {
        final var start = OffsetDateTime.now();

        final var entity = new PhonyEntity();

        final var created = entity.getCreated();
        final var updated = entity.getUpdated();

        assertThat(created)
                .isAfterOrEqualTo(start)
                .isBeforeOrEqualTo(OffsetDateTime.now())
                .satisfies(cr -> assertThat(cr.getOffset()).isEqualTo(ZoneOffset.UTC));
        assertThat(updated)
                .isAfterOrEqualTo(start)
                .isBeforeOrEqualTo(OffsetDateTime.now())
                .isAfter(entity.getCreated())
                .satisfies(up -> assertThat(up.getOffset()).isEqualTo(ZoneOffset.UTC));

        assertThat(entity.update()).isSameAs(entity);

        assertThat(entity.getCreated()).isSameAs(created).satisfies(cr -> assertThat(cr.getOffset())
                .isEqualTo(ZoneOffset.UTC));

        assertThat(entity.getUpdated()).isNotSameAs(updated).isAfter(updated).satisfies(up -> assertThat(up.getOffset())
                .isEqualTo(ZoneOffset.UTC));
    }
}
