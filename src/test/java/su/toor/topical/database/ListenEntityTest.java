package su.toor.topical.database;

import static org.assertj.core.api.Assertions.assertThat;
import static su.toor.topical.shared.misc.RequireAssert.assertThatRequireException;

import java.time.OffsetDateTime;
import org.junit.jupiter.api.Test;
import org.springframework.core.style.DefaultValueStyler;
import org.springframework.core.style.ValueStyler;
import su.toor.topical.shared.Qos;

class ListenEntityTest {

    private static final ValueStyler STYLER = new DefaultValueStyler();

    private static final String FILTER = "some/+/topic";
    private static final Qos QOS = Qos.ONE;
    private static final boolean ENABLED = false;

    @Test
    void createInvalid() {
        assertThatRequireException().isThrownBy(() -> ListenEntity.with(null, QOS, ENABLED));
        assertThatRequireException().isThrownBy(() -> ListenEntity.with("", QOS, ENABLED));
        assertThatRequireException().isThrownBy(() -> ListenEntity.with("c++", QOS, ENABLED));
        assertThatRequireException().isThrownBy(() -> ListenEntity.with(FILTER, null, ENABLED));
        assertThatRequireException().isThrownBy(() -> ListenEntity.with(FILTER, QOS, null));
    }

    @Test
    void create() {
        final var start = OffsetDateTime.now();

        final var entity = ListenEntity.with(FILTER, QOS, ENABLED);

        assertThat(entity.getId()).isNull();
        final var uuid = entity.getUuid();
        assertThat(uuid).isNotNull();
        final var created = entity.getCreated();
        assertThat(created).isAfterOrEqualTo(start).isBeforeOrEqualTo(OffsetDateTime.now());
        final var updated = entity.getUpdated();
        assertThat(updated)
                .isAfterOrEqualTo(start)
                .isBeforeOrEqualTo(OffsetDateTime.now())
                .isAfter(created);
        assertThat(entity.getFilter()).isEqualTo(FILTER);
        assertThat(entity.getUuid()).isNotNull();
        assertThat(entity.getQos()).isEqualTo(QOS);
        assertThat(entity.isEnabled()).isEqualTo(ENABLED);

        assertThat(entity.get()).isSameAs(entity);

        assertThat(entity.toString())
                .isNotBlank()
                .contains(STYLER.style(uuid))
                .contains(STYLER.style(created))
                .contains(STYLER.style(updated))
                .contains(FILTER)
                .contains(STYLER.style(QOS))
                .contains(STYLER.style(ENABLED));
    }

    @Test
    void filter() {
        final var filter = "some/#";
        final var entity = ListenEntity.with(FILTER, QOS, ENABLED);

        assertThat(entity.getFilter()).isEqualTo(FILTER);
        final var updated = entity.getUpdated();

        assertThat(entity.setFilter(filter)).isSameAs(entity);

        assertThat(entity.getFilter()).isNotEqualTo(FILTER).isEqualTo(filter);
        assertThat(entity.getUpdated()).isAfter(updated);
    }

    @Test
    void qos() {
        final var qos = Qos.TWO;
        final var entity = ListenEntity.with(FILTER, QOS, ENABLED);

        assertThat(entity.getQos()).isEqualTo(QOS);
        final var updated = entity.getUpdated();

        assertThat(entity.setQos(qos)).isSameAs(entity);

        assertThat(entity.getQos()).isNotEqualTo(QOS).isEqualTo(qos);
        assertThat(entity.getUpdated()).isAfter(updated);
    }

    @Test
    void enabled() {
        final var entity = ListenEntity.with(FILTER, QOS, ENABLED);

        assertThat(entity.isEnabled()).isEqualTo(ENABLED);
        final var updated = entity.getUpdated();

        assertThat(entity.setEnabled(!ENABLED)).isSameAs(entity);

        assertThat(entity.isEnabled()).isNotEqualTo(ENABLED).isEqualTo(!ENABLED);
        assertThat(entity.getUpdated()).isAfter(updated);
    }
}
