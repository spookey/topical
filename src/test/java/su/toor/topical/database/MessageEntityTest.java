package su.toor.topical.database;

import static org.assertj.core.api.Assertions.assertThat;
import static su.toor.topical.shared.misc.RequireAssert.assertThatRequireException;

import java.time.OffsetDateTime;
import org.junit.jupiter.api.Test;
import org.springframework.core.style.DefaultValueStyler;
import org.springframework.core.style.ValueStyler;
import su.toor.topical.shared.Qos;

class MessageEntityTest {

    private static final ValueStyler STYLER = new DefaultValueStyler();

    private static final TopicEntity TOPIC_ENTITY = TopicEntity.with("some/topic");
    private static final byte[] PAYLOAD = new byte[] {0x13, 0x12};
    private static final Qos QOS = Qos.ZERO;
    private static final boolean RETAINED = false;
    private static final boolean DUPLICATE = true;
    private static final int MESSAGE_ID = 42;

    @Test
    void createInvalid() {
        assertThatRequireException().isThrownBy(() -> MessageEntity.with(null, PAYLOAD, null, null, null, null));
        assertThatRequireException().isThrownBy(() -> MessageEntity.with(TOPIC_ENTITY, null, null, null, null, null));
    }

    @Test
    void create() {
        final var start = OffsetDateTime.now();

        final var entity = MessageEntity.with(TOPIC_ENTITY, PAYLOAD, QOS, RETAINED, DUPLICATE, MESSAGE_ID);

        assertThat(entity.getId()).isNull();
        final var uuid = entity.getUuid();
        assertThat(uuid).isNotNull();
        final var created = entity.getCreated();
        assertThat(created).isAfterOrEqualTo(start).isBeforeOrEqualTo(OffsetDateTime.now());
        final var updated = entity.getUpdated();
        assertThat(updated)
                .isAfterOrEqualTo(start)
                .isBeforeOrEqualTo(OffsetDateTime.now())
                .isAfter(created);
        assertThat(entity.getTopicEntity().getName()).isEqualTo(TOPIC_ENTITY.getName());
        assertThat(entity.getPayload()).isEqualTo(PAYLOAD);
        assertThat(entity.getQos()).isEqualTo(QOS);
        assertThat(entity.isRetained()).isEqualTo(RETAINED);
        assertThat(entity.isDuplicate()).isEqualTo(DUPLICATE);
        assertThat(entity.getMessageId()).isEqualTo(MESSAGE_ID);

        assertThat(entity.get()).isSameAs(entity);

        assertThat(entity.toString())
                .isNotBlank()
                .contains(STYLER.style(uuid))
                .contains(STYLER.style(created))
                .contains(STYLER.style(updated))
                .contains(STYLER.style(TOPIC_ENTITY))
                .contains(STYLER.style(PAYLOAD))
                .contains(STYLER.style(QOS))
                .contains(STYLER.style(RETAINED))
                .contains(STYLER.style(DUPLICATE))
                .contains(STYLER.style(MESSAGE_ID));
    }
}
