package su.toor.topical.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.failBecauseExceptionWasNotThrown;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.util.ReflectionTestUtils.setField;
import static su.toor.topical.shared.misc.RequireAssert.assertThatRequireException;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.time.OffsetDateTime;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpMethod;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import su.toor.topical.DataBase;
import su.toor.topical.application.setting.BasicSetting;
import su.toor.topical.database.CallbackEntity;
import su.toor.topical.database.CallbackRepository;
import su.toor.topical.database.ListenEntity;
import su.toor.topical.database.ListenRepository;
import su.toor.topical.database.MessageEntity;
import su.toor.topical.database.MessageRepository;
import su.toor.topical.database.TopicEntity;
import su.toor.topical.database.TopicRepository;
import su.toor.topical.shared.Qos;

@ExtendWith(SpringExtension.class)
@DataJpaTest
class EntityServiceTest extends DataBase {

    @Autowired
    private TopicRepository topicRepository;

    @Autowired
    private MessageRepository messageRepository;

    @Autowired
    private ListenRepository listenRepository;

    @Autowired
    private CallbackRepository callbackRepository;

    static final int KEEP_DAYS = 7;
    static final int RECENT_HOURS = 2;
    static final int PAGINATION_SIZE = 8;
    private static final BasicSetting BASIC_SETTING = new BasicSetting()
            .setKeepDays(KEEP_DAYS)
            .setRecentHours(RECENT_HOURS)
            .setPaginationSize(PAGINATION_SIZE);

    private EntityService entityService;

    @BeforeEach
    public void setup() {
        listenRepository.deleteAllInBatch();
        messageRepository.deleteAllInBatch();
        topicRepository.deleteAllInBatch();

        entityService = new EntityService(
                BASIC_SETTING, topicRepository, messageRepository, listenRepository, callbackRepository);
    }

    @Test
    void pagination() {
        final BiFunction<Integer, Integer, Pageable> make =
                (page, size) -> entityService.pagination(page, size, Sort.Direction.ASC, "some");

        assertThat(make.apply(-1, PAGINATION_SIZE).getPageNumber()).isZero();
        assertThat(make.apply(0, PAGINATION_SIZE).getPageNumber()).isZero();
        assertThat(make.apply(null, PAGINATION_SIZE).getPageNumber()).isZero();
        assertThat(make.apply(1, PAGINATION_SIZE).getPageNumber()).isOne();
        assertThat(make.apply(1337, PAGINATION_SIZE).getPageNumber()).isEqualTo(1337);

        assertThat(make.apply(0, -1).getPageSize()).isEqualTo(PAGINATION_SIZE);
        assertThat(make.apply(0, 0).getPageSize()).isEqualTo(PAGINATION_SIZE);
        assertThat(make.apply(0, null).getPageSize()).isEqualTo(PAGINATION_SIZE);
        assertThat(make.apply(0, 1).getPageSize()).isOne();
        assertThat(make.apply(0, 2 * EntityService.MAX_PAGINATION_SIZE).getPageSize())
                .isEqualTo(EntityService.MAX_PAGINATION_SIZE);
    }

    @Test
    void counters() throws MalformedURLException {
        final List<Supplier<Long>> counters = List.of(
                entityService::countTopics,
                entityService::countMessages,
                entityService::countListeners,
                entityService::countCallbacks);

        counters.forEach(counter -> assertThat(counter.get()).isZero());

        final var topicEntity = topicRepository.save(TopicEntity.with("topic"));
        messageRepository.save(MessageEntity.with(topicEntity, new byte[] {}, null, null, null, null));
        listenRepository.save(ListenEntity.with("topic/+", Qos.ZERO, false));
        callbackRepository.save(CallbackEntity.with(
                topicEntity, HttpMethod.PUT, URI.create("https://example.org").toURL(), false));

        counters.forEach(counter -> assertThat(counter.get()).isOne());
    }

    @Test
    void findTopic() {
        final var topicEntity = topicRepository.save(TopicEntity.with("some/topic"));

        assertThat(entityService.findTopic("different/topic")).isEmpty();
        assertThat(entityService.findTopic(UUID.randomUUID())).isEmpty();
        assertThat(entityService.findTopic(topicEntity.getName())).hasValue(topicEntity);
        assertThat(entityService.findTopic(topicEntity.getUuid())).hasValue(topicEntity);
    }

    @Test
    void allTopics() {
        final var pages = 3;
        final var amount = pages * PAGINATION_SIZE;

        final var topicEntities = IntStream.range(0, amount)
                .boxed()
                .map(num -> topicRepository.save(TopicEntity.with("topic/t-%04d".formatted(amount - num))))
                .collect(Collectors.toList());

        assertThat(topicRepository.findAll()).containsExactlyElementsOf(topicEntities);
        Collections.reverse(topicEntities);

        IntStream.range(0, pages).forEach(pageNumber -> {
            final var page = entityService.allTopics(pageNumber, PAGINATION_SIZE);

            assertThat(page.getTotalElements()).isEqualTo(amount);
            assertThat(page.getTotalPages()).isEqualTo(pages);

            assertThat(page)
                    .containsExactlyElementsOf(topicEntities.stream()
                            .skip((long) PAGINATION_SIZE * pageNumber)
                            .limit(PAGINATION_SIZE)
                            .toList());
        });
    }

    @Test
    void ensureTopicInvalid() {
        assertThatRequireException().isThrownBy(() -> entityService.ensureTopic("filter/#"));
    }

    @Test
    void ensureTopicExisting() {
        final var topicEntity = topicRepository.save(TopicEntity.with("does/exist"));

        final var result = entityService.ensureTopic(topicEntity.getName());

        assertThat(result).isSameAs(topicEntity);
    }

    @Test
    void ensureTopicCreate() {
        final var topic = "create/me";
        final var topicEntity = entityService.ensureTopic(topic);

        assertThat(topicEntity.getName()).isEqualTo(topic);
        assertThat(topicRepository.findAll()).containsExactly(topicEntity);
    }

    @Test
    void deleteTopic() {
        final var topicEntity = topicRepository.save(TopicEntity.with("delete/me"));

        assertThat(topicRepository.findAll()).containsExactly(topicEntity);

        assertThat(entityService.deleteTopic(UUID.randomUUID())).isFalse();
        assertThat(topicRepository.findAll()).containsExactly(topicEntity);

        assertThat(entityService.deleteTopic(topicEntity.getUuid())).isTrue();
        assertThat(topicRepository.findAll()).isEmpty();
    }

    @Test
    void findMessage() {
        final var topicEntity = topicRepository.save(TopicEntity.with("some/topic"));
        final var messageEntity =
                messageRepository.save(MessageEntity.with(topicEntity, new byte[] {}, null, null, null, null));

        assertThat(entityService.findMessage(UUID.randomUUID())).isEmpty();
        assertThat(entityService.findMessage(messageEntity.getUuid())).hasValue(messageEntity);
    }

    @Test
    void allMessages() {
        final var pages = 5;
        final var amount = pages * PAGINATION_SIZE;

        final var topicEntity = topicRepository.save(TopicEntity.with("the/topic"));
        final var messageEntities = IntStream.range(0, amount)
                .boxed()
                .map(num -> messageRepository.save(
                        MessageEntity.with(topicEntity, new byte[] {num.byteValue()}, null, null, null, null)))
                .collect(Collectors.toList());

        assertThat(messageRepository.findAll()).containsExactlyElementsOf(messageEntities);
        Collections.reverse(messageEntities);

        IntStream.range(0, pages).forEach(pageNumber -> {
            final var page = entityService.allMessages(pageNumber, PAGINATION_SIZE);

            assertThat(page.getTotalElements()).isEqualTo(amount);
            assertThat(page.getTotalPages()).isEqualTo(pages);

            assertThat(page)
                    .containsExactlyElementsOf(messageEntities.stream()
                            .skip((long) PAGINATION_SIZE * pageNumber)
                            .limit(PAGINATION_SIZE)
                            .toList());
        });
    }

    @Test
    void allMessagesAfter() {
        final var topicEntity = topicRepository.save(TopicEntity.with("some/topic"));
        final var messageEntitiesOutdated = IntStream.range(0, PAGINATION_SIZE)
                .boxed()
                .map(num -> messageRepository.save(
                        MessageEntity.with(topicEntity, new byte[] {num.byteValue()}, null, null, null, null)))
                .toList();

        final var stamp = OffsetDateTime.now();

        final var messageEntitiesRecent = IntStream.range(0, PAGINATION_SIZE)
                .boxed()
                .map(num -> messageRepository.save(
                        MessageEntity.with(topicEntity, new byte[] {num.byteValue()}, null, null, null, null)))
                .collect(Collectors.toList());

        assertThat(messageRepository.findAll())
                .containsExactlyElementsOf(
                        Stream.concat(messageEntitiesOutdated.stream(), messageEntitiesRecent.stream())
                                .toList());

        Collections.reverse(messageEntitiesRecent);
        assertThat(entityService.allMessagesAfter(stamp, 0, PAGINATION_SIZE))
                .containsExactlyElementsOf(messageEntitiesRecent)
                .doesNotContainAnyElementsOf(messageEntitiesOutdated);
    }

    @Test
    void recentMessages() {
        final var start = OffsetDateTime.now();
        final var page = 23;

        final var service = mock(EntityService.class);
        setField(service, "basicSetting", BASIC_SETTING);

        when(service.recentMessages(page, PAGINATION_SIZE)).thenCallRealMethod();
        when(service.allMessagesAfter(any(OffsetDateTime.class), eq(page), eq(PAGINATION_SIZE)))
                .thenAnswer(invocation -> {
                    final var stamp = invocation.getArgument(0, OffsetDateTime.class);
                    assertThat(stamp)
                            .isBefore(start)
                            .isAfter(start.minusDays(RECENT_HOURS).minusMinutes(1));

                    return null;
                });

        assertThat(service.recentMessages(page, PAGINATION_SIZE)).isNull();

        verify(service, times(1)).recentMessages(page, PAGINATION_SIZE);
        verify(service, times(1)).allMessagesAfter(any(OffsetDateTime.class), eq(page), eq(PAGINATION_SIZE));
        verifyNoMoreInteractions(service);
    }

    @Test
    void allMessagesBefore() {
        final var topicEntity = topicRepository.save(TopicEntity.with("some/topic"));
        final var messageEntitiesOutdated = IntStream.range(0, PAGINATION_SIZE)
                .boxed()
                .map(num -> messageRepository.save(
                        MessageEntity.with(topicEntity, new byte[] {num.byteValue()}, null, null, null, null)))
                .collect(Collectors.toSet());

        final var stamp = OffsetDateTime.now();

        final var messageEntitiesRecent = IntStream.range(0, PAGINATION_SIZE)
                .boxed()
                .map(num -> messageRepository.save(
                        MessageEntity.with(topicEntity, new byte[] {num.byteValue()}, null, null, null, null)))
                .collect(Collectors.toSet());

        assertThat(messageRepository.findAll())
                .containsExactlyInAnyOrderElementsOf(
                        Stream.concat(messageEntitiesOutdated.stream(), messageEntitiesRecent.stream())
                                .toList());

        assertThat(entityService.allMessagesBefore(stamp))
                .containsExactlyInAnyOrderElementsOf(messageEntitiesOutdated)
                .doesNotContainAnyElementsOf(messageEntitiesRecent);
    }

    @Test
    void createMessageInvalid() {
        final var topic = "some/topic";
        final var payload = new byte[] {};

        assertThatRequireException()
                .isThrownBy(() -> entityService.createMessage("listen/#", payload, null, null, null, null));
        assertThatRequireException()
                .isThrownBy(() -> entityService.createMessage(null, payload, null, null, null, null));
        assertThatRequireException().isThrownBy(() -> entityService.createMessage(topic, null, null, null, null, null));
    }

    @Test
    void createMessage() {
        final var topic = "some/topic";
        final var payload = new byte[] {0x01, 0x03, 0x01, 0x02};
        final var qos = Qos.ZERO;
        final var retained = false;
        final var duplicate = true;
        final var messageId = 1312;

        final var messageEntity = entityService.createMessage(topic, payload, qos, retained, duplicate, messageId);

        final var topicEntity = messageEntity.getTopicEntity();
        assertThat(topicEntity.getName()).isEqualTo(topic);
        assertThat(messageEntity.getPayload()).isEqualTo(payload);
        assertThat(messageEntity.getQos()).isEqualTo(qos);
        assertThat(messageEntity.isRetained()).isEqualTo(retained);
        assertThat(messageEntity.isDuplicate()).isEqualTo(duplicate);
        assertThat(messageEntity.getMessageId()).isEqualTo(messageId);

        assertThat(messageRepository.findAll()).containsExactly(messageEntity);
        assertThat(topicRepository.findAll()).containsExactly(topicEntity);
    }

    @Test
    void deleteMessage() {
        final var topicEntity = topicRepository.save(TopicEntity.with("some/topic"));
        final var messageEntity =
                messageRepository.save(MessageEntity.with(topicEntity, new byte[] {}, null, null, null, null));

        assertThat(topicRepository.findAll()).containsExactly(topicEntity);
        assertThat(messageRepository.findAll()).containsExactly(messageEntity);

        assertThat(entityService.deleteMessage(UUID.randomUUID())).isFalse();

        assertThat(topicRepository.findAll()).containsExactly(topicEntity);
        assertThat(messageRepository.findAll()).containsExactly(messageEntity);

        assertThat(entityService.deleteMessage(messageEntity.getUuid())).isTrue();

        assertThat(topicRepository.findAll()).containsExactly(topicEntity);
        assertThat(messageRepository.findAll()).isEmpty();
    }

    @Test
    void findListener() {
        final var listenEntity = listenRepository.save(ListenEntity.with("filter/#", Qos.TWO, false));

        assertThat(entityService.findListener(UUID.randomUUID())).isEmpty();
        assertThat(entityService.findListener(listenEntity.getUuid())).hasValue(listenEntity);
    }

    @Test
    void allListeners() {
        final var pages = 2;
        final var amount = pages * PAGINATION_SIZE;

        final var listenEntities = IntStream.range(0, amount)
                .boxed()
                .map(num -> listenRepository.save(
                        ListenEntity.with("filter/+/%04d".formatted(amount - num), Qos.ZERO, true)))
                .collect(Collectors.toList());

        assertThat(listenRepository.findAll()).containsExactlyElementsOf(listenEntities);
        assertThat(entityService.allListeners()).containsExactlyElementsOf(listenEntities);
        Collections.reverse(listenEntities);

        IntStream.range(0, pages).forEach(pageNumber -> {
            final var page = entityService.allListeners(pageNumber, PAGINATION_SIZE);

            assertThat(page.getTotalElements()).isEqualTo(amount);
            assertThat(page.getTotalPages()).isEqualTo(pages);

            assertThat(page)
                    .containsExactlyElementsOf(listenEntities.stream()
                            .skip((long) PAGINATION_SIZE * pageNumber)
                            .limit(PAGINATION_SIZE)
                            .toList());
        });
    }

    @Test
    void createListenerInvalid() {
        final var filter = "filter/+/topic";
        final var qos = Qos.ZERO;
        final var enabled = true;

        assertThatRequireException().isThrownBy(() -> entityService.createListener("c#", qos, enabled));
        assertThatRequireException().isThrownBy(() -> entityService.createListener(null, qos, enabled));
        assertThatRequireException().isThrownBy(() -> entityService.createListener(filter, null, enabled));
        assertThatRequireException().isThrownBy(() -> entityService.createListener(filter, qos, null));
    }

    @Test
    void createListener() {
        final var filter = "filter/#";
        final var qos = Qos.TWO;
        final var enabled = false;

        final var listenEntity = entityService.createListener(filter, qos, enabled);
        assertThat(listenEntity.getFilter()).isEqualTo(filter);
        assertThat(listenEntity.getQos()).isEqualTo(qos);
        assertThat(listenEntity.isEnabled()).isEqualTo(enabled);

        assertThat(listenRepository.findAll()).containsExactly(listenEntity);
    }

    @Test
    void modifyListener() {
        final var filter = "filter/+/topic";
        final var qos = Qos.ONE;
        final var enabled = true;

        final var listenEntityOld = listenRepository.save(ListenEntity.with("filter/#", Qos.TWO, false));

        assertThat(entityService.modifyListener(UUID.randomUUID(), filter, qos, enabled))
                .isEmpty();

        final var optionalListenEntity = entityService.modifyListener(listenEntityOld.getUuid(), filter, qos, enabled);
        assertThat(optionalListenEntity).isPresent().hasValue(listenEntityOld);

        final var listenEntity = optionalListenEntity.get();
        assertThat(listenEntity.getFilter()).isEqualTo(filter);
        assertThat(listenEntity.getQos()).isEqualTo(qos);
        assertThat(listenEntity.isEnabled()).isEqualTo(enabled);

        assertThat(listenRepository.findAll()).containsExactly(listenEntity);
    }

    @Test
    void deleteListener() {
        final var listenEntity = listenRepository.save(ListenEntity.with("some", Qos.ONE, true));

        assertThat(listenRepository.findAll()).containsExactly(listenEntity);

        assertThat(entityService.deleteListener(UUID.randomUUID())).isFalse();

        assertThat(listenRepository.findAll()).containsExactly(listenEntity);

        assertThat(entityService.deleteListener(listenEntity.getUuid())).isTrue();

        assertThat(listenRepository.findAll()).isEmpty();
    }

    @Test
    void findCallback() throws MalformedURLException {
        final var url = URI.create("https://example.org").toURL();

        final var topicEntity = topicRepository.save(TopicEntity.with("some/topic"));
        final var callbackEntity =
                callbackRepository.save(CallbackEntity.with(topicEntity, HttpMethod.POST, url, true));

        assertThat(entityService.findCallback(UUID.randomUUID())).isEmpty();
        assertThat(entityService.findCallback(callbackEntity.getUuid())).hasValue(callbackEntity);
    }

    @Test
    void allActiveCallbacks() throws MalformedURLException {
        final var url = URI.create("https://example.org/api").toURL();

        final var topicEntityOne = topicRepository.save(TopicEntity.with("one"));
        final var callbackEntityOneEnabled =
                callbackRepository.save(CallbackEntity.with(topicEntityOne, HttpMethod.POST, url, true));
        final var callbackEntityOneDisabled =
                callbackRepository.save(CallbackEntity.with(topicEntityOne, HttpMethod.PUT, url, false));

        final var topicEntityTwo = topicRepository.save(TopicEntity.with("two"));
        final var callbackEntityTwoEnabled =
                callbackRepository.save(CallbackEntity.with(topicEntityTwo, HttpMethod.PUT, url, true));
        final var callbackEntityTwoDisabled =
                callbackRepository.save(CallbackEntity.with(topicEntityTwo, HttpMethod.POST, url, false));

        assertThat(topicRepository.findAll()).containsExactly(topicEntityOne, topicEntityTwo);
        assertThat(callbackRepository.findAll())
                .containsExactly(
                        callbackEntityOneEnabled,
                        callbackEntityOneDisabled,
                        callbackEntityTwoEnabled,
                        callbackEntityTwoDisabled);

        assertThat(entityService.allActiveCallbacks(topicEntityOne)).containsExactly(callbackEntityOneEnabled);
        assertThat(entityService.allActiveCallbacks(topicEntityTwo)).containsExactly(callbackEntityTwoEnabled);
    }

    @Test
    void allCallbacks() {
        final Function<Integer, URL> makeUrl = (num) -> {
            try {
                return URI.create("https://example.net/api/%04d".formatted(num)).toURL();
            } catch (final MalformedURLException ex) {
                failBecauseExceptionWasNotThrown(MalformedURLException.class);
                return null;
            }
        };

        final var pages = 2;
        final var amount = pages * PAGINATION_SIZE;

        final var topicEntity = topicRepository.save(TopicEntity.with("some/topic"));
        final var callbackEntities = IntStream.range(0, amount)
                .boxed()
                .map(num -> callbackRepository.save(
                        CallbackEntity.with(topicEntity, HttpMethod.PUT, makeUrl.apply(amount - num), true)))
                .collect(Collectors.toList());

        assertThat(callbackRepository.findAll()).containsExactlyElementsOf(callbackEntities);
        Collections.reverse(callbackEntities);

        IntStream.range(0, pages).forEach(pageNumber -> {
            final var page = entityService.allCallbacks(pageNumber, PAGINATION_SIZE);

            assertThat(page.getTotalElements()).isEqualTo(amount);
            assertThat(page.getTotalPages()).isEqualTo(pages);

            assertThat(page)
                    .containsExactlyElementsOf(callbackEntities.stream()
                            .skip((long) PAGINATION_SIZE * pageNumber)
                            .limit(PAGINATION_SIZE)
                            .toList());
        });
    }

    @Test
    void createCallbackInvalid() throws MalformedURLException {
        final var topic = "callback/topic";
        final var method = HttpMethod.POST;
        final var url = URI.create("https://api.example.org").toURL();
        final var enabled = true;

        assertThatRequireException().isThrownBy(() -> entityService.createCallback("c++", method, url, enabled));
        assertThatRequireException().isThrownBy(() -> entityService.createCallback(null, method, url, enabled));
        assertThatRequireException().isThrownBy(() -> entityService.createCallback(topic, null, url, enabled));
        assertThatRequireException().isThrownBy(() -> entityService.createCallback(topic, method, null, enabled));
        assertThatRequireException().isThrownBy(() -> entityService.createCallback(topic, method, url, null));
    }

    @Test
    void createCallback() throws MalformedURLException {
        final var topic = "the/topic";
        final var method = HttpMethod.PUT;
        final var url = URI.create("https://api.example.net").toURL();
        final var enabled = true;

        final var callbackEntity = entityService.createCallback(topic, method, url, enabled);
        assertThat(callbackEntity.getMethod()).isEqualTo(method);
        assertThat(callbackEntity.getUrl()).isEqualTo(url);
        assertThat(callbackEntity.isEnabled()).isEqualTo(enabled);
        final var topicEntity = callbackEntity.getTopicEntity();
        assertThat(topicEntity.getName()).isEqualTo(topic);

        assertThat(callbackRepository.findAll()).containsExactly(callbackEntity);
        assertThat(topicRepository.findAll()).containsExactly(topicEntity);
    }

    @Test
    void modifyCallback() throws MalformedURLException {
        final var topic = "some/topic";
        final var method = HttpMethod.POST;
        final var url = URI.create("https://api.example.org").toURL();
        final var enabled = true;

        final var topicEntityOld = topicRepository.save(TopicEntity.with(topic));
        final var callbackEntityOld = callbackRepository.save(CallbackEntity.with(
                topicEntityOld,
                HttpMethod.GET,
                URI.create("https://example.com").toURL(),
                false));

        assertThat(entityService.modifyCallback(UUID.randomUUID(), method, url, enabled))
                .isEmpty();

        final var optionalCallbackEntity =
                entityService.modifyCallback(callbackEntityOld.getUuid(), method, url, enabled);
        assertThat(optionalCallbackEntity).isPresent().hasValue(callbackEntityOld);

        final var callbackEntity = optionalCallbackEntity.get();
        assertThat(callbackEntity.getMethod()).isEqualTo(method);
        assertThat(callbackEntity.getUrl()).isEqualTo(url);
        assertThat(callbackEntity.isEnabled()).isEqualTo(enabled);
        final var topicEntity = callbackEntity.getTopicEntity();
        assertThat(topicEntity.getName()).isEqualTo(topic);

        assertThat(topicRepository.findAll()).containsExactly(topicEntity);
        assertThat(callbackRepository.findAll()).containsExactly(callbackEntity);
    }

    @Test
    void deleteCallback() throws MalformedURLException {
        final var url = URI.create("https://example.org/api").toURL();

        final var topicEntity = topicRepository.save(TopicEntity.with("topic"));
        final var callbackEntity =
                callbackRepository.save(CallbackEntity.with(topicEntity, HttpMethod.POST, url, false));

        assertThat(topicRepository.findAll()).containsExactly(topicEntity);
        assertThat(callbackRepository.findAll()).containsExactly(callbackEntity);

        assertThat(entityService.deleteCallback(UUID.randomUUID())).isFalse();

        assertThat(topicRepository.findAll()).containsExactly(topicEntity);
        assertThat(callbackRepository.findAll()).containsExactly(callbackEntity);

        assertThat(entityService.deleteCallback(callbackEntity.getUuid())).isTrue();

        assertThat(topicRepository.findAll()).containsExactly(topicEntity);
        assertThat(callbackRepository.findAll()).isEmpty();
    }
}
