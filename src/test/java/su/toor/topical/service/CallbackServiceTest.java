package su.toor.topical.service;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;

import java.net.MalformedURLException;
import java.net.URI;
import java.util.List;
import java.util.UUID;
import java.util.stream.Stream;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.bean.override.mockito.MockitoBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.client.ExpectedCount;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;
import su.toor.topical.component.mqtt.PayloadParser;
import su.toor.topical.database.CallbackEntity;
import su.toor.topical.database.MessageEntity;
import su.toor.topical.database.TopicEntity;
import su.toor.topical.shared.dto.CallbackSendDto;
import su.toor.topical.shared.dto.PayloadDto;

@ExtendWith(SpringExtension.class)
@RestClientTest
class CallbackServiceTest {

    private static final String TOPIC = "some/topic";
    private static final byte[] PAYLOAD = new byte[] {};

    private static final TopicEntity TOPIC_ENTITY = TopicEntity.with(TOPIC);
    private static final MessageEntity MESSAGE_ENTITY =
            MessageEntity.with(TOPIC_ENTITY, PAYLOAD, null, null, null, null);

    @MockitoBean
    private EntityService entityService;

    @MockitoBean
    private PayloadParser payloadParser;

    private final RestTemplate liveRestTemplate = new RestTemplate();

    private MockRestServiceServer mockServer;

    @BeforeEach
    public void setup() {
        this.mockServer = MockRestServiceServer.createServer(liveRestTemplate);
    }

    @Test
    void emitNoCallbacks() {
        final var restTemplate = mock(RestTemplate.class);
        final var callbackService = new CallbackService(restTemplate, entityService, payloadParser);

        callbackService.emit(MESSAGE_ENTITY);

        verify(entityService, times(1)).allActiveCallbacks(TOPIC_ENTITY);

        verifyNoMoreInteractions(entityService);
        verifyNoInteractions(restTemplate);
        verifyNoInteractions(payloadParser);
    }

    @Test
    void emit() throws MalformedURLException {
        final var uri = URI.create("https://example.net/api");
        final var url = uri.toURL();
        final var method = HttpMethod.PUT;
        final var callbackEntity = CallbackEntity.with(TOPIC_ENTITY, method, url, true);
        final var payloadDto = new PayloadDto("===", null, null, null, null, null);

        final var response = ResponseEntity.ok().body("ok");

        final var restTemplate = mock(RestTemplate.class);

        when(entityService.allActiveCallbacks(TOPIC_ENTITY)).thenReturn(List.of(callbackEntity));
        when(payloadParser.parse(PAYLOAD)).thenReturn(payloadDto);

        when(restTemplate.exchange(eq(uri), eq(method), any(HttpEntity.class), eq(String.class)))
                .thenReturn(response);

        final var callbackService = new CallbackService(restTemplate, entityService, payloadParser);

        callbackService.emit(MESSAGE_ENTITY);

        verify(entityService, times(1)).allActiveCallbacks(TOPIC_ENTITY);
        verifyNoMoreInteractions(entityService);

        verify(payloadParser, times(1)).parse(PAYLOAD);
        verifyNoMoreInteractions(payloadParser);

        verify(restTemplate, times(1)).exchange(eq(uri), eq(method), any(HttpEntity.class), eq(String.class));
        verifyNoMoreInteractions(restTemplate);
    }

    private static Stream<Arguments> httpMethodStatus() {
        return Stream.of(HttpMethod.values()).flatMap(httpMethod -> Stream.of(
                        HttpStatus.OK,
                        HttpStatus.CREATED,
                        HttpStatus.FOUND,
                        HttpStatus.BAD_REQUEST,
                        HttpStatus.NOT_FOUND,
                        HttpStatus.CONFLICT,
                        HttpStatus.INTERNAL_SERVER_ERROR)
                .map(httpStatus -> Arguments.of(httpMethod, httpStatus)));
    }

    @ParameterizedTest
    @MethodSource(value = "httpMethodStatus")
    void send(final HttpMethod httpMethod, final HttpStatus httpStatus) throws MalformedURLException {
        final var uri = URI.create("https://api.example.org");
        final var url = uri.toURL();

        final var payloadDto = new PayloadDto("===", null, null, null, null, null);
        final var callbackDto = new CallbackSendDto(TOPIC, UUID.randomUUID(), payloadDto);

        final var callbackService = new CallbackService(liveRestTemplate, entityService, payloadParser);

        mockServer
                .expect(ExpectedCount.once(), requestTo(uri))
                .andExpect(method(httpMethod))
                .andRespond(withStatus(httpStatus));

        callbackService.send(url, httpMethod, callbackDto);

        mockServer.verify();
        mockServer.reset();

        verifyNoInteractions(entityService);
        verifyNoInteractions(payloadParser);
    }
}
