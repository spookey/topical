package su.toor.topical.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.mockConstruction;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.List;
import org.eclipse.paho.mqttv5.client.MqttAsyncClient;
import org.eclipse.paho.mqttv5.client.MqttToken;
import org.eclipse.paho.mqttv5.common.MqttException;
import org.eclipse.paho.mqttv5.common.MqttMessage;
import org.eclipse.paho.mqttv5.common.MqttSubscription;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.bean.override.mockito.MockitoBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import su.toor.topical.application.setting.mqtt.BirthStatusSetting;
import su.toor.topical.database.ListenEntity;
import su.toor.topical.shared.Qos;

@ExtendWith(SpringExtension.class)
class ClientServiceTest {

    @MockitoBean
    private EntityService entityService;

    @MockitoBean
    private MqttAsyncClient mqttAsyncClient;

    private static final String TOPIC = "some/topic";
    private static final MqttMessage MESSAGE = new MqttMessage();

    private ClientService create() {
        return new ClientService(entityService, mqttAsyncClient);
    }

    @Test
    void clientServiceBeanCreator() {
        final var birthStatus = new BirthStatusSetting();

        try (final var serviceMock = mockConstruction(ClientService.class)) {
            final var clientBean = ClientService.clientService(entityService, mqttAsyncClient, birthStatus);

            final var client = serviceMock.constructed().getFirst();
            assertThat(client).isSameAs(clientBean);

            verify(client, times(1)).refreshSubscriptions();
            verify(client, times(1)).sendBirthMessage(birthStatus);
            verifyNoMoreInteractions(client);

            verifyNoInteractions(entityService);
            verifyNoInteractions(mqttAsyncClient);
        }
    }

    @Test
    void subscribeNotConnected() {
        final var subscription = new MqttSubscription(TOPIC);

        when(mqttAsyncClient.isConnected()).thenReturn(false);

        final var client = create();
        assertThat(client.subscribe(subscription)).isFalse();

        verify(mqttAsyncClient, times(1)).isConnected();

        verifyNoInteractions(entityService);
        verifyNoMoreInteractions(mqttAsyncClient);
    }

    @Test
    void subscribeIssues() throws MqttException {
        final var subscription = new MqttSubscription(TOPIC);

        when(mqttAsyncClient.isConnected()).thenReturn(true);
        when(mqttAsyncClient.subscribe(subscription)).thenThrow(new MqttException(new RuntimeException("error")));

        final var client = create();
        assertThat(client.subscribe(subscription)).isFalse();

        verify(mqttAsyncClient, times(1)).isConnected();
        verify(mqttAsyncClient, times(1)).subscribe(subscription);

        verifyNoInteractions(entityService);
        verifyNoMoreInteractions(mqttAsyncClient);
    }

    @Test
    void subscribe() throws MqttException {
        final var subscription = new MqttSubscription(TOPIC);
        final var token = mock(MqttToken.class);

        when(mqttAsyncClient.isConnected()).thenReturn(true);
        when(mqttAsyncClient.subscribe(subscription)).thenReturn(token);

        final var client = create();
        assertThat(client.subscribe(subscription)).isTrue();

        verify(token, times(1)).waitForCompletion();
        verifyNoMoreInteractions(token);

        verify(mqttAsyncClient, times(1)).isConnected();
        verify(mqttAsyncClient, times(1)).subscribe(subscription);

        verifyNoInteractions(entityService);
        verifyNoMoreInteractions(mqttAsyncClient);
    }

    @Test
    void subscribeEntityDisabled() {
        final var listenEntity = ListenEntity.with(TOPIC, Qos.ZERO, false);

        final var client = create();
        assertThat(client.subscribe(listenEntity)).isTrue();
    }

    @Test
    void subscribeEntity() {
        final var listenEntity = ListenEntity.with(TOPIC, Qos.ZERO, true);

        final var client = mock(ClientService.class);
        when(client.subscribe(any(MqttSubscription.class))).thenReturn(true);
        when(client.subscribe(listenEntity)).thenCallRealMethod();

        assertThat(client.subscribe(listenEntity)).isTrue();

        verify(client, times(1)).subscribe(listenEntity);
        verify(client, times(1)).subscribe(any(MqttSubscription.class));

        verifyNoMoreInteractions(client);
    }

    @Test
    void refreshSubscriptionsEmpty() {
        when(entityService.allListeners()).thenReturn(List.of());

        final var client = create();
        assertThat(client.refreshSubscriptions()).isTrue();

        verify(entityService, times(1)).allListeners();

        verifyNoMoreInteractions(entityService);
        verifyNoInteractions(mqttAsyncClient);
    }

    @Test
    void refreshSubscriptionsAllDisabled() {
        final var listenEntities =
                List.of(ListenEntity.with(TOPIC, Qos.ONE, false), ListenEntity.with(TOPIC, Qos.TWO, false));

        when(entityService.allListeners()).thenReturn(listenEntities);

        final var client = create();
        assertThat(client.refreshSubscriptions()).isTrue();

        verify(entityService, times(1)).allListeners();

        verifyNoMoreInteractions(entityService);
        verifyNoInteractions(mqttAsyncClient);
    }

    @Test
    void refreshSubscriptions() {
        final var listenEntities =
                List.of(ListenEntity.with(TOPIC, Qos.ONE, true), ListenEntity.with(TOPIC, Qos.TWO, true));

        when(entityService.allListeners()).thenReturn(listenEntities);
        when(mqttAsyncClient.isConnected()).thenReturn(false);

        final var client = create();
        assertThat(client.refreshSubscriptions()).isFalse();

        verify(entityService, times(1)).allListeners();
        verify(mqttAsyncClient, times(1)).isConnected();

        verifyNoMoreInteractions(entityService);
        verifyNoMoreInteractions(mqttAsyncClient);
    }

    @Test
    void sendNullObjects() {
        final var client = create();

        assertThat(client.send(null, MESSAGE)).isFalse();
        assertThat(client.send(TOPIC, null)).isFalse();

        verifyNoInteractions(entityService);
        verifyNoInteractions(mqttAsyncClient);
    }

    @Test
    void sendNotConnected() {
        when(mqttAsyncClient.isConnected()).thenReturn(false);

        final var client = create();
        assertThat(client.send(TOPIC, MESSAGE)).isFalse();

        verify(mqttAsyncClient, times(1)).isConnected();

        verifyNoInteractions(entityService);
        verifyNoMoreInteractions(mqttAsyncClient);
    }

    @Test
    void sendIssues() throws MqttException {
        when(mqttAsyncClient.isConnected()).thenReturn(true);
        when(mqttAsyncClient.publish(TOPIC, MESSAGE)).thenThrow(new MqttException(new RuntimeException("error")));

        final var client = create();
        assertThat(client.send(TOPIC, MESSAGE)).isFalse();

        verify(mqttAsyncClient, times(1)).isConnected();
        verify(mqttAsyncClient, times(1)).publish(TOPIC, MESSAGE);

        verifyNoInteractions(entityService);
        verifyNoMoreInteractions(mqttAsyncClient);
    }

    @Test
    void send() throws MqttException {
        final var token = mock(MqttToken.class);
        when(mqttAsyncClient.isConnected()).thenReturn(true);
        when(mqttAsyncClient.publish(TOPIC, MESSAGE)).thenReturn(token);

        final var client = create();
        assertThat(client.send(TOPIC, MESSAGE)).isTrue();

        verify(token, times(1)).waitForCompletion();
        verifyNoMoreInteractions(token);

        verify(mqttAsyncClient, times(1)).isConnected();
        verify(mqttAsyncClient, times(1)).publish(TOPIC, MESSAGE);

        verifyNoInteractions(entityService);
        verifyNoMoreInteractions(mqttAsyncClient);
    }

    @Test
    void sendBirthMessageDisabled() {
        final var birthStatus = new BirthStatusSetting().setEnabled(false);

        final var client = create();
        assertThat(client.sendBirthMessage(birthStatus)).isFalse();

        verifyNoInteractions(entityService);
        verifyNoInteractions(mqttAsyncClient);
    }

    @Test
    void sendBirthMessage() {
        final var birthStatus = new BirthStatusSetting()
                .setEnabled(true)
                .setMessage("Some Message")
                .setTopic(TOPIC);

        when(mqttAsyncClient.isConnected()).thenReturn(false);

        final var client = create();
        assertThat(client.sendBirthMessage(birthStatus)).isFalse();

        verify(mqttAsyncClient, times(1)).isConnected();

        verifyNoInteractions(entityService);
        verifyNoMoreInteractions(mqttAsyncClient);
    }
}
