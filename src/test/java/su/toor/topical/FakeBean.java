package su.toor.topical;

import java.util.Properties;
import org.springframework.boot.info.BuildProperties;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;

@TestConfiguration
public class FakeBean {

    public static final String APPLICATION_NAME = "some-application";
    public static final String APPLICATION_VERSION = "v1.33.7";
    public static final String APPLICATION_DESCRIPTION = "testing";

    @Bean
    public BuildProperties buildProperties() {
        final var properties = new Properties();
        properties.setProperty("name", APPLICATION_NAME);
        properties.setProperty("version", APPLICATION_VERSION);
        properties.setProperty("description", APPLICATION_DESCRIPTION);

        return new BuildProperties(properties);
    }
}
