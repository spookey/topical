package su.toor.topical.resource.model;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatNoException;
import static su.toor.topical.shared.misc.RequireAssert.assertThatRequireException;

import java.nio.charset.Charset;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import org.junit.jupiter.api.Test;
import org.springframework.boot.autoconfigure.thymeleaf.ThymeleafProperties;
import su.toor.topical.application.setting.BasicSetting;

class CoreModelTest {

    private static final BasicSetting BASIC_SETTING = new BasicSetting();
    private static final String APPLICATION_NAME = "some-name";
    private static final Charset CHARSET = Charset.forName("KOI8-R");

    private static final ThymeleafProperties EMPTY = new ThymeleafProperties();
    private static final ThymeleafProperties PROPERTIES = new ThymeleafProperties();

    static {
        PROPERTIES.setEncoding(CHARSET);
    }

    @Test
    void createInvalid() {
        final var charset = CHARSET.name();

        assertThatRequireException().isThrownBy(() -> new CoreModel(null, APPLICATION_NAME, charset));
        assertThatRequireException().isThrownBy(() -> new CoreModel(BASIC_SETTING, null, charset));
        assertThatRequireException().isThrownBy(() -> new CoreModel(BASIC_SETTING, "\n", charset));
        assertThatRequireException().isThrownBy(() -> new CoreModel(BASIC_SETTING, APPLICATION_NAME, null));
        assertThatRequireException().isThrownBy(() -> new CoreModel(BASIC_SETTING, APPLICATION_NAME, "\n"));
    }

    @Test
    void fromInvalid() {
        assertThatRequireException().isThrownBy(() -> CoreModel.from(null, APPLICATION_NAME, PROPERTIES));
        assertThatRequireException().isThrownBy(() -> CoreModel.from(BASIC_SETTING, null, PROPERTIES));
        assertThatNoException().isThrownBy(() -> CoreModel.from(BASIC_SETTING, APPLICATION_NAME, null));
    }

    @Test
    void from() {
        final var empty = CoreModel.from(BASIC_SETTING, APPLICATION_NAME, EMPTY);
        assertThat(empty.getApplicationName()).isEqualTo(APPLICATION_NAME);
        assertThat(empty.getCharset()).isEqualTo("UTF-8");
        assertThat(empty.getLanguage()).isEqualTo(CoreModel.LOCALE.getLanguage());

        final var model = CoreModel.from(BASIC_SETTING, APPLICATION_NAME, PROPERTIES);
        assertThat(model.getApplicationName()).isEqualTo(APPLICATION_NAME);
        assertThat(model.getCharset()).isEqualTo(CHARSET.name());
        assertThat(empty.getLanguage()).isEqualTo(CoreModel.LOCALE.getLanguage());

        assertThat(model.toString())
                .isNotBlank()
                .contains(APPLICATION_NAME)
                .contains(CHARSET.name())
                .contains(CoreModel.LOCALE.getLanguage());
    }

    @Test
    void shiftPage() {
        assertThat(CoreModel.shiftPage(null)).isZero();
        assertThat(CoreModel.shiftPage(-2)).isZero();
        assertThat(CoreModel.shiftPage(0)).isZero();
        assertThat(CoreModel.shiftPage(1)).isZero();
        assertThat(CoreModel.shiftPage(2)).isOne();
        assertThat(CoreModel.shiftPage(3)).isEqualTo(2);
    }

    @Test
    void showPage() {
        assertThat(CoreModel.showPage(null)).isOne();
        assertThat(CoreModel.showPage(-2)).isOne();
        assertThat(CoreModel.showPage(0)).isOne();
        assertThat(CoreModel.showPage(1)).isEqualTo(1);
        assertThat(CoreModel.showPage(2)).isEqualTo(2);
        assertThat(CoreModel.showPage(3)).isEqualTo(3);
    }

    @Test
    void showSize() {
        final var fallback = 42;
        final var model =
                new CoreModel(new BasicSetting().setPaginationSize(fallback), APPLICATION_NAME, CHARSET.name());

        assertThat(CoreModel.showSize(null, fallback)).isEqualTo(fallback);
        assertThat(model.showSize(null)).isEqualTo(fallback);
        assertThat(CoreModel.showSize(-2, fallback)).isEqualTo(fallback);
        assertThat(model.showSize(-2)).isEqualTo(fallback);
        assertThat(CoreModel.showSize(0, fallback)).isEqualTo(fallback);
        assertThat(model.showSize(0)).isEqualTo(fallback);
        assertThat(CoreModel.showSize(1, fallback)).isOne();
        assertThat(model.showSize(1)).isOne();
        assertThat(CoreModel.showSize(2, fallback)).isEqualTo(2);
        assertThat(model.showSize(2)).isEqualTo(2);
        assertThat(CoreModel.showSize(3, fallback)).isEqualTo(3);
        assertThat(model.showSize(3)).isEqualTo(3);
    }

    @Test
    void pageLink() {
        final var model = new CoreModel(new BasicSetting().setPaginationSize(23), APPLICATION_NAME, CHARSET.name());

        assertThat(model.pageLink(null, null)).isEqualTo("/page/1/size/23");
        assertThat(model.pageLink(-2, null)).isEqualTo("/page/1/size/23");
        assertThat(model.pageLink(null, -2)).isEqualTo("/page/1/size/23");
        assertThat(model.pageLink(0, null)).isEqualTo("/page/1/size/23");
        assertThat(model.pageLink(null, 0)).isEqualTo("/page/1/size/23");
        assertThat(model.pageLink(null, 1)).isEqualTo("/page/1/size/1");
        assertThat(model.pageLink(1, null)).isEqualTo("/page/1/size/23");
        assertThat(model.pageLink(1, 2)).isEqualTo("/page/1/size/2");
        assertThat(model.pageLink(3, 4)).isEqualTo("/page/3/size/4");
    }

    @Test
    void formatTime() {
        final var pattern = "yyyy-MM-dd HH:mm:ss.SSS Z";
        final var model = new CoreModel(new BasicSetting().setTimeFormat(pattern), APPLICATION_NAME, CHARSET.name());

        final var epoch = OffsetDateTime.ofInstant(Instant.EPOCH, ZoneOffset.UTC);
        final var epochExpect = "1970-01-01 00:00:00.000 +0000";

        assertThat(CoreModel.formatTime(epoch, pattern)).isEqualTo(epochExpect);
        assertThat(model.formatTime(epoch)).isEqualTo(epochExpect);
    }
}
