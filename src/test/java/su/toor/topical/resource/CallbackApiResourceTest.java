package su.toor.topical.resource;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.isA;
import static org.hamcrest.Matchers.notNullValue;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static su.toor.topical.resource.JsonOffsetDateTimeMatcher.jOdtEqual;
import static su.toor.topical.resource.JsonUuidMatcher.jUuidEqual;

import com.fasterxml.jackson.databind.json.JsonMapper;
import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.Import;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.HttpMethod;
import org.springframework.test.context.bean.override.mockito.MockitoBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.util.UriComponentsBuilder;
import su.toor.topical.application.config.JsonMapperConfiguration;
import su.toor.topical.database.CallbackEntity;
import su.toor.topical.database.TopicEntity;
import su.toor.topical.resource.dto.CallbackRequestDto;
import su.toor.topical.service.EntityService;

@WebMvcTest(CallbackApiResource.class)
@Import({JsonMapperConfiguration.class})
class CallbackApiResourceTest {

    @Autowired
    private MockMvc mockMvc;

    @MockitoBean
    private EntityService entityService;

    @Autowired
    private JsonMapper jsonMapper;

    private static final String URL_BASE = "/api/callbacks";
    private static final String URL_UUID = URL_BASE + "/{uuid}";

    @Test
    void createCallback() throws Exception {
        final var topic = "some/topic";
        final var method = HttpMethod.PUT;
        final var url = URI.create("https://example.org/api").toURL();
        final var enabled = true;

        final var topicEntity = TopicEntity.with(topic);
        final var callbackEntity = CallbackEntity.with(topicEntity, method, url, enabled);

        when(entityService.createCallback(topic, method, url, enabled)).thenReturn(callbackEntity);

        final var requestDto = new CallbackRequestDto(topic, method, url, enabled);
        final var requestPayload = jsonMapper.writeValueAsString(requestDto);
        final var redirection = UriComponentsBuilder.fromPath(URL_UUID).build(Map.of("uuid", callbackEntity.getUuid()));

        mockMvc.perform(post(URL_BASE).contentType(APPLICATION_JSON).content(requestPayload))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(redirectedUrl(redirection.toString()));

        verify(entityService, times(1)).createCallback(topic, method, url, enabled);
        verifyNoMoreInteractions(entityService);
    }

    @Test
    void allCallbacks() throws Exception {
        final var url = URI.create("https://example.org").toURL();

        final var topicEntity = TopicEntity.with("some/topic");
        final var callbackEntityOne = CallbackEntity.with(topicEntity, HttpMethod.PUT, url, true);
        final var callbackEntityTwo = CallbackEntity.with(topicEntity, HttpMethod.POST, url, false);

        when(entityService.allCallbacks(anyInt(), anyInt()))
                .thenReturn(new PageImpl<>(List.of(callbackEntityOne, callbackEntityTwo)));

        mockMvc.perform(get(URL_BASE))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON))
                .andExpect(jsonPath("$", isA(Map.class)))
                .andExpect(jsonPath("$.size", equalTo(2)))
                .andExpect(jsonPath("$.items", allOf(isA(List.class), hasSize(2))))
                .andExpect(jsonPath("$.items.[0]", isA(Map.class)))
                .andExpect(jsonPath("$.items.[0].topic", isA(Map.class)))
                .andExpect(jsonPath("$.items.[0].topic.name", equalTo(topicEntity.getName())))
                .andExpect(jsonPath(
                        "$.items.[0].method",
                        equalTo(callbackEntityOne.getMethod().name())))
                .andExpect(jsonPath(
                        "$.items.[0].url", equalTo(callbackEntityOne.getUrl().toString())))
                .andExpect(jsonPath("$.items.[0].enabled", equalTo(callbackEntityOne.isEnabled())))
                .andExpect(jsonPath("$.items.[0].uuid", jUuidEqual(callbackEntityOne.getUuid())))
                .andExpect(jsonPath("$.items.[0].created", jOdtEqual(callbackEntityOne.getCreated())))
                .andExpect(jsonPath("$.items.[0].updated", jOdtEqual(callbackEntityOne.getUpdated())))
                .andExpect(jsonPath("$.items.[1]", isA(Map.class)))
                .andExpect(jsonPath("$.items.[1].topic.name", equalTo(topicEntity.getName())))
                .andExpect(jsonPath(
                        "$.items.[1].method",
                        equalTo(callbackEntityTwo.getMethod().name())))
                .andExpect(jsonPath(
                        "$.items.[1].url", equalTo(callbackEntityTwo.getUrl().toString())))
                .andExpect(jsonPath("$.items.[1].enabled", equalTo(callbackEntityTwo.isEnabled())))
                .andExpect(jsonPath("$.items.[1].uuid", jUuidEqual(callbackEntityTwo.getUuid())))
                .andExpect(jsonPath("$.items.[1].created", jOdtEqual(callbackEntityTwo.getCreated())))
                .andExpect(jsonPath("$.items.[1].updated", jOdtEqual(callbackEntityTwo.getUpdated())));

        verify(entityService, times(1)).allCallbacks(anyInt(), anyInt());
        verifyNoMoreInteractions(entityService);
    }

    @Test
    void oneCallback() throws Exception {
        final var url = URI.create("https://example.org").toURL();

        final var topicEntity = TopicEntity.with("some/topic");
        final var callbackEntity = CallbackEntity.with(topicEntity, HttpMethod.PUT, url, true);

        final var wrongUuid = UUID.randomUUID();

        when(entityService.findCallback(wrongUuid)).thenReturn(Optional.empty());
        when(entityService.findCallback(callbackEntity.getUuid())).thenReturn(Optional.of(callbackEntity));

        mockMvc.perform(get(URL_UUID, wrongUuid.toString())).andDo(print()).andExpect(status().isNotFound());

        mockMvc.perform(get(URL_UUID, callbackEntity.getUuid().toString()))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON))
                .andExpect(jsonPath("$", isA(Map.class)))
                .andExpect(jsonPath("$.topic", isA(Map.class)))
                .andExpect(jsonPath("$.topic.name", equalTo(topicEntity.getName())))
                .andExpect(
                        jsonPath("$.method", equalTo(callbackEntity.getMethod().name())))
                .andExpect(jsonPath("$.url", equalTo(callbackEntity.getUrl().toString())))
                .andExpect(jsonPath("$.enabled", equalTo(callbackEntity.isEnabled())))
                .andExpect(jsonPath("$.uuid", jUuidEqual(callbackEntity.getUuid())))
                .andExpect(jsonPath("$.created", jOdtEqual(callbackEntity.getCreated())))
                .andExpect(jsonPath("$.updated", jOdtEqual(callbackEntity.getUpdated())));

        verify(entityService, times(1)).findCallback(wrongUuid);
        verify(entityService, times(1)).findCallback(callbackEntity.getUuid());
        verifyNoMoreInteractions(entityService);
    }

    @Test
    void modifyCallback() throws Exception {
        final var url = URI.create("https://example.org").toURL();
        final var topic = "some/topic";
        final var method = HttpMethod.PUT;
        final var enabled = true;

        final var topicEntity = TopicEntity.with(topic);
        final var callbackEntity = CallbackEntity.with(topicEntity, method, url, enabled);

        final var wrongUuid = UUID.randomUUID();

        when(entityService.modifyCallback(wrongUuid, method, url, enabled)).thenReturn(Optional.empty());
        when(entityService.modifyCallback(callbackEntity.getUuid(), method, url, enabled))
                .thenReturn(Optional.of(callbackEntity));

        final var requestDto = new CallbackRequestDto(topic, method, url, enabled);
        final var requestPayload = jsonMapper.writeValueAsString(requestDto);

        mockMvc.perform(put(URL_UUID, wrongUuid.toString())
                        .contentType(APPLICATION_JSON)
                        .content(requestPayload))
                .andDo(print())
                .andExpect(status().isNotFound());

        mockMvc.perform(put(URL_UUID, callbackEntity.getUuid().toString())
                        .contentType(APPLICATION_JSON)
                        .content(requestPayload))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON))
                .andExpect(jsonPath("$", isA(Map.class)))
                .andExpect(jsonPath("$.topic", isA(Map.class)))
                .andExpect(jsonPath("$.topic.name", equalTo(topic)))
                .andExpect(jsonPath("$.method", equalTo(method.name())))
                .andExpect(jsonPath("$.url", equalTo(url.toString())))
                .andExpect(jsonPath("$.enabled", equalTo(enabled)))
                .andExpect(jsonPath("$.uuid", notNullValue()))
                .andExpect(jsonPath("$.created", notNullValue()))
                .andExpect(jsonPath("$.updated", notNullValue()));

        verify(entityService, times(1)).modifyCallback(wrongUuid, method, url, enabled);
        verify(entityService, times(1)).modifyCallback(callbackEntity.getUuid(), method, url, enabled);
        verifyNoMoreInteractions(entityService);
    }

    @Test
    void deleteCallback() throws Exception {
        final var wrongUuid = UUID.randomUUID();
        final var correctUuid = UUID.randomUUID();

        when(entityService.deleteCallback(wrongUuid)).thenReturn(false);
        when(entityService.deleteCallback(correctUuid)).thenReturn(true);

        mockMvc.perform(delete(URL_UUID, wrongUuid.toString())).andDo(print()).andExpect(status().isNotFound());

        mockMvc.perform(delete(URL_UUID, correctUuid.toString())).andDo(print()).andExpect(status().isOk());

        verify(entityService, times(1)).deleteCallback(wrongUuid);
        verify(entityService, times(1)).deleteCallback(correctUuid);
        verifyNoMoreInteractions(entityService);
    }
}
