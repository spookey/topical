package su.toor.topical.resource.dto;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static su.toor.topical.shared.misc.RequireAssert.assertThatRequireException;

import java.time.OffsetDateTime;
import java.util.UUID;
import org.junit.jupiter.api.Test;
import su.toor.topical.component.mqtt.PayloadParser;
import su.toor.topical.database.MessageEntity;
import su.toor.topical.database.TopicEntity;
import su.toor.topical.shared.Qos;
import su.toor.topical.shared.dto.PayloadDto;

class MessageDtoTest {

    private static final String TOPIC = "some/topic";
    private static final byte[] PAYLOAD = new byte[] {};
    private static final Qos QOS = Qos.ZERO;
    private static final boolean RETAINED = false;
    private static final boolean DUPLICATE = true;
    private static final int MESSAGE_ID = 23;
    private static final UUID UU_ID = UUID.randomUUID();
    private static final OffsetDateTime CREATED = OffsetDateTime.now();
    private static final TopicDto TOPIC_DTO = new TopicDto(TOPIC, UUID.randomUUID(), OffsetDateTime.now());
    private static final PayloadDto PAYLOAD_DTO = new PayloadDto("===", null, null, null, null, null);

    @Test
    void invalid() {
        assertThatRequireException()
                .isThrownBy(() -> new MessageDto(null, PAYLOAD_DTO, null, null, null, null, UU_ID, CREATED));
        assertThatRequireException()
                .isThrownBy(() -> new MessageDto(TOPIC_DTO, null, null, null, null, null, UU_ID, CREATED));
        assertThatRequireException()
                .isThrownBy(() -> new MessageDto(TOPIC_DTO, PAYLOAD_DTO, null, null, null, null, null, CREATED));
        assertThatRequireException()
                .isThrownBy(() -> new MessageDto(TOPIC_DTO, PAYLOAD_DTO, null, null, null, null, UU_ID, null));
    }

    @Test
    void fields() {
        final var dto = new MessageDto(TOPIC_DTO, PAYLOAD_DTO, QOS, RETAINED, DUPLICATE, MESSAGE_ID, UU_ID, CREATED);

        assertThat(dto.topic()).isEqualTo(TOPIC_DTO);
        assertThat(dto.payload()).isEqualTo(PAYLOAD_DTO);
        assertThat(dto.qos()).isEqualTo(QOS);
        assertThat(dto.retained()).isEqualTo(RETAINED);
        assertThat(dto.duplicate()).isEqualTo(DUPLICATE);
        assertThat(dto.messageId()).isEqualTo(MESSAGE_ID);
        assertThat(dto.uuid()).isEqualTo(UU_ID);
        assertThat(dto.created()).isEqualTo(CREATED);

        assertThat(dto.toString())
                .isNotBlank()
                .contains(String.valueOf(TOPIC_DTO))
                .contains(String.valueOf(PAYLOAD_DTO))
                .contains(String.valueOf(QOS))
                .contains(String.valueOf(RETAINED))
                .contains(String.valueOf(DUPLICATE))
                .contains(String.valueOf(MESSAGE_ID))
                .contains(String.valueOf(UU_ID))
                .contains(String.valueOf(CREATED));
    }

    @Test
    void from() {
        final var topicEntity = TopicEntity.with(TOPIC);
        final var messageEntity = MessageEntity.with(topicEntity, PAYLOAD, QOS, RETAINED, DUPLICATE, MESSAGE_ID);

        final var parser = mock(PayloadParser.class);
        when(parser.parse(PAYLOAD)).thenReturn(PAYLOAD_DTO);

        final var dto = MessageDto.from(messageEntity, parser::parse);

        assertThat(dto.topic().name()).isEqualTo(TOPIC);
        assertThat(dto.payload().base64()).isEqualTo(PAYLOAD_DTO.base64());
        assertThat(dto.uuid()).isEqualTo(messageEntity.getUuid());
        assertThat(dto.created()).isEqualTo(messageEntity.getCreated());
    }
}
