package su.toor.topical.resource.dto;

import static org.assertj.core.api.Assertions.assertThat;
import static su.toor.topical.shared.misc.RequireAssert.assertThatRequireException;

import java.net.MalformedURLException;
import java.net.URI;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpMethod;

class CallbackRequestDtoTest {

    private static final String TOPIC = "topic/#";
    private static final HttpMethod METHOD = HttpMethod.PUT;
    private static final boolean ENABLED = false;

    @Test
    void invalid() throws MalformedURLException {
        final var url = URI.create("https://api.example.org").toURL();

        assertThatRequireException().isThrownBy(() -> new CallbackRequestDto(null, METHOD, url, ENABLED));
        assertThatRequireException().isThrownBy(() -> new CallbackRequestDto(TOPIC, METHOD, null, ENABLED));
        assertThatRequireException().isThrownBy(() -> new CallbackRequestDto("c#", METHOD, null, ENABLED));
        assertThatRequireException().isThrownBy(() -> new CallbackRequestDto("c++", METHOD, null, ENABLED));
    }

    @Test
    void missingMethod() throws MalformedURLException {
        final var url = URI.create("https://api.example.org").toURL();
        final var dto = new CallbackRequestDto(TOPIC, null, url, ENABLED);
        assertThat(dto.method()).isEqualTo(HttpMethod.POST);
    }

    @Test
    void missingEnabled() throws MalformedURLException {
        final var url = URI.create("https://api.example.org").toURL();
        final var dto = new CallbackRequestDto(TOPIC, METHOD, url, null);
        assertThat(dto.enabled()).isTrue();
    }

    @Test
    void fields() throws MalformedURLException {
        final var url = URI.create("https://api.example.org").toURL();

        final var dto = new CallbackRequestDto(TOPIC, METHOD, url, ENABLED);

        assertThat(dto.topic()).isEqualTo(TOPIC);
        assertThat(dto.method()).isEqualTo(METHOD);
        assertThat(dto.url()).isEqualTo(url);
        assertThat(dto.enabled()).isEqualTo(ENABLED);

        assertThat(dto.toString())
                .isNotBlank()
                .contains(TOPIC)
                .contains(String.valueOf(METHOD))
                .contains(String.valueOf(url))
                .contains(String.valueOf(ENABLED));
    }
}
