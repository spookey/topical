package su.toor.topical.resource.dto;

import static org.assertj.core.api.Assertions.assertThat;
import static su.toor.topical.shared.misc.RequireAssert.assertThatRequireException;

import java.time.OffsetDateTime;
import java.util.UUID;
import org.junit.jupiter.api.Test;
import su.toor.topical.database.TopicEntity;

class TopicDtoTest {

    private static final String NAME = "some/topic/name";
    private static final UUID UU_ID = UUID.randomUUID();
    private static final OffsetDateTime CREATED = OffsetDateTime.now();

    @Test
    void invalid() {
        assertThatRequireException().isThrownBy(() -> new TopicDto(null, UU_ID, CREATED));
        assertThatRequireException().isThrownBy(() -> new TopicDto("", UU_ID, CREATED));
        assertThatRequireException().isThrownBy(() -> new TopicDto("\t", UU_ID, CREATED));
        assertThatRequireException().isThrownBy(() -> new TopicDto(NAME, null, CREATED));
        assertThatRequireException().isThrownBy(() -> new TopicDto(NAME, UU_ID, null));
    }

    @Test
    void fields() {
        final var dto = new TopicDto(NAME, UU_ID, CREATED);

        assertThat(dto.name()).isEqualTo(NAME);
        assertThat(dto.uuid()).isEqualTo(UU_ID);
        assertThat(dto.created()).isEqualTo(CREATED);

        assertThat(dto.toString())
                .isNotBlank()
                .contains(NAME)
                .contains(String.valueOf(UU_ID))
                .contains(String.valueOf(CREATED));
    }

    @Test
    void from() {
        final var topicEntity = TopicEntity.with(NAME);
        final var dto = TopicDto.from(topicEntity);

        assertThat(dto.name()).isEqualTo(NAME);
        assertThat(dto.uuid()).isEqualTo(topicEntity.getUuid());
        assertThat(dto.created()).isEqualTo(topicEntity.getCreated());
    }
}
