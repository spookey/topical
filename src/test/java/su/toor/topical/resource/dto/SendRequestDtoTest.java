package su.toor.topical.resource.dto;

import static org.assertj.core.api.Assertions.assertThat;
import static su.toor.topical.shared.misc.RequireAssert.assertThatRequireException;

import org.junit.jupiter.api.Test;
import su.toor.topical.shared.Qos;

class SendRequestDtoTest {

    private static final String TOPIC = "topic";
    private static final Qos QOS = Qos.ONE;
    private static final boolean RETAINED = true;
    private static final String VALUE = "value";

    @Test
    void invalid() {
        assertThatRequireException().isThrownBy(() -> new SendRequestDto(null, QOS, RETAINED, VALUE));
        assertThatRequireException().isThrownBy(() -> new SendRequestDto("subscribe/#", QOS, RETAINED, VALUE));
        assertThatRequireException().isThrownBy(() -> new SendRequestDto(TOPIC, null, RETAINED, VALUE));
        assertThatRequireException().isThrownBy(() -> new SendRequestDto(TOPIC, QOS, RETAINED, null));
    }

    @Test
    void retained() {
        assertThat(new SendRequestDto(TOPIC, QOS, null, VALUE).retained()).isFalse();
        assertThat(new SendRequestDto(TOPIC, QOS, false, VALUE).retained()).isFalse();
        assertThat(new SendRequestDto(TOPIC, QOS, true, VALUE).retained()).isTrue();
    }

    @Test
    void fields() {
        final var dto = new SendRequestDto(TOPIC, QOS, RETAINED, VALUE);

        assertThat(dto.topic()).isEqualTo(TOPIC);
        assertThat(dto.qos()).isEqualTo(QOS);
        assertThat(dto.retained()).isEqualTo(RETAINED);
        assertThat(dto.value()).isEqualTo(VALUE);

        assertThat(dto.toString())
                .isNotBlank()
                .contains(TOPIC)
                .contains(String.valueOf(QOS))
                .contains(String.valueOf(RETAINED))
                .contains(VALUE);
    }
}
