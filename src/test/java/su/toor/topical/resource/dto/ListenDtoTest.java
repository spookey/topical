package su.toor.topical.resource.dto;

import static org.assertj.core.api.Assertions.assertThat;
import static su.toor.topical.shared.misc.RequireAssert.assertThatRequireException;

import java.time.OffsetDateTime;
import java.util.UUID;
import org.junit.jupiter.api.Test;
import su.toor.topical.database.ListenEntity;
import su.toor.topical.shared.Qos;

class ListenDtoTest {

    private static final String FILTER = "some/+/topic";
    private static final Qos QOS = Qos.ZERO;
    private static final boolean ENABLED = true;
    private static final UUID UU_ID = UUID.randomUUID();
    private static final OffsetDateTime UPDATED = OffsetDateTime.now();
    private static final OffsetDateTime CREATED = UPDATED.minusSeconds(5);

    @Test
    void invalid() {
        assertThatRequireException().isThrownBy(() -> new ListenDto(null, QOS, ENABLED, UU_ID, CREATED, UPDATED));
        assertThatRequireException().isThrownBy(() -> new ListenDto("c++", QOS, ENABLED, UU_ID, CREATED, UPDATED));
        assertThatRequireException().isThrownBy(() -> new ListenDto(FILTER, null, ENABLED, UU_ID, CREATED, UPDATED));
        assertThatRequireException().isThrownBy(() -> new ListenDto(FILTER, QOS, null, UU_ID, CREATED, UPDATED));
        assertThatRequireException().isThrownBy(() -> new ListenDto(FILTER, QOS, ENABLED, null, CREATED, UPDATED));
        assertThatRequireException().isThrownBy(() -> new ListenDto(FILTER, QOS, ENABLED, UU_ID, null, UPDATED));
        assertThatRequireException().isThrownBy(() -> new ListenDto(FILTER, QOS, ENABLED, UU_ID, CREATED, null));
    }

    @Test
    void fields() {
        final var dto = new ListenDto(FILTER, QOS, ENABLED, UU_ID, CREATED, UPDATED);

        assertThat(dto.filter()).isEqualTo(FILTER);
        assertThat(dto.qos()).isEqualTo(QOS);
        assertThat(dto.enabled()).isEqualTo(ENABLED);
        assertThat(dto.uuid()).isEqualTo(UU_ID);
        assertThat(dto.created()).isEqualTo(CREATED);
        assertThat(dto.updated()).isEqualTo(UPDATED);

        assertThat(dto.toString())
                .isNotBlank()
                .contains(FILTER)
                .contains(String.valueOf(QOS))
                .contains(String.valueOf(ENABLED))
                .contains(String.valueOf(UU_ID))
                .contains(String.valueOf(CREATED))
                .contains(String.valueOf(UPDATED));
    }

    @Test
    void from() {
        final var listenEntity = ListenEntity.with(FILTER, QOS, ENABLED);
        final var dto = ListenDto.from(listenEntity);

        assertThat(dto.filter()).isEqualTo(FILTER);
        assertThat(dto.qos()).isEqualTo(QOS);
        assertThat(dto.enabled()).isEqualTo(ENABLED);
        assertThat(dto.uuid()).isEqualTo(listenEntity.getUuid());
        assertThat(dto.created()).isEqualTo(listenEntity.getCreated());
        assertThat(dto.updated()).isEqualTo(listenEntity.getUpdated());
    }
}
