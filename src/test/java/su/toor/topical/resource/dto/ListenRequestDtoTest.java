package su.toor.topical.resource.dto;

import static org.assertj.core.api.Assertions.assertThat;
import static su.toor.topical.shared.misc.RequireAssert.assertThatRequireException;

import org.junit.jupiter.api.Test;
import su.toor.topical.shared.Qos;

class ListenRequestDtoTest {

    private static final String FILTER = "topic/#";
    private static final Qos QOS = Qos.ONE;
    private static final boolean ENABLED = false;

    @Test
    void invalid() {
        assertThatRequireException().isThrownBy(() -> new ListenRequestDto(null, QOS, ENABLED));
        assertThatRequireException().isThrownBy(() -> new ListenRequestDto("c#", QOS, ENABLED));
    }

    @Test
    void missingQos() {
        final var dto = new ListenRequestDto(FILTER, null, ENABLED);
        assertThat(dto.qos()).isEqualTo(Qos.ZERO);
    }

    @Test
    void missingEnabled() {
        final var dto = new ListenRequestDto(FILTER, QOS, null);
        assertThat(dto.enabled()).isTrue();
    }

    @Test
    void fields() {
        final var dto = new ListenRequestDto(FILTER, QOS, ENABLED);

        assertThat(dto.filter()).isEqualTo(FILTER);
        assertThat(dto.qos()).isEqualTo(QOS);
        assertThat(dto.enabled()).isEqualTo(ENABLED);

        assertThat(dto.toString())
                .isNotBlank()
                .contains(FILTER)
                .contains(String.valueOf(QOS))
                .contains(String.valueOf(ENABLED));
    }
}
