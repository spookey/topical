package su.toor.topical.resource.dto;

import static org.assertj.core.api.Assertions.assertThat;
import static su.toor.topical.shared.misc.RequireAssert.assertThatRequireException;

import java.net.MalformedURLException;
import java.net.URI;
import java.time.OffsetDateTime;
import java.util.UUID;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpMethod;
import su.toor.topical.database.CallbackEntity;
import su.toor.topical.database.TopicEntity;

class CallbackDtoTest {

    private static final String TOPIC = "some/topic";
    private static final TopicDto TOPIC_DTO = new TopicDto(TOPIC, UUID.randomUUID(), OffsetDateTime.now());

    private static final HttpMethod METHOD = HttpMethod.PUT;
    private static final boolean ENABLED = true;
    private static final UUID UU_ID = UUID.randomUUID();
    private static final OffsetDateTime UPDATED = OffsetDateTime.now();
    private static final OffsetDateTime CREATED = UPDATED.minusSeconds(5);

    @Test
    void invalid() throws MalformedURLException {
        final var url = URI.create("https://example.net/api").toURL();

        assertThatRequireException()
                .isThrownBy(() -> new CallbackDto(null, METHOD, url, ENABLED, UU_ID, CREATED, UPDATED));
        assertThatRequireException()
                .isThrownBy(() -> new CallbackDto(TOPIC_DTO, null, url, ENABLED, UU_ID, CREATED, UPDATED));
        assertThatRequireException()
                .isThrownBy(() -> new CallbackDto(TOPIC_DTO, METHOD, null, ENABLED, UU_ID, CREATED, UPDATED));
        assertThatRequireException()
                .isThrownBy(() -> new CallbackDto(TOPIC_DTO, METHOD, url, null, UU_ID, CREATED, UPDATED));
        assertThatRequireException()
                .isThrownBy(() -> new CallbackDto(TOPIC_DTO, METHOD, url, ENABLED, null, CREATED, UPDATED));
        assertThatRequireException()
                .isThrownBy(() -> new CallbackDto(TOPIC_DTO, METHOD, url, ENABLED, UU_ID, null, UPDATED));
        assertThatRequireException()
                .isThrownBy(() -> new CallbackDto(TOPIC_DTO, METHOD, url, ENABLED, UU_ID, CREATED, null));
    }

    @Test
    void fields() throws MalformedURLException {
        final var url = URI.create("https://example.org").toURL();
        final var dto = new CallbackDto(TOPIC_DTO, METHOD, url, ENABLED, UU_ID, CREATED, UPDATED);

        assertThat(dto.topic()).isEqualTo(TOPIC_DTO);
        assertThat(dto.method()).isEqualTo(METHOD);
        assertThat(dto.url()).isEqualTo(url);
        assertThat(dto.enabled()).isEqualTo(ENABLED);
        assertThat(dto.uuid()).isEqualTo(UU_ID);
        assertThat(dto.created()).isEqualTo(CREATED);
        assertThat(dto.updated()).isEqualTo(UPDATED);

        assertThat(dto.toString())
                .isNotBlank()
                .contains(String.valueOf(TOPIC_DTO))
                .contains(String.valueOf(METHOD))
                .contains(String.valueOf(url))
                .contains(String.valueOf(ENABLED))
                .contains(String.valueOf(UU_ID))
                .contains(String.valueOf(CREATED))
                .contains(String.valueOf(UPDATED));
    }

    @Test
    void from() throws MalformedURLException {
        final var url = URI.create("https://example.org").toURL();
        final var topicEntity = TopicEntity.with(TOPIC);
        final var callbackEntity = CallbackEntity.with(topicEntity, METHOD, url, ENABLED);
        final var dto = CallbackDto.from(callbackEntity);

        assertThat(dto.topic().name()).isEqualTo(TOPIC);
        assertThat(dto.method()).isEqualTo(METHOD);
        assertThat(dto.url()).isEqualTo(url);
        assertThat(dto.enabled()).isEqualTo(ENABLED);
        assertThat(dto.uuid()).isEqualTo(callbackEntity.getUuid());
        assertThat(dto.created()).isEqualTo(callbackEntity.getCreated());
        assertThat(dto.updated()).isEqualTo(callbackEntity.getUpdated());
    }
}
