package su.toor.topical.resource;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.isA;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static su.toor.topical.resource.JsonOffsetDateTimeMatcher.jOdtEqual;
import static su.toor.topical.resource.JsonUuidMatcher.jUuidEqual;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.test.context.bean.override.mockito.MockitoBean;
import org.springframework.test.web.servlet.MockMvc;
import su.toor.topical.component.mqtt.PayloadParser;
import su.toor.topical.database.MessageEntity;
import su.toor.topical.database.TopicEntity;
import su.toor.topical.service.EntityService;
import su.toor.topical.shared.Qos;
import su.toor.topical.shared.dto.PayloadDto;

@WebMvcTest(MessageApiResource.class)
class MessageApiResourceTest {

    @Autowired
    private MockMvc mockMvc;

    @MockitoBean
    private EntityService entityService;

    @MockitoBean
    private PayloadParser payloadParser;

    private static final String URL_BASE = "/api/messages";
    private static final String URL_UUID = URL_BASE + "/{uuid}";

    @Test
    void allMessages() throws Exception {
        final var qosOne = Qos.ONE;
        final var qosTwo = Qos.TWO;
        final var topicEntityOne = TopicEntity.with("topic/one");
        final var topicEntityTwo = TopicEntity.with("topic/two");
        final var messageEntityOne = MessageEntity.with(topicEntityOne, new byte[] {0x01}, qosOne, true, false, 1);
        final var messageEntityTwo = MessageEntity.with(topicEntityTwo, new byte[] {0x02}, qosTwo, false, true, 2);
        final var payloadDtoOne = new PayloadDto("===", "one", null, null, null, null);
        final var payloadDtoTwo = new PayloadDto("===", "one", null, null, null, null);

        when(entityService.allMessages(anyInt(), anyInt()))
                .thenReturn(new PageImpl<>(List.of(messageEntityTwo, messageEntityOne)));
        when(payloadParser.parse(messageEntityOne.getPayload())).thenReturn(payloadDtoOne);
        when(payloadParser.parse(messageEntityTwo.getPayload())).thenReturn(payloadDtoTwo);

        mockMvc.perform(get(URL_BASE))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON))
                .andExpect(jsonPath("$", isA(Map.class)))
                .andExpect(jsonPath("$.size", equalTo(2)))
                .andExpect(jsonPath("$.items", allOf(isA(List.class), hasSize(2))))
                .andExpect(jsonPath("$.items.[0]", isA(Map.class)))
                .andExpect(jsonPath("$.items.[0].topic", isA(Map.class)))
                .andExpect(jsonPath("$.items.[0].topic.name", equalTo(topicEntityTwo.getName())))
                .andExpect(jsonPath("$.items.[0].topic.created", jOdtEqual(topicEntityTwo.getCreated())))
                .andExpect(jsonPath("$.items.[0].payload", isA(Map.class)))
                .andExpect(jsonPath("$.items.[0].payload.base64", equalTo(payloadDtoTwo.base64())))
                .andExpect(jsonPath("$.items.[0].payload.string", equalTo(payloadDtoTwo.string())))
                .andExpect(jsonPath("$.items.[0].qos", equalTo(qosTwo.name())))
                .andExpect(jsonPath("$.items.[0].retained", equalTo(messageEntityTwo.isRetained())))
                .andExpect(jsonPath("$.items.[0].duplicate", equalTo(messageEntityTwo.isDuplicate())))
                .andExpect(jsonPath("$.items.[0].messageId", equalTo(messageEntityTwo.getMessageId())))
                .andExpect(jsonPath("$.items.[0].uuid", jUuidEqual(messageEntityTwo.getUuid())))
                .andExpect(jsonPath("$.items.[0].created", jOdtEqual(messageEntityTwo.getCreated())))
                .andExpect(jsonPath("$.items.[1]", isA(Map.class)))
                .andExpect(jsonPath("$.items.[1].topic", isA(Map.class)))
                .andExpect(jsonPath("$.items.[1].topic.name", equalTo(topicEntityOne.getName())))
                .andExpect(jsonPath("$.items.[1].topic.created", jOdtEqual(topicEntityOne.getCreated())))
                .andExpect(jsonPath("$.items.[1].payload", isA(Map.class)))
                .andExpect(jsonPath("$.items.[1].payload.base64", equalTo(payloadDtoOne.base64())))
                .andExpect(jsonPath("$.items.[1].payload.string", equalTo(payloadDtoOne.string())))
                .andExpect(jsonPath("$.items.[1].qos", equalTo(qosOne.name())))
                .andExpect(jsonPath("$.items.[1].retained", equalTo(messageEntityOne.isRetained())))
                .andExpect(jsonPath("$.items.[1].duplicate", equalTo(messageEntityOne.isDuplicate())))
                .andExpect(jsonPath("$.items.[1].messageId", equalTo(messageEntityOne.getMessageId())))
                .andExpect(jsonPath("$.items.[1].uuid", jUuidEqual(messageEntityOne.getUuid())))
                .andExpect(jsonPath("$.items.[1].created", jOdtEqual(messageEntityOne.getCreated())));

        verify(payloadParser, times(1)).parse(messageEntityOne.getPayload());
        verify(payloadParser, times(1)).parse(messageEntityTwo.getPayload());
        verifyNoMoreInteractions(payloadParser);

        verify(entityService, times(1)).allMessages(anyInt(), anyInt());
        verifyNoMoreInteractions(entityService);
    }

    @Test
    void oneMessage() throws Exception {
        final var qos = Qos.ONE;
        final var topicEntity = TopicEntity.with("some/topic");
        final var messageEntity = MessageEntity.with(topicEntity, new byte[] {}, qos, true, false, 0);
        final var payloadDto = new PayloadDto("===", "", null, null, null, null);

        final var wrongUuid = UUID.randomUUID();

        when(payloadParser.parse(messageEntity.getPayload())).thenReturn(payloadDto);

        when(entityService.findMessage(wrongUuid)).thenReturn(Optional.empty());
        when(entityService.findMessage(messageEntity.getUuid())).thenReturn(Optional.of(messageEntity));

        mockMvc.perform(get(URL_UUID, wrongUuid.toString())).andDo(print()).andExpect(status().isNotFound());

        mockMvc.perform(get(URL_UUID, messageEntity.getUuid().toString()))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON))
                .andExpect(jsonPath("$", isA(Map.class)))
                .andExpect(jsonPath("$.topic", isA(Map.class)))
                .andExpect(jsonPath("$.topic.name", equalTo(topicEntity.getName())))
                .andExpect(jsonPath("$.payload", isA(Map.class)))
                .andExpect(jsonPath("$.payload.base64", equalTo(payloadDto.base64())))
                .andExpect(jsonPath("$.payload.string", equalTo(payloadDto.string())))
                .andExpect(jsonPath("$.qos", equalTo(qos.name())))
                .andExpect(jsonPath("$.retained", equalTo(messageEntity.isRetained())))
                .andExpect(jsonPath("$.duplicate", equalTo(messageEntity.isDuplicate())))
                .andExpect(jsonPath("$.messageId", equalTo(messageEntity.getMessageId())))
                .andExpect(jsonPath("$.uuid", jUuidEqual(messageEntity.getUuid())))
                .andExpect(jsonPath("$.created", jOdtEqual(messageEntity.getCreated())));

        verify(payloadParser, times(1)).parse(messageEntity.getPayload());
        verifyNoMoreInteractions(payloadParser);

        verify(entityService, times(1)).findMessage(wrongUuid);
        verify(entityService, times(1)).findMessage(messageEntity.getUuid());
        verifyNoMoreInteractions(entityService);
    }

    @Test
    void deleteMessage() throws Exception {
        final var wrongUuid = UUID.randomUUID();
        final var correctUuid = UUID.randomUUID();

        when(entityService.deleteMessage(wrongUuid)).thenReturn(false);
        when(entityService.deleteMessage(correctUuid)).thenReturn(true);

        mockMvc.perform(delete(URL_UUID, wrongUuid.toString())).andDo(print()).andExpect(status().isNotFound());

        mockMvc.perform(delete(URL_UUID, correctUuid.toString())).andDo(print()).andExpect(status().isOk());

        verify(entityService, times(1)).deleteMessage(wrongUuid);
        verify(entityService, times(1)).deleteMessage(correctUuid);
        verifyNoMoreInteractions(entityService);
    }
}
