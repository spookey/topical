package su.toor.topical.resource;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.isA;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static su.toor.topical.resource.JsonOffsetDateTimeMatcher.jOdtEqual;
import static su.toor.topical.resource.JsonUuidMatcher.jUuidEqual;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.test.context.bean.override.mockito.MockitoBean;
import org.springframework.test.web.servlet.MockMvc;
import su.toor.topical.database.TopicEntity;
import su.toor.topical.service.EntityService;

@WebMvcTest(TopicApiResource.class)
class TopicApiResourceTest {

    @Autowired
    private MockMvc mockMvc;

    @MockitoBean
    private EntityService entityService;

    private static final String URL_BASE = "/api/topics";
    private static final String URL_UUID = URL_BASE + "/{uuid}";

    @Test
    void allTopics() throws Exception {
        final var topicEntityNil = TopicEntity.with("topic/nil");
        final var topicEntityOne = TopicEntity.with("topic/one");
        final var topicEntityTwo = TopicEntity.with("topic/two");

        when(entityService.allTopics(anyInt(), anyInt()))
                .thenReturn(new PageImpl<>(List.of(topicEntityNil, topicEntityOne, topicEntityTwo)));

        mockMvc.perform(get(URL_BASE))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON))
                .andExpect(jsonPath("$", isA(Map.class)))
                .andExpect(jsonPath("$.size", equalTo(3)))
                .andExpect(jsonPath("$.items", allOf(isA(List.class), hasSize(3))))
                .andExpect(jsonPath("$.items.[0]", isA(Map.class)))
                .andExpect(jsonPath("$.items.[0].name", equalTo(topicEntityNil.getName())))
                .andExpect(jsonPath("$.items.[0].uuid", jUuidEqual(topicEntityNil.getUuid())))
                .andExpect(jsonPath("$.items.[0].created", jOdtEqual(topicEntityNil.getCreated())))
                .andExpect(jsonPath("$.items.[1]", isA(Map.class)))
                .andExpect(jsonPath("$.items.[1].name", equalTo(topicEntityOne.getName())))
                .andExpect(jsonPath("$.items.[1].uuid", jUuidEqual(topicEntityOne.getUuid())))
                .andExpect(jsonPath("$.items.[1].created", jOdtEqual(topicEntityOne.getCreated())))
                .andExpect(jsonPath("$.items.[2]", isA(Map.class)))
                .andExpect(jsonPath("$.items.[2].name", equalTo(topicEntityTwo.getName())))
                .andExpect(jsonPath("$.items.[2].uuid", jUuidEqual(topicEntityTwo.getUuid())))
                .andExpect(jsonPath("$.items.[2].created", jOdtEqual(topicEntityTwo.getCreated())));

        verify(entityService, times(1)).allTopics(anyInt(), anyInt());
        verifyNoMoreInteractions(entityService);
    }

    @Test
    void oneTopic() throws Exception {
        final var topicEntity = TopicEntity.with("some/topic");

        final var wrongUuid = UUID.randomUUID();

        when(entityService.findTopic(wrongUuid)).thenReturn(Optional.empty());
        when(entityService.findTopic(topicEntity.getUuid())).thenReturn(Optional.of(topicEntity));

        mockMvc.perform(get(URL_UUID, wrongUuid.toString())).andDo(print()).andExpect(status().isNotFound());

        mockMvc.perform(get(URL_UUID, topicEntity.getUuid().toString()))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON))
                .andExpect(jsonPath("$", isA(Map.class)))
                .andExpect(jsonPath("$.name", equalTo(topicEntity.getName())))
                .andExpect(jsonPath("$.uuid", jUuidEqual(topicEntity.getUuid())))
                .andExpect(jsonPath("$.created", jOdtEqual(topicEntity.getCreated())));

        verify(entityService, times(1)).findTopic(wrongUuid);
        verify(entityService, times(1)).findTopic(topicEntity.getUuid());
        verifyNoMoreInteractions(entityService);
    }

    @Test
    void deleteTopic() throws Exception {
        final var wrongUuid = UUID.randomUUID();
        final var correctUuid = UUID.randomUUID();

        when(entityService.deleteTopic(wrongUuid)).thenReturn(false);
        when(entityService.deleteTopic(correctUuid)).thenReturn(true);

        mockMvc.perform(delete(URL_UUID, wrongUuid.toString())).andDo(print()).andExpect(status().isNotFound());

        mockMvc.perform(delete(URL_UUID, correctUuid.toString())).andDo(print()).andExpect(status().isOk());

        verify(entityService, times(1)).deleteTopic(wrongUuid);
        verify(entityService, times(1)).deleteTopic(correctUuid);
        verifyNoMoreInteractions(entityService);
    }
}
