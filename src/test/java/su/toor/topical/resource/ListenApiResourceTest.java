package su.toor.topical.resource;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.isA;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static su.toor.topical.resource.JsonOffsetDateTimeMatcher.jOdtEqual;
import static su.toor.topical.resource.JsonUuidMatcher.jUuidEqual;

import com.fasterxml.jackson.databind.json.JsonMapper;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.Import;
import org.springframework.data.domain.PageImpl;
import org.springframework.test.context.bean.override.mockito.MockitoBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.util.UriComponentsBuilder;
import su.toor.topical.application.config.JsonMapperConfiguration;
import su.toor.topical.database.ListenEntity;
import su.toor.topical.resource.dto.ListenRequestDto;
import su.toor.topical.service.ClientService;
import su.toor.topical.service.EntityService;
import su.toor.topical.shared.Qos;

@WebMvcTest(ListenApiResource.class)
@Import({JsonMapperConfiguration.class})
class ListenApiResourceTest {

    @Autowired
    private MockMvc mockMvc;

    @MockitoBean
    private EntityService entityService;

    @MockitoBean
    private ClientService clientService;

    @Autowired
    private JsonMapper jsonMapper;

    private static final String URL_BASE = "/api/listen";
    private static final String URL_UUID = URL_BASE + "/{uuid}";

    @Test
    void createListener() throws Exception {
        final var filter = "some/+/topic";
        final var qos = Qos.ONE;
        final var enabled = true;

        final var listenEntity = ListenEntity.with(filter, qos, enabled);

        when(entityService.createListener(filter, qos, enabled)).thenReturn(listenEntity);

        final var requestDto = new ListenRequestDto(filter, qos, enabled);
        final var requestPayload = jsonMapper.writeValueAsString(requestDto);
        final var redirection = UriComponentsBuilder.fromPath(URL_UUID).build(Map.of("uuid", listenEntity.getUuid()));

        mockMvc.perform(post(URL_BASE).contentType(APPLICATION_JSON).content(requestPayload))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(redirectedUrl(redirection.toString()));

        verify(entityService, times(1)).createListener(filter, qos, enabled);
        verifyNoMoreInteractions(entityService);

        verify(clientService, times(1)).refreshSubscriptions();
        verifyNoMoreInteractions(clientService);
    }

    @Test
    void allListeners() throws Exception {
        final var listenEntityOne = ListenEntity.with("some/filter", Qos.ONE, false);
        final var listenEntityTwo = ListenEntity.with("filter/+", Qos.TWO, true);

        when(entityService.allListeners(anyInt(), anyInt()))
                .thenReturn(new PageImpl<>(List.of(listenEntityOne, listenEntityTwo)));

        mockMvc.perform(get(URL_BASE))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON))
                .andExpect(jsonPath("$", isA(Map.class)))
                .andExpect(jsonPath("$.size", equalTo(2)))
                .andExpect(jsonPath("$.items", allOf(isA(List.class), hasSize(2))))
                .andExpect(jsonPath("$.items.[0]", isA(Map.class)))
                .andExpect(jsonPath("$.items.[0].filter", equalTo(listenEntityOne.getFilter())))
                .andExpect(jsonPath(
                        "$.items.[0].qos", equalTo(listenEntityOne.getQos().name())))
                .andExpect(jsonPath("$.items.[0].enabled", equalTo(listenEntityOne.isEnabled())))
                .andExpect(jsonPath("$.items.[0].uuid", jUuidEqual(listenEntityOne.getUuid())))
                .andExpect(jsonPath("$.items.[0].created", jOdtEqual(listenEntityOne.getCreated())))
                .andExpect(jsonPath("$.items.[0].updated", jOdtEqual(listenEntityOne.getUpdated())))
                .andExpect(jsonPath("$.items.[1]", isA(Map.class)))
                .andExpect(jsonPath("$.items.[1].filter", equalTo(listenEntityTwo.getFilter())))
                .andExpect(jsonPath(
                        "$.items.[1].qos", equalTo(listenEntityTwo.getQos().name())))
                .andExpect(jsonPath("$.items.[1].enabled", equalTo(listenEntityTwo.isEnabled())))
                .andExpect(jsonPath("$.items.[1].uuid", jUuidEqual(listenEntityTwo.getUuid())))
                .andExpect(jsonPath("$.items.[1].created", jOdtEqual(listenEntityTwo.getCreated())))
                .andExpect(jsonPath("$.items.[1].updated", jOdtEqual(listenEntityTwo.getUpdated())));

        verify(entityService, times(1)).allListeners(anyInt(), anyInt());
        verifyNoMoreInteractions(entityService);
    }

    @Test
    void oneListener() throws Exception {
        final var listenEntity = ListenEntity.with("some/#", Qos.TWO, true);

        final var wrongUuid = UUID.randomUUID();

        when(entityService.findListener(wrongUuid)).thenReturn(Optional.empty());
        when(entityService.findListener(listenEntity.getUuid())).thenReturn(Optional.of(listenEntity));

        mockMvc.perform(get(URL_UUID, wrongUuid.toString())).andDo(print()).andExpect(status().isNotFound());

        mockMvc.perform(get(URL_UUID, listenEntity.getUuid().toString()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON))
                .andExpect(jsonPath("$", isA(Map.class)))
                .andExpect(jsonPath("$.filter", equalTo(listenEntity.getFilter())))
                .andExpect(jsonPath("$.qos", equalTo(listenEntity.getQos().name())))
                .andExpect(jsonPath("$.enabled", equalTo(listenEntity.isEnabled())))
                .andExpect(jsonPath("$.uuid", jUuidEqual(listenEntity.getUuid())))
                .andExpect(jsonPath("$.created", jOdtEqual(listenEntity.getCreated())))
                .andExpect(jsonPath("$.updated", jOdtEqual(listenEntity.getUpdated())));

        verify(entityService, times(1)).findListener(wrongUuid);
        verify(entityService, times(1)).findListener(listenEntity.getUuid());
        verifyNoMoreInteractions(entityService);
    }

    @Test
    void modifyListener() throws Exception {
        final var filter = "filter/+";
        final var qos = Qos.ZERO;
        final var enabled = false;

        final var listenEntity = ListenEntity.with(filter, qos, enabled);

        final var wrongUuid = UUID.randomUUID();

        when(entityService.modifyListener(wrongUuid, filter, qos, enabled)).thenReturn(Optional.empty());
        when(entityService.modifyListener(listenEntity.getUuid(), filter, qos, enabled))
                .thenReturn(Optional.of(listenEntity));

        final var requestDto = new ListenRequestDto(filter, qos, enabled);
        final var requestPayload = jsonMapper.writeValueAsString(requestDto);

        mockMvc.perform(put(URL_UUID, wrongUuid.toString())
                        .contentType(APPLICATION_JSON)
                        .content(requestPayload))
                .andDo(print())
                .andExpect(status().isNotFound());

        mockMvc.perform(put(URL_UUID, listenEntity.getUuid().toString())
                        .contentType(APPLICATION_JSON)
                        .content(requestPayload))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON))
                .andExpect(jsonPath("$", isA(Map.class)))
                .andExpect(jsonPath("$.filter", equalTo(listenEntity.getFilter())))
                .andExpect(jsonPath("$.qos", equalTo(listenEntity.getQos().name())))
                .andExpect(jsonPath("$.enabled", equalTo(listenEntity.isEnabled())))
                .andExpect(jsonPath("$.uuid", jUuidEqual(listenEntity.getUuid())))
                .andExpect(jsonPath("$.created", jOdtEqual(listenEntity.getCreated())))
                .andExpect(jsonPath("$.updated", jOdtEqual(listenEntity.getUpdated())));

        verify(entityService, times(1)).modifyListener(wrongUuid, filter, qos, enabled);
        verify(entityService, times(1)).modifyListener(listenEntity.getUuid(), filter, qos, enabled);
        verifyNoMoreInteractions(entityService);
    }

    @Test
    void deleteListener() throws Exception {
        final var wrongUuid = UUID.randomUUID();
        final var correctUuid = UUID.randomUUID();

        when(entityService.deleteListener(wrongUuid)).thenReturn(false);
        when(entityService.deleteListener(correctUuid)).thenReturn(true);

        mockMvc.perform(delete(URL_UUID, wrongUuid.toString())).andDo(print()).andExpect(status().isNotFound());

        mockMvc.perform(delete(URL_UUID, correctUuid.toString())).andDo(print()).andExpect(status().isOk());

        verify(entityService, times(1)).deleteListener(wrongUuid);
        verify(entityService, times(1)).deleteListener(correctUuid);
        verifyNoMoreInteractions(entityService);
    }
}
