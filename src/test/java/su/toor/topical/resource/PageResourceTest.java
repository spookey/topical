package su.toor.topical.resource;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.instanceOf;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.TEXT_HTML;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.net.URI;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.info.BuildProperties;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.Import;
import org.springframework.data.domain.PageImpl;
import org.springframework.test.context.bean.override.mockito.MockitoBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.result.ModelResultMatchers;
import org.springframework.web.util.UriComponentsBuilder;
import su.toor.topical.FakeBean;
import su.toor.topical.application.setting.BasicSetting;
import su.toor.topical.component.mqtt.PayloadParser;
import su.toor.topical.database.MessageEntity;
import su.toor.topical.database.TopicEntity;
import su.toor.topical.resource.model.CoreModel;
import su.toor.topical.service.EntityService;
import su.toor.topical.shared.Qos;
import su.toor.topical.shared.dto.PayloadDto;

@WebMvcTest(PageResource.class)
@Import(FakeBean.class)
class PageResourceTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private BuildProperties buildProperties;

    @MockitoBean
    private EntityService entityService;

    @MockitoBean
    private PayloadParser payloadParser;

    @MockitoBean
    private BasicSetting basicSetting;

    private static ResultMatcher verifyCoreModel(final ModelResultMatchers modelResultMatchers) {
        return modelResultMatchers.attribute(
                PageResource.FIELD_CORE,
                allOf(
                        instanceOf(CoreModel.class),
                        hasProperty("language"),
                        hasProperty("charset"),
                        hasProperty("applicationName")));
    }

    private static URI pageUri(final int page, final int size) {
        return UriComponentsBuilder.fromPath("/page/{page}/size/{size}").build(Map.of("page", page, "size", size));
    }

    @Test
    void getIndexEmpty() throws Exception {
        final var reqPage = 3;
        final var reqSize = 4;

        when(entityService.recentMessages(reqPage - 1, reqSize)).thenReturn(new PageImpl<>(List.of()));

        when(basicSetting.getPaginationSize()).thenReturn(8);

        mockMvc.perform(get(pageUri(reqPage, reqSize)).accept(TEXT_HTML))
                .andDo(print())
                .andExpect(status().isNotFound());

        verify(entityService, times(1)).recentMessages(reqPage - 1, reqSize);
        verifyNoMoreInteractions(entityService);

        verifyNoInteractions(payloadParser);

        verify(basicSetting, times(1)).getPaginationSize();
        verifyNoMoreInteractions(basicSetting);
    }

    @Test
    void getIndex() throws Exception {
        final var topicEntity = TopicEntity.with("/some/topic");
        final var messageEntityOne = MessageEntity.with(topicEntity, new byte[] {0x01}, Qos.ONE, true, false, 1);
        final var messageEntityTwo = MessageEntity.with(topicEntity, new byte[] {0x02}, Qos.TWO, false, true, 2);
        final var payloadOne = new PayloadDto("1==", "string-one", "json-1", 1, 1d, true);
        final var payloadTwo = new PayloadDto("2==", "string-two", "json-2", 2, 2d, true);

        final var reqPage = 1;
        final var reqSize = 2;

        when(entityService.recentMessages(reqPage - 1, reqSize))
                .thenReturn(new PageImpl<>(List.of(messageEntityTwo, messageEntityOne)));
        when(entityService.countTopics()).thenReturn(23L);
        when(entityService.countMessages()).thenReturn(1337L);

        when(payloadParser.parse(messageEntityOne.getPayload())).thenReturn(payloadOne);
        when(payloadParser.parse(messageEntityTwo.getPayload())).thenReturn(payloadTwo);

        when(basicSetting.getPaginationSize()).thenReturn(8);
        when(basicSetting.getTimeFormat()).thenReturn("yyyy");

        mockMvc.perform(get(pageUri(reqPage, reqSize)).accept(TEXT_HTML))
                .andDo(print())
                .andExpect(content().contentTypeCompatibleWith(TEXT_HTML))
                .andExpect(status().isOk())
                .andExpect(view().name(PageResource.VIEW_INDEX))
                .andExpect(verifyCoreModel(model()))
                .andExpect(content().string(containsString(buildProperties.getName())))
                .andExpect(content()
                        .string(allOf(
                                containsString(topicEntity.getName()),
                                containsString(payloadOne.base64()),
                                containsString(payloadTwo.base64()),
                                containsString(payloadOne.string()),
                                containsString(payloadTwo.string()),
                                containsString(String.valueOf(payloadOne.json())),
                                containsString(String.valueOf(payloadTwo.json())))));

        verify(entityService, times(1)).recentMessages(reqPage - 1, reqSize);
        verify(entityService, times(1)).countTopics();
        verify(entityService, times(1)).countMessages();
        verifyNoMoreInteractions(entityService);

        verify(payloadParser, times(2)).parse(any());
        verifyNoMoreInteractions(payloadParser);

        verify(basicSetting, atLeastOnce()).getPaginationSize();
        verify(basicSetting, atLeastOnce()).getTimeFormat();
        verifyNoMoreInteractions(basicSetting);
    }
}
