package su.toor.topical.resource;

import java.time.OffsetDateTime;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;

class JsonOffsetDateTimeMatcher extends BaseMatcher<OffsetDateTime> {

    private final OffsetDateTime expect;

    JsonOffsetDateTimeMatcher(final OffsetDateTime expect) {
        this.expect = expect;
    }

    public static JsonOffsetDateTimeMatcher jOdtEqual(final OffsetDateTime expect) {
        return new JsonOffsetDateTimeMatcher(expect);
    }

    @Override
    public boolean matches(final Object item) {
        final var value = OffsetDateTime.parse(item.toString());
        final var instant = value.toInstant();

        return instant.equals(expect.toInstant());
    }

    @Override
    public void describeTo(final Description description) {
        description.appendText("<%s>".formatted(expect));
    }
}
