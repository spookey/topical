package su.toor.topical.resource;

import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.json.JsonMapper;
import java.util.Map;
import org.eclipse.paho.mqttv5.common.MqttMessage;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.bean.override.mockito.MockitoBean;
import org.springframework.test.web.servlet.MockMvc;
import su.toor.topical.application.config.JsonMapperConfiguration;
import su.toor.topical.component.mqtt.PayloadParser;
import su.toor.topical.resource.dto.SendRequestDto;
import su.toor.topical.service.ClientService;
import su.toor.topical.shared.Qos;

@WebMvcTest(SendApiResource.class)
@Import({JsonMapperConfiguration.class})
class SendApiResourceTest {

    @Autowired
    private MockMvc mockMvc;

    @MockitoBean
    private ClientService clientService;

    @MockitoBean
    private PayloadParser payloadParser;

    @Autowired
    private JsonMapper jsonMapper;

    private static final String URL_SEND = "/api/send";

    @Test
    void sendMessage() throws Exception {
        final var topic = "send/message/test";
        final var qos = Qos.ONE;
        final var retained = true;
        final var value = "some message text";

        final var message = new MqttMessage();

        final var sendDto = new SendRequestDto(topic, qos, retained, value);
        final var sendPayload = jsonMapper.writeValueAsString(sendDto);

        final var states = Map.of(
                true, HttpStatus.CREATED,
                false, HttpStatus.FAILED_DEPENDENCY);
        for (final var state : states.entrySet()) {
            final var status = state.getValue();

            when(payloadParser.compose(value, qos, retained)).thenReturn(message);

            when(clientService.send(topic, message)).thenReturn(state.getKey());

            mockMvc.perform(post(URL_SEND).contentType(APPLICATION_JSON).content(sendPayload))
                    .andDo(print())
                    .andExpect(status().is(status.value()));

            verify(payloadParser, times(1)).compose(value, qos, retained);
            verifyNoMoreInteractions(payloadParser);
            reset(payloadParser);

            verify(clientService, times(1)).send(topic, message);
            verifyNoMoreInteractions(clientService);
            reset(clientService);
        }
    }
}
