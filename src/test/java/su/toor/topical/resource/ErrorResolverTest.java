package su.toor.topical.resource;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;

import jakarta.servlet.http.HttpServletRequest;
import java.util.Map;
import org.junit.jupiter.api.Test;
import org.springframework.boot.autoconfigure.thymeleaf.ThymeleafProperties;
import org.springframework.http.HttpStatus;
import su.toor.topical.FakeBean;
import su.toor.topical.application.setting.BasicSetting;
import su.toor.topical.resource.model.CoreModel;

class ErrorResolverTest {

    private static final FakeBean FAKE_BEAN = new FakeBean();

    @Test
    void resolveErrorView() {
        final var httpServletRequest = mock(HttpServletRequest.class);
        final var httpStatus = HttpStatus.BAD_REQUEST;
        final var existingModelKey = "key";
        final var existingModelValue = new Object();

        final var immutableModel = Map.of(existingModelKey, existingModelValue);

        final var buildProperties = FAKE_BEAN.buildProperties();
        final var errorResolver = new ErrorResolver(buildProperties, new BasicSetting(), new ThymeleafProperties());

        final var result = errorResolver.resolveErrorView(httpServletRequest, httpStatus, immutableModel);

        assertThat(result).isNotNull();
        assertThat(result.getViewName()).isEqualTo(ErrorResolver.VIEW_ERROR);
        assertThat(result.getStatus()).isEqualTo(httpStatus);

        final var model = result.getModel();
        assertThat(model).isNotNull();

        assertThat(model.getOrDefault(existingModelKey, null)).isNotNull().isEqualTo(existingModelValue);
        assertThat(model.getOrDefault(ErrorResolver.FIELD_CORE, null))
                .isNotNull()
                .isInstanceOf(CoreModel.class);
    }
}
