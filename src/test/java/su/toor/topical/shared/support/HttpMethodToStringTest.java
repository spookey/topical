package su.toor.topical.shared.support;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.springframework.http.HttpMethod;

class HttpMethodToStringTest {

    private static final HttpMethodToString CONVERTER = new HttpMethodToString();

    @ParameterizedTest
    @ArgumentsSource(value = HttpMethodArgumentsProvider.class)
    void convert(final HttpMethod method) {
        assertThat(CONVERTER.convert(null)).isNull();

        assertThat(CONVERTER.convert(method)).isEqualTo(method.name());
    }
}
