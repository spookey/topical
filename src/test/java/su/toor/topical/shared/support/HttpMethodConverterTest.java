package su.toor.topical.shared.support;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.springframework.http.HttpMethod;

class HttpMethodConverterTest {

    private static final HttpMethodConverter CONVERTER = new HttpMethodConverter();

    @ParameterizedTest
    @ArgumentsSource(value = HttpMethodArgumentsProvider.class)
    void convertToDatabaseColumn(final HttpMethod method) {
        assertThat(CONVERTER.convertToDatabaseColumn(null)).isNull();

        assertThat(CONVERTER.convertToDatabaseColumn(method)).isEqualTo(method.name());
    }

    @ParameterizedTest
    @ArgumentsSource(value = HttpMethodArgumentsProvider.class)
    void convertToEntityAttribute(final HttpMethod method) {
        assertThat(CONVERTER.convertToEntityAttribute(null)).isNull();
        assertThat(CONVERTER.convertToEntityAttribute("")).isNull();

        assertThat(CONVERTER.convertToEntityAttribute(method.name())).isEqualTo(method);

        final var name = "SOMETHING";
        final var custom = CONVERTER.convertToEntityAttribute(name);
        assertThat(custom).isNotNull();
        assertThat(custom.name()).isEqualTo(name);
    }
}
