package su.toor.topical.shared.support;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.springframework.http.HttpMethod;

class StringToHttpMethodTest {

    private static final StringToHttpMethod CONVERTER = new StringToHttpMethod();

    @ParameterizedTest
    @ArgumentsSource(value = HttpMethodArgumentsProvider.class)
    void convert(final HttpMethod method) {
        assertThat(CONVERTER.convert(null)).isNull();
        assertThat(CONVERTER.convert("")).isNull();

        assertThat(CONVERTER.convert(method.name())).isEqualTo(method);

        final var name = "SOMETHING";
        final var custom = CONVERTER.convert(name);
        assertThat(custom).isNotNull();
        assertThat(custom.name()).isEqualTo(name);
    }
}
