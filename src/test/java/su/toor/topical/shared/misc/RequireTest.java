package su.toor.topical.shared.misc;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatIllegalArgumentException;
import static su.toor.topical.shared.misc.RequireAssert.assertThatRequireException;

import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.Stream;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;

class RequireTest {

    @Nested
    class RequireExceptionTest {

        @ParameterizedTest
        @ValueSource(strings = {"error", "message"})
        @NullAndEmptySource
        void create(final String message) {
            assertThat(new Require.RequireException(message)).hasMessage(message);
        }
    }

    @Test
    void condition() {
        final var message = "error message";
        final String value = "content";

        assertThatIllegalArgumentException()
                .isThrownBy(() -> Require.condition(null, message, 42))
                .withMessage("check is null");
        assertThatIllegalArgumentException()
                .isThrownBy(() -> Require.condition(Objects::nonNull, null, 23))
                .withMessage("message is null, empty or blank");
        assertThatIllegalArgumentException()
                .isThrownBy(() -> Require.condition(Objects::nonNull, "", 23))
                .withMessage("message is null, empty or blank");
        assertThatIllegalArgumentException()
                .isThrownBy(() -> Require.condition(Objects::nonNull, "\t", 42))
                .withMessage("message is null, empty or blank");

        assertThatRequireException()
                .isThrownBy(() -> Require.condition(String::isEmpty, message, value))
                .withMessage(message);

        assertThat(Require.condition(Predicate.not(String::isEmpty), message, value))
                .isEqualTo(value);
    }

    @Test
    void notNull() {
        final var fieldName = "something";
        final var number = 13.37f;

        assertThatRequireException()
                .isThrownBy(() -> Require.notNull(fieldName, null))
                .withMessage(fieldName + " is null");

        assertThat(Require.notNull(fieldName, number)).isEqualTo(number);
    }

    @Test
    void notEmpty() {
        final var fieldName = "something";
        final var text = "text";

        assertThatRequireException()
                .isThrownBy(() -> Require.notEmpty(fieldName, null))
                .withMessage(fieldName + " is null");

        assertThatRequireException()
                .isThrownBy(() -> Require.notEmpty(fieldName, ""))
                .withMessage(fieldName + " is empty");

        assertThat(Require.notEmpty(fieldName, text)).isEqualTo(text);
    }

    @Test
    void notBlank() {
        final var fieldName = "something";
        final var text = "text";

        assertThatRequireException()
                .isThrownBy(() -> Require.notBlank(fieldName, null))
                .withMessage(fieldName + " is null");

        assertThatRequireException()
                .isThrownBy(() -> Require.notBlank(fieldName, ""))
                .withMessage(fieldName + " is empty");

        assertThatRequireException()
                .isThrownBy(() -> Require.notBlank(fieldName, "\t"))
                .withMessage(fieldName + " is blank");

        assertThat(Require.notBlank(fieldName, text)).isEqualTo(text);
    }

    @Test
    void greaterEqualThan() {
        final var fieldName = "numeric";
        final var value = 1337L;
        final var bound = 1312L;

        assertThatRequireException()
                .isThrownBy(() -> Require.greaterEqualThan(fieldName, value, null))
                .withMessage(fieldName + ".bound is null");

        assertThatRequireException()
                .isThrownBy(() -> Require.greaterEqualThan(fieldName, null, bound))
                .withMessage(fieldName + " is null");

        assertThatRequireException()
                .isThrownBy(() -> Require.greaterEqualThan(fieldName, bound, value))
                .withMessage(fieldName + " is not greater or equal than " + value);

        assertThat(Require.greaterEqualThan(fieldName, value, value)).isEqualTo(value);
        assertThat(Require.greaterEqualThan(fieldName, value, bound)).isEqualTo(value);
    }

    @Test
    void validListenTopic() {
        Stream.of("", "\t", "c++", "c#").forEach(name -> assertThatRequireException()
                .describedAs(name)
                .isThrownBy(() -> Require.validListenTopic(name))
                .withMessageContaining("listen topic"));

        Stream.of("topic", "$share/topic", "some/+/topic", "some/topic/#")
                .forEach(name -> assertThat(Require.validListenTopic(name)).isEqualTo(name));
    }

    @Test
    void validMessageTopic() {
        Stream.of("", "\n", "c++", "c#", "$share/topic", "some/+/topic", "some/topic/#")
                .forEach(name -> assertThatRequireException()
                        .describedAs(name)
                        .isThrownBy(() -> Require.validMessageTopic(name))
                        .withMessageContaining("message topic"));

        Stream.of("topic", "some/topic")
                .forEach(name -> assertThat(Require.validMessageTopic(name)).isEqualTo(name));
    }
}
