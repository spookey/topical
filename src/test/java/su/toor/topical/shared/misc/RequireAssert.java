package su.toor.topical.shared.misc;

import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

import org.assertj.core.api.ThrowableTypeAssert;

public final class RequireAssert {
    private RequireAssert() {}

    public static ThrowableTypeAssert<Require.RequireException> assertThatRequireException() {
        return assertThatExceptionOfType(Require.RequireException.class);
    }
}
