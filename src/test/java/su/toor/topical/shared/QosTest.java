package su.toor.topical.shared;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Map;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

class QosTest {

    private static final Map<Qos, Integer> VALUES = Map.of(
            Qos.ZERO, 0,
            Qos.ONE, 1,
            Qos.TWO, 2);

    @ParameterizedTest
    @EnumSource(Qos.class)
    void value(final Qos qos) {
        final var expect = VALUES.getOrDefault(qos, null);
        assertThat(expect).isNotNull();

        assertThat(qos.ordinal()).isEqualTo(expect);
    }

    @ParameterizedTest
    @EnumSource(Qos.class)
    void of(final Qos qos) {
        assertThat(Qos.of(null)).isNull();
        assertThat(Qos.of(23)).isNull();
        assertThat(Qos.of(-1)).isNull();

        assertThat(Qos.of(qos.ordinal())).isEqualTo(qos);
    }
}
