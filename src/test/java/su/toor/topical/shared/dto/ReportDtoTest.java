package su.toor.topical.shared.dto;

import static org.assertj.core.api.Assertions.assertThat;
import static su.toor.topical.shared.misc.RequireAssert.assertThatRequireException;

import java.time.Duration;
import org.junit.jupiter.api.Test;

class ReportDtoTest {

    private static final String TITLE = "title";
    private static final long SENT = 1337L;
    private static final long COLLECTED = 42L;
    private static final long PURGED = 23L;
    private static final long UPTIME_MILLIS = 1312L;
    private static final Duration DURATION = Duration.ofMillis(UPTIME_MILLIS);

    @Test
    void invalid() {
        assertThatRequireException().isThrownBy(() -> new ReportDto(null, SENT, COLLECTED, PURGED, DURATION));
        assertThatRequireException().isThrownBy(() -> new ReportDto("", SENT, COLLECTED, PURGED, DURATION));
        assertThatRequireException().isThrownBy(() -> new ReportDto("\t", SENT, COLLECTED, PURGED, DURATION));
        assertThatRequireException().isThrownBy(() -> new ReportDto(TITLE, null, COLLECTED, PURGED, DURATION));
        assertThatRequireException().isThrownBy(() -> new ReportDto(TITLE, -1 * SENT, COLLECTED, PURGED, DURATION));
        assertThatRequireException().isThrownBy(() -> new ReportDto(TITLE, SENT, null, PURGED, DURATION));
        assertThatRequireException().isThrownBy(() -> new ReportDto(TITLE, SENT, -1 * COLLECTED, PURGED, DURATION));
        assertThatRequireException().isThrownBy(() -> new ReportDto(TITLE, COLLECTED, SENT, null, DURATION));
        assertThatRequireException().isThrownBy(() -> new ReportDto(TITLE, COLLECTED, SENT, -1 * PURGED, DURATION));
        assertThatRequireException().isThrownBy(() -> new ReportDto(TITLE, COLLECTED, SENT, PURGED, (Duration) null));
    }

    @Test
    void invalidMillis() {
        assertThatRequireException()
                .isThrownBy(() -> new ReportDto(TITLE, COLLECTED, SENT, PURGED, -1 * UPTIME_MILLIS));
        assertThatRequireException().isThrownBy(() -> new ReportDto(TITLE, COLLECTED, SENT, PURGED, (Long) null));
    }

    @Test
    void fields() {
        final var dto = new ReportDto(TITLE, SENT, COLLECTED, PURGED, UPTIME_MILLIS);

        assertThat(dto.title()).isEqualTo(TITLE);
        assertThat(dto.sent()).isEqualTo(SENT);
        assertThat(dto.collected()).isEqualTo(COLLECTED);
        assertThat(dto.purged()).isEqualTo(PURGED);
        assertThat(dto.uptime()).isEqualByComparingTo(DURATION);

        assertThat(dto.toString())
                .isNotBlank()
                .contains(TITLE)
                .contains(String.valueOf(SENT))
                .contains(String.valueOf(COLLECTED))
                .contains(String.valueOf(PURGED))
                .contains(String.valueOf(DURATION));
    }
}
