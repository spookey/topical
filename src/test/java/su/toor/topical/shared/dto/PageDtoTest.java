package su.toor.topical.shared.dto;

import static org.assertj.core.api.Assertions.assertThat;
import static su.toor.topical.shared.misc.RequireAssert.assertThatRequireException;

import java.util.List;
import java.util.stream.IntStream;
import org.junit.jupiter.api.Test;
import org.springframework.core.style.DefaultValueStyler;
import org.springframework.core.style.ValueStyler;

class PageDtoTest {

    private static final ValueStyler STYLER = new DefaultValueStyler();

    @Test
    void createInvalid() {
        assertThatRequireException().isThrownBy(() -> new PageDto<>(null));
    }

    @Test
    void withInvalid() {
        assertThatRequireException().isThrownBy(() -> PageDto.with(null));

        final var items = List.of(1, 3, 1, 2);
        assertThatRequireException().isThrownBy(() -> PageDto.with(42L, -1, 1, items));
        assertThatRequireException().isThrownBy(() -> PageDto.with(42L, 1, 0, items));
        assertThatRequireException().isThrownBy(() -> PageDto.with(42L, 1, 1, null));
    }

    @Test
    void with() {
        final var pages = 4;
        final var size = 8;
        final var total = size * pages;
        final var number = 3;

        final var items = IntStream.rangeClosed(1, total)
                .boxed()
                .skip(size * number)
                .limit(size)
                .toList();

        final var dto = PageDto.with((long) total, number, size, items);

        assertThat(dto.getPages()).isEqualTo(pages);
        assertThat(dto.getTotal()).isEqualTo(total);
        assertThat(dto.getNumber()).isEqualTo(number);
        assertThat(dto.getSize()).isEqualTo(size);
        assertThat(dto.isFirst()).isFalse();
        assertThat(dto.isLast()).isTrue();
        assertThat(dto.hasNext()).isFalse();
        assertThat(dto.hasPrevious()).isTrue();
        assertThat(dto.getItems()).isEqualTo(items);

        assertThat(dto.toString())
                .isNotBlank()
                .contains(STYLER.style(pages))
                .contains(STYLER.style(total))
                .contains(STYLER.style(number))
                .contains(STYLER.style(size))
                .contains(STYLER.style(true))
                .contains(STYLER.style(false))
                .contains(STYLER.style(items));
    }
}
