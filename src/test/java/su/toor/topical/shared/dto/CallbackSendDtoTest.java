package su.toor.topical.shared.dto;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatNoException;
import static su.toor.topical.shared.misc.RequireAssert.assertThatRequireException;

import java.util.UUID;
import org.junit.jupiter.api.Test;

class CallbackSendDtoTest {

    private static final String TOPIC = "some/topic";
    private static final UUID UU_ID = UUID.randomUUID();
    private static final PayloadDto PAYLOAD = new PayloadDto("===", null, null, null, null, null);

    @Test
    void invalid() {
        assertThatRequireException().isThrownBy(() -> new CallbackSendDto(null, UU_ID, PAYLOAD));
        assertThatRequireException().isThrownBy(() -> new CallbackSendDto("", UU_ID, PAYLOAD));
        assertThatRequireException().isThrownBy(() -> new CallbackSendDto(TOPIC, null, PAYLOAD));
        assertThatRequireException().isThrownBy(() -> new CallbackSendDto(TOPIC, UU_ID, null));
        assertThatNoException().isThrownBy(() -> new CallbackSendDto(TOPIC, UU_ID, PAYLOAD));
    }

    @Test
    void fields() {
        final var dto = new CallbackSendDto(TOPIC, UU_ID, PAYLOAD);

        assertThat(dto.topic()).isEqualTo(TOPIC);
        assertThat(dto.uuid()).isEqualTo(UU_ID);
        assertThat(dto.payload()).isEqualTo(PAYLOAD);

        assertThat(dto.toString())
                .isNotBlank()
                .contains(TOPIC)
                .contains(String.valueOf(UU_ID))
                .contains(String.valueOf(PAYLOAD));
    }
}
