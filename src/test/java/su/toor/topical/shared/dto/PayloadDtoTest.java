package su.toor.topical.shared.dto;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatNoException;
import static su.toor.topical.shared.misc.RequireAssert.assertThatRequireException;

import org.junit.jupiter.api.Test;

class PayloadDtoTest {

    private static final String BASE_64 = "===";
    private static final String STRING = "twenty-three-dot-forty-two";
    private static final Object JSON = new Object();
    private static final Integer NUMBER = 23;
    private static final Double FRACTION = 23.42d;
    private static final Boolean BOOL = false;

    @Test
    void invalid() {
        assertThatRequireException().isThrownBy(() -> new PayloadDto(null, STRING, JSON, NUMBER, FRACTION, BOOL));
        assertThatNoException().isThrownBy(() -> new PayloadDto(BASE_64, null, null, null, null, null));
    }

    @Test
    void fields() {
        final var dto = new PayloadDto(BASE_64, STRING, JSON, NUMBER, FRACTION, BOOL);

        assertThat(dto.base64()).isEqualTo(BASE_64);
        assertThat(dto.string()).isEqualTo(STRING);
        assertThat(dto.json()).isEqualTo(JSON);
        assertThat(dto.number()).isEqualTo(NUMBER);
        assertThat(dto.fraction()).isEqualTo(FRACTION);
        assertThat(dto.bool()).isEqualTo(BOOL);

        assertThat(dto.toString())
                .isNotBlank()
                .contains(BASE_64)
                .contains(STRING)
                .contains(String.valueOf(JSON))
                .contains(String.valueOf(NUMBER))
                .contains(String.valueOf(FRACTION))
                .contains(String.valueOf(BOOL));
    }
}
