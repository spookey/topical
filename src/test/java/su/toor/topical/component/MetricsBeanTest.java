package su.toor.topical.component;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatNoException;
import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import io.micrometer.core.instrument.Gauge;
import io.micrometer.core.instrument.simple.SimpleMeterRegistry;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.bean.override.mockito.MockitoBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import su.toor.topical.service.EntityService;

@ExtendWith(SpringExtension.class)
class MetricsBeanTest {

    @MockitoBean
    private CounterBean counterBean;

    @MockitoBean
    private EntityService entityService;

    @Test
    void fromCounters() {
        final var registry = new SimpleMeterRegistry();
        final var metrics = new MetricsBean(registry, counterBean, entityService);

        assertThat(registry.getMeters()).isEmpty();
        metrics.fromCounters();

        assertThat(registry.getMeters()).hasSize(6);

        registry.forEachMeter(meter -> {
            assertThat(meter).isInstanceOf(Gauge.class);

            final var id = meter.getId();
            assertThat(id.getName()).isEqualTo(MetricsBean.METER_COUNT_NAME);
            assertThat(id.getDescription()).isEqualTo(MetricsBean.METER_COUNT_DESCRIPTION);

            final var categoryTag = id.getTag(MetricsBean.METER_TAG_CATEGORY);
            assertThat(categoryTag)
                    .isNotNull()
                    .isIn(MetricsBean.METER_CATEGORY_CONNECTION, MetricsBean.METER_CATEGORY_DELIVERY);
            final var typeTag = id.getTag(MetricsBean.METER_TAG_TYPE);
            assertThat(typeTag).isNotNull().isIn("connects", "disconnects", "errors", "sent", "collected", "purged");
        });

        verifyNoInteractions(entityService);
    }

    @Test
    void fromDatabase() {
        final var registry = new SimpleMeterRegistry();
        final var metrics = new MetricsBean(registry, counterBean, entityService);

        assertThat(registry.getMeters()).isEmpty();
        metrics.fromDatabase();

        assertThat(registry.getMeters()).hasSize(4);

        registry.forEachMeter(meter -> {
            assertThat(meter).isInstanceOf(Gauge.class);

            final var id = meter.getId();
            assertThat(id.getName()).isEqualTo(MetricsBean.METER_STORE_NAME);
            assertThat(id.getDescription()).isEqualTo(MetricsBean.METER_STORE_DESCRIPTION);

            final var categoryTag = id.getTag(MetricsBean.METER_TAG_CATEGORY);
            assertThat(categoryTag).isNotNull().isEqualTo(MetricsBean.METER_CATEGORY_DATABASE);

            final var typeTag = id.getTag(MetricsBean.METER_TAG_TYPE);
            assertThat(typeTag).isNotNull().isIn("topics", "messages", "listeners", "callbacks");
        });

        verifyNoInteractions(counterBean);
    }

    @Test
    void registerMetrics() {
        final var metrics = mock(MetricsBean.class);
        doCallRealMethod().when(metrics).registerMetrics();

        assertThatNoException().isThrownBy(metrics::registerMetrics);

        verify(metrics, times(1)).registerMetrics();
        verify(metrics, times(1)).fromCounters();
        verify(metrics, times(1)).fromDatabase();
        verifyNoMoreInteractions(metrics);
    }
}
