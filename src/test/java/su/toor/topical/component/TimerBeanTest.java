package su.toor.topical.component;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatNoException;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.json.JsonMapper;
import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.time.Duration;
import java.time.OffsetDateTime;
import java.util.Set;
import java.util.UUID;
import org.eclipse.paho.mqttv5.common.MqttMessage;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.bean.override.mockito.MockitoBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import su.toor.topical.application.config.JsonMapperConfiguration;
import su.toor.topical.application.setting.BasicSetting;
import su.toor.topical.application.setting.mqtt.ReportStatusSetting;
import su.toor.topical.database.MessageEntity;
import su.toor.topical.database.TopicEntity;
import su.toor.topical.service.ClientService;
import su.toor.topical.service.EntityService;
import su.toor.topical.shared.dto.ReportDto;

@ExtendWith(SpringExtension.class)
@JsonTest
@Import(value = {JsonMapperConfiguration.class})
class TimerBeanTest {

    @MockitoBean
    private ClientService clientService;

    @MockitoBean
    private EntityService entityService;

    @MockitoBean
    private CounterBean counterBean;

    @Autowired
    private JsonMapper jsonMapper;

    private TimerBean create(final BasicSetting basicSetting, final ReportStatusSetting reportStatusSetting) {
        return new TimerBean(basicSetting, reportStatusSetting, clientService, entityService, counterBean, jsonMapper);
    }

    private TimerBean create(final BasicSetting basicSetting) {
        return create(basicSetting, new ReportStatusSetting());
    }

    private TimerBean create(final ReportStatusSetting reportStatusSetting) {
        return create(new BasicSetting(), reportStatusSetting);
    }

    @Test
    void constants() {
        assertThat(TimerBean.INTERVAL_RUN_INITIAL).isPositive();
        assertThat(TimerBean.INTERVAL_RUN_REGULAR).isPositive().isLessThanOrEqualTo(TimerBean.INTERVAL_RUN_INITIAL);
    }

    @Test
    void shouldReportDisabled() {
        final var timer = create(new ReportStatusSetting().setIntervalSeconds(-1));
        assertThat(timer.reporting.get()).isZero();
        assertThat(timer.shouldReport()).isFalse();
        assertThat(timer.reporting.get()).isZero();
    }

    @Test
    void shouldReport() {
        final var timer = create(new ReportStatusSetting().setIntervalSeconds(60));
        assertThat(timer.reporting.get()).isZero();
        assertThat(timer.shouldReport()).isTrue();
        assertThat(timer.reporting.get()).isNotZero();
        assertThat(timer.shouldReport()).isFalse();
        assertThat(timer.shouldReport()).isFalse();
    }

    @Test
    void reportJson() throws JsonProcessingException {
        final var title = "some message title";
        final var sent = 1312L;
        final var collected = 13L;
        final var purged = 12L;
        final var uptime = Duration.ofSeconds(1337L);

        when(counterBean.getSentMessages()).thenReturn(sent);
        when(counterBean.getCollectedMessages()).thenReturn(collected);
        when(counterBean.getPurgedMessages()).thenReturn(purged);

        final var runtimeMXBean = mock(RuntimeMXBean.class);
        when(runtimeMXBean.getUptime()).thenReturn(uptime.toMillis());

        final var timer = create(new ReportStatusSetting().setMessage(title));

        try (final var managementFactory = mockStatic(ManagementFactory.class)) {
            managementFactory.when(ManagementFactory::getRuntimeMXBean).thenReturn(runtimeMXBean);

            final var body = timer.reportJson();
            assertThat(body).isNotBlank();

            final var report = jsonMapper.readValue(body, ReportDto.class);

            assertThat(report.title()).isEqualTo(title);
            assertThat(report.sent()).isEqualTo(sent);
            assertThat(report.collected()).isEqualTo(collected);
            assertThat(report.purged()).isEqualTo(purged);
            assertThat(report.uptime()).isEqualTo(uptime);

            managementFactory.verify(ManagementFactory::getRuntimeMXBean, times(1));
            managementFactory.verifyNoMoreInteractions();
        }

        verify(runtimeMXBean, times(1)).getUptime();
        verifyNoMoreInteractions(runtimeMXBean);

        verify(counterBean, times(1)).getSentMessages();
        verify(counterBean, times(1)).getCollectedMessages();
        verify(counterBean, times(1)).getPurgedMessages();
        verifyNoMoreInteractions(counterBean);
    }

    @Test
    void reportJsonIssues() {
        final var title = "\t";

        final var timer = create(new ReportStatusSetting().setMessage(title));

        final var body = timer.reportJson();
        assertThat(body).isEqualTo(title);
    }

    @Test
    void sendReport() {
        final var timer = create(new ReportStatusSetting().setIntervalSeconds(90));
        timer.sendReport();
        timer.sendReport();
        timer.sendReport();

        verify(clientService, times(1)).send(anyString(), any(MqttMessage.class));
        verifyNoMoreInteractions(clientService);
    }

    @Test
    void purgeMessagesDisabled() {
        final var days = 0;

        final var timer = create(new BasicSetting().setKeepDays(days));

        assertThatNoException().isThrownBy(timer::purgeMessages);

        verifyNoInteractions(entityService);
        verifyNoInteractions(counterBean);
    }

    @Test
    void purgeMessages() {
        final var days = 23;
        final var topicEntity = TopicEntity.with("some");
        final var messageEntities = Set.of(
                MessageEntity.with(topicEntity, new byte[] {0x00}, null, null, null, null),
                MessageEntity.with(topicEntity, new byte[] {0x01}, null, null, null, null),
                MessageEntity.with(topicEntity, new byte[] {0x02}, null, null, null, null),
                MessageEntity.with(topicEntity, new byte[] {0x03}, null, null, null, null),
                MessageEntity.with(topicEntity, new byte[] {0x04}, null, null, null, null),
                MessageEntity.with(topicEntity, new byte[] {0x05}, null, null, null, null));

        when(entityService.allMessagesBefore(any(OffsetDateTime.class))).thenAnswer(invocation -> {
            final var now = OffsetDateTime.now();
            final var stamp = invocation.getArgument(0, OffsetDateTime.class);
            assertThat(stamp).isBefore(now).isStrictlyBetween(now.minusDays(days + 1), now.minusDays(days - 1));
            return messageEntities;
        });

        final var timer = create(new BasicSetting().setKeepDays(days));

        assertThatNoException().isThrownBy(timer::purgeMessages);

        verify(entityService, times(1)).allMessagesBefore(any(OffsetDateTime.class));
        verify(entityService, times(messageEntities.size())).deleteMessage(any(UUID.class));
        verifyNoMoreInteractions(entityService);

        verify(counterBean, times(messageEntities.size())).incPurgedMessages();
        verifyNoMoreInteractions(counterBean);
    }

    @Test
    void repeating() {
        final var timer = mock(TimerBean.class);

        doCallRealMethod().when(timer).repeating();

        assertThatNoException().isThrownBy(timer::repeating);

        verify(timer, times(1)).repeating();
        verify(timer, times(1)).purgeMessages();
        verify(timer, times(1)).sendReport();
        verifyNoMoreInteractions(timer);
    }
}
