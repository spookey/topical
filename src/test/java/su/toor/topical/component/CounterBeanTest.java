package su.toor.topical.component;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.function.Supplier;
import java.util.stream.Stream;
import org.junit.jupiter.api.Test;

class CounterBeanTest {

    @Test
    void initial() {
        final var counter = new CounterBean();

        Stream.of(
                        counter.getMqttConnects(),
                        counter.getMqttDisconnects(),
                        counter.getMqttErrors(),
                        counter.getSentMessages(),
                        counter.getCollectedMessages(),
                        counter.getPurgedMessages())
                .forEach(num -> assertThat(num).isZero());
    }

    private static void verifyIncrease(final Supplier<Long> get, final Supplier<Long> inc) {
        assertThat(get.get()).isZero();
        assertThat(get.get()).isZero();

        assertThat(inc.get()).isOne();
        assertThat(get.get()).isOne();
        assertThat(get.get()).isOne();

        assertThat(inc.get()).isEqualTo(2L);
        assertThat(get.get()).isEqualTo(2L);
        assertThat(get.get()).isEqualTo(2L);
    }

    @Test
    void increase() {
        final var counter = new CounterBean();

        verifyIncrease(counter::getMqttConnects, counter::incMqttConnects);
        verifyIncrease(counter::getMqttDisconnects, counter::incMqttDisconnects);
        verifyIncrease(counter::getMqttErrors, counter::incMqttErrors);
        verifyIncrease(counter::getSentMessages, counter::incSentMessages);
        verifyIncrease(counter::getCollectedMessages, counter::incCollectedMessages);
        verifyIncrease(counter::getPurgedMessages, counter::incPurgedMessages);
    }
}
