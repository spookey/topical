package su.toor.topical.component.mqtt;

import static org.assertj.core.api.Assertions.assertThatNoException;
import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import org.eclipse.paho.mqttv5.client.MqttDisconnectResponse;
import org.eclipse.paho.mqttv5.client.MqttToken;
import org.eclipse.paho.mqttv5.common.MqttException;
import org.eclipse.paho.mqttv5.common.MqttMessage;
import org.eclipse.paho.mqttv5.common.packet.MqttProperties;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.bean.override.mockito.MockitoBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import su.toor.topical.component.CounterBean;
import su.toor.topical.database.MessageEntity;
import su.toor.topical.database.TopicEntity;
import su.toor.topical.service.CallbackService;
import su.toor.topical.service.EntityService;
import su.toor.topical.shared.Qos;

@ExtendWith(SpringExtension.class)
class RunnerCallbackTest {

    private static final String SERVER_URI = "localhost";
    private static final MqttProperties PROPERTIES = new MqttProperties();
    private static final MqttException EXCEPTION = new MqttException(new RuntimeException("test"));
    private static final MqttDisconnectResponse DISCONNECT_RESPONSE = new MqttDisconnectResponse(EXCEPTION);
    private static final MqttToken TOKEN = new MqttToken();
    private static final String TOPIC = "some/topic";
    private static final byte[] PAYLOAD = new byte[] {0x01, 0x03, 0x01, 0x02};
    private static final Qos QOS = Qos.TWO;
    private static final boolean RETAINED = true;
    private static final boolean DUPLICATE = true;
    private static final int MESSAGE_ID = 1312;
    private static final MqttMessage MESSAGE = new MqttMessage(PAYLOAD, QOS.ordinal(), RETAINED, PROPERTIES);

    static {
        MESSAGE.setDuplicate(DUPLICATE);
    }

    static {
        MESSAGE.setId(MESSAGE_ID);
    }

    @MockitoBean
    private CounterBean counterBean;

    @MockitoBean
    private EntityService entityService;

    @MockitoBean
    private CallbackService callbackService;

    @Test
    void collectMessage() {
        final var topicEntity = TopicEntity.with(TOPIC);
        final var messageEntity = MessageEntity.with(topicEntity, PAYLOAD, QOS, RETAINED, DUPLICATE, MESSAGE_ID);

        when(entityService.createMessage(TOPIC, PAYLOAD, QOS, RETAINED, DUPLICATE, MESSAGE_ID))
                .thenReturn(messageEntity);

        final var callback = new RunnerCallback(counterBean, entityService, callbackService);
        final var future = callback.collectMessage(TOPIC, MESSAGE);

        assertThatNoException().isThrownBy(future::get);

        verify(entityService, times(1)).createMessage(TOPIC, PAYLOAD, QOS, RETAINED, DUPLICATE, MESSAGE_ID);
        verifyNoMoreInteractions(entityService);

        verify(callbackService, times(1)).emit(messageEntity);
        verifyNoMoreInteractions(callbackService);

        verify(counterBean, times(1)).incCollectedMessages();
        verifyNoMoreInteractions(counterBean);
    }

    @Test
    void messageArrived() {
        final var callback = mock(RunnerCallback.class);
        doCallRealMethod().when(callback).messageArrived(TOPIC, MESSAGE);

        assertThatNoException().isThrownBy(() -> callback.messageArrived(TOPIC, MESSAGE));

        verify(callback, times(1)).messageArrived(TOPIC, MESSAGE);
        verify(callback, times(1)).collectMessage(TOPIC, MESSAGE);
    }

    @Test
    void deliveryComplete() {
        final var callback = new RunnerCallback(counterBean, entityService, callbackService);

        assertThatNoException().isThrownBy(() -> callback.deliveryComplete(TOKEN));

        verify(counterBean, times(1)).incSentMessages();
        verifyNoMoreInteractions(counterBean);
    }

    @Test
    void connectComplete() {
        final var callback = new RunnerCallback(counterBean, entityService, callbackService);

        assertThatNoException().isThrownBy(() -> callback.connectComplete(false, SERVER_URI));

        verify(counterBean, times(1)).incMqttConnects();
        verifyNoMoreInteractions(counterBean);
    }

    @Test
    void authPacketArrived() {
        final var callback = new RunnerCallback(counterBean, entityService, callbackService);

        assertThatNoException().isThrownBy(() -> callback.authPacketArrived(23, PROPERTIES));

        verifyNoInteractions(counterBean);
    }

    @Test
    void disconnected() {
        final var callback = new RunnerCallback(counterBean, entityService, callbackService);

        assertThatNoException().isThrownBy(() -> callback.disconnected(DISCONNECT_RESPONSE));

        verify(counterBean, times(1)).incMqttDisconnects();
        verifyNoMoreInteractions(counterBean);
    }

    @Test
    void mqttErrorOccurred() {
        final var callback = new RunnerCallback(counterBean, entityService, callbackService);

        assertThatNoException().isThrownBy(() -> callback.mqttErrorOccurred(EXCEPTION));

        verify(counterBean, times(1)).incMqttErrors();
        verifyNoMoreInteractions(counterBean);
    }
}
