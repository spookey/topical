package su.toor.topical.component.mqtt;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatNoException;
import static su.toor.topical.shared.misc.RequireAssert.assertThatRequireException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.json.JsonMapper;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.stream.IntStream;
import org.junit.jupiter.api.Test;
import su.toor.topical.shared.Qos;

class PayloadParserTest {

    private static final JsonMapper JSON_MAPPER = new JsonMapper();

    private static final byte[] EMPTY = new byte[] {};

    private static byte[] asBytes(final String content) {
        return content.getBytes(StandardCharsets.UTF_8);
    }

    private static byte[] asBytes(final Integer number) {
        return MessageComposer.numeric(Integer.BYTES, buf -> buf.putInt(number));
    }

    private static byte[] asBytes(final Double number) {
        return MessageComposer.numeric(Double.BYTES, buf -> buf.putDouble(number));
    }

    private PayloadParser.Parser create(final byte[] payload) {
        return new PayloadParser.Parser(JSON_MAPPER, payload);
    }

    private PayloadParser.Parser create(final String content) {
        return create(asBytes(content));
    }

    private PayloadParser.Parser create(final Integer number) {
        return create(asBytes(number));
    }

    private PayloadParser.Parser create(final Double number) {
        return create(asBytes(number));
    }

    private PayloadParser.Parser createEmpty() {
        return create(EMPTY);
    }

    private PayloadParser.Parser createInvalidText() {
        return create(new byte[] {-1});
    }

    @Test
    void parserCreateNull() {
        assertThatRequireException().isThrownBy(() -> new PayloadParser.Parser(JSON_MAPPER, null));
        assertThatNoException().isThrownBy(() -> new PayloadParser.Parser(JSON_MAPPER, EMPTY));
    }

    @Test
    void parserAsBase64Simple() {
        final var emptyParser = createEmpty();
        assertThat(emptyParser.asBase64()).isNotNull().isEmpty();

        final var nilParser = create(new byte[] {0x00});
        assertThat(nilParser.asBase64()).isEqualTo("AA==");

        final var oneParser = create(new byte[] {0x01});
        assertThat(oneParser.asBase64()).isEqualTo("AQ==");

        final var twoParser = create(new byte[] {0x02});
        assertThat(twoParser.asBase64()).isEqualTo("Ag==");
    }

    @Test
    void parserAsBase64() {
        final var size = 2048;
        final var payload = new byte[size];
        IntStream.range(0, size).boxed().forEach(num -> payload[num] = num.byteValue());

        final var parser = create(payload);
        final var base64 = parser.asBase64();
        assertThat(base64).isNotBlank();

        final var decoded = Base64.getDecoder().decode(base64);
        assertThat(decoded).isEqualTo(payload);
    }

    @Test
    void parserAsString() {
        final var invalidParser = createInvalidText();
        assertThat(invalidParser.asString()).isNull();

        final var text = "some text";
        final var textParser = create(text);
        assertThat(textParser.asString()).isEqualTo(text);

        final var emoji = "🥔";
        final var emojiParser = create(emoji);
        assertThat(emojiParser.asString()).isEqualTo(emoji);
    }

    @Test
    void parserAsJsonSimple() throws JsonProcessingException {
        final var invalidParser = createInvalidText();
        assertThat(invalidParser.asJson()).isNull();

        final var emptyParser = createEmpty();
        assertThat(emptyParser.asJson()).isNull();

        final var brokenParser = create("{");
        assertThat(brokenParser.asJson()).isNull();

        final var nullParser = create(JSON_MAPPER.writeValueAsString(null));
        assertThat(nullParser.asJson()).isNull();

        final var text = "text";
        final var textParser = create(JSON_MAPPER.writeValueAsString(text));
        assertThat(textParser.asJson()).isEqualTo(text);

        final var number = 23;
        final var numberParser = create(JSON_MAPPER.writeValueAsString(number));
        assertThat(numberParser.asJson()).isEqualTo(number);
    }

    @Test
    void parserAsJson() throws JsonProcessingException {
        final var size = 1024;
        final var collection = IntStream.range(0, size).boxed().toList();

        final String content = JSON_MAPPER.writeValueAsString(collection);

        final var parser = create(content);
        assertThat(parser.asJson()).isEqualTo(collection);
    }

    @Test
    void parserAsNumber() {
        final var emptyParser = createEmpty();
        assertThat(emptyParser.asNumber()).isNull();

        final var longTextParser = create("very long text extending byte size");
        assertThat(longTextParser.asNumber()).isNull();

        final var shortTextParser = create("text");
        assertThat(shortTextParser.asNumber()).isNotNull().isNotZero();

        final var number = 23;
        final var numberParser = create(number);
        assertThat(numberParser.asNumber()).isEqualTo(number);

        final var fraction = 13.37d;
        final var fractionParser = create(fraction);
        assertThat(fractionParser.asNumber()).isNull();
    }

    @Test
    void parserAsFraction() {
        final var emptyParser = createEmpty();
        assertThat(emptyParser.asFraction()).isNull();

        final var longTextParser = create("very long text extending byte size");
        assertThat(longTextParser.asFraction()).isNull();

        final var shortTextParser = create("text");
        assertThat(shortTextParser.asFraction()).isNotNull().isNotZero();

        final var number = 42;
        final var numberParser = create(number);
        assertThat(numberParser.asFraction()).isNotNull().isNotZero();

        final var fraction = 13.12d;
        final var fractionParser = create(fraction);
        assertThat(fractionParser.asFraction()).isEqualTo(fraction);
    }

    @Test
    void parserAsBool() {
        final var emptyParser = createEmpty();
        assertThat(emptyParser.asBool()).isNull();

        final var invalidParser = create("text");
        assertThat(invalidParser.asBool()).isFalse();

        final var trueParser = create("TrUe");
        assertThat(trueParser.asBool()).isTrue();

        final var falseParser = create("fAlSe");
        assertThat(falseParser.asBool()).isFalse();

        final var zeroParser = create(0);
        assertThat(zeroParser.asBool()).isTrue();

        final var oneParser = create(1);
        assertThat(oneParser.asBool()).isFalse();
    }

    @Test
    void parse() {
        final var payloadParser = new PayloadParser(JSON_MAPPER);
        final var dto = payloadParser.parse(EMPTY);

        assertThat(dto.base64()).isNotNull().isEmpty();
        assertThat(dto.string()).isNotNull().isEmpty();
        assertThat(dto.json()).isNull();
        assertThat(dto.number()).isNull();
        assertThat(dto.fraction()).isNull();
        assertThat(dto.bool()).isNull();
    }

    @Test
    void numberFrom() {
        assertThat(PayloadParser.numberFrom(null)).isNull();
        assertThat(PayloadParser.numberFrom("")).isNull();
        assertThat(PayloadParser.numberFrom(" ")).isNull();
        assertThat(PayloadParser.numberFrom("text")).isNull();
        assertThat(PayloadParser.numberFrom("23")).isEqualTo(23);
        assertThat(PayloadParser.numberFrom("23.42")).isNull();
    }

    @Test
    void fractionFrom() {
        assertThat(PayloadParser.fractionFrom(null)).isNull();
        assertThat(PayloadParser.fractionFrom("")).isNull();
        assertThat(PayloadParser.fractionFrom(" ")).isNull();
        assertThat(PayloadParser.fractionFrom("text")).isNull();
        assertThat(PayloadParser.fractionFrom("23")).isEqualTo(23d);
        assertThat(PayloadParser.fractionFrom("23.42")).isEqualTo(23.42d);
    }

    @Test
    void compose() {
        final var payloadParser = new PayloadParser(JSON_MAPPER);

        final var qos = Qos.TWO;
        final var retained = true;

        final var string = "some text";
        final var stringMessage = payloadParser.compose(string, qos, retained);
        assertThat(stringMessage.getPayload()).isEqualTo(asBytes(string));
        assertThat(stringMessage.getQos()).isEqualTo(qos.ordinal());
        assertThat(stringMessage.isRetained()).isEqualTo(retained);

        final var fraction = 23.42d;
        final var fractionMessage = payloadParser.compose(Double.toString(fraction), qos, retained);
        assertThat(fractionMessage.getPayload()).isEqualTo(asBytes(fraction));
        assertThat(fractionMessage.getQos()).isEqualTo(qos.ordinal());
        assertThat(fractionMessage.isRetained()).isEqualTo(retained);

        final var number = 42;
        final var numberMessage = payloadParser.compose(Integer.toString(number), qos, retained);
        assertThat(numberMessage.getPayload()).isEqualTo(asBytes(number));
        assertThat(numberMessage.getQos()).isEqualTo(qos.ordinal());
        assertThat(numberMessage.isRetained()).isEqualTo(retained);
    }
}
