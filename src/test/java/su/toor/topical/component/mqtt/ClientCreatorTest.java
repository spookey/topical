package su.toor.topical.component.mqtt;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.nio.charset.StandardCharsets;
import org.eclipse.paho.mqttv5.client.MqttAsyncClient;
import org.eclipse.paho.mqttv5.client.MqttConnectionOptions;
import org.eclipse.paho.mqttv5.client.MqttToken;
import org.eclipse.paho.mqttv5.common.MqttException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.BeanInstantiationException;
import su.toor.topical.application.setting.MqttSetting;
import su.toor.topical.application.setting.mqtt.WillStatusSetting;

class ClientCreatorTest {

    @Test
    void options() {
        final var cleanStart = true;
        final var autoReconnect = false;
        final var username = "user-name";
        final var password = "pass-word";

        final var willTopic = "will/topic";
        final var willMessage = "Will message";
        final var willStatus =
                new WillStatusSetting().setEnabled(true).setTopic(willTopic).setMessage(willMessage);

        final var setting = new MqttSetting()
                .setCleanStart(cleanStart)
                .setAutoReconnect(autoReconnect)
                .setUsername(username)
                .setPassword(password);

        final var creator = new ClientCreator();
        final var options = creator.mqttConnectionOptions(setting, willStatus);

        assertThat(options.isCleanStart()).isEqualTo(cleanStart);
        assertThat(options.isAutomaticReconnect()).isEqualTo(autoReconnect);
        assertThat(options.useSubscriptionIdentifiers()).isTrue();
        assertThat(options.getRequestResponseInfo()).isTrue();
        assertThat(options.getRequestProblemInfo()).isTrue();

        assertThat(options.getUserName()).isEqualTo(username);
        assertThat(options.getPassword()).isEqualTo(password.getBytes(StandardCharsets.UTF_8));

        assertThat(options.getWillDestination()).isEqualTo(willTopic);
        final var will = options.getWillMessage();
        assertThat(will.getPayload()).isEqualTo(willMessage.getBytes(StandardCharsets.UTF_8));
        assertThat(will.getQos()).isEqualTo(willStatus.getQos().ordinal());
        assertThat(will.isRetained()).isEqualTo(willStatus.isRetained());
    }

    @Test
    void optionsEmpty() {
        final var willStatus = new WillStatusSetting().setEnabled(false);

        final var setting = new MqttSetting();

        final var creator = new ClientCreator();
        final var options = creator.mqttConnectionOptions(setting, willStatus);

        assertThat(options.isCleanStart()).isEqualTo(setting.isCleanStart());
        assertThat(options.isAutomaticReconnect()).isEqualTo(setting.isAutoReconnect());
        assertThat(options.useSubscriptionIdentifiers()).isTrue();
        assertThat(options.getRequestResponseInfo()).isTrue();
        assertThat(options.getRequestProblemInfo()).isTrue();

        assertThat(options.getUserName()).isNull();
        assertThat(options.getPassword()).isNull();

        assertThat(options.getWillDestination()).isNull();
    }

    @Test
    void basicClientBroken() {
        final var setting = new MqttSetting().setServerUri("gopher://localhost:1312");

        final var creator = new ClientCreator();

        assertThatExceptionOfType(BeanInstantiationException.class)
                .isThrownBy(() -> creator.basicMqttAsyncClient(setting, null))
                .withMessageContaining("creation failed");
    }

    @Test
    void basicClient() {
        final var serverUri = "tcp://localhost:1312";
        final var clientId = "test-client";

        final var setting = new MqttSetting().setServerUri(serverUri).setClientId(clientId);

        final var runnerCallback = mock(RunnerCallback.class);

        final var creator = new ClientCreator();
        final var client = creator.basicMqttAsyncClient(setting, runnerCallback);

        assertThat(client.isConnected()).isFalse();
        assertThat(client.getServerURI()).isEqualTo(serverUri);
        assertThat(client.getClientId()).isEqualTo(clientId);

        verifyNoInteractions(runnerCallback);
    }

    @Test
    void clientIssues() throws MqttException {
        final var mqttClient = mock(MqttAsyncClient.class);
        final var mqttOptions = mock(MqttConnectionOptions.class);

        final var creator = new ClientCreator();

        when(mqttClient.connect(mqttOptions)).thenThrow(new MqttException(new RuntimeException("error")));

        assertThatExceptionOfType(BeanInstantiationException.class)
                .isThrownBy(() -> creator.mqttAsyncClient(mqttClient, mqttOptions))
                .withMessageContaining("connection failed");

        verify(mqttClient, times(1)).getServerURI();
        verify(mqttClient, times(1)).connect(mqttOptions);
        verifyNoMoreInteractions(mqttClient);

        verifyNoInteractions(mqttOptions);
    }

    @Test
    void client() throws MqttException {
        final var mqttClient = mock(MqttAsyncClient.class);
        final var mqttOptions = mock(MqttConnectionOptions.class);
        final var token = mock(MqttToken.class);

        when(mqttClient.connect(mqttOptions)).thenReturn(token);

        final var creator = new ClientCreator();

        final var client = creator.mqttAsyncClient(mqttClient, mqttOptions);
        assertThat(client).isSameAs(mqttClient);

        verify(token, times(1)).waitForCompletion();
        verifyNoMoreInteractions(token);

        verify(mqttClient, times(1)).getServerURI();
        verify(mqttClient, times(1)).connect(mqttOptions);
        verifyNoMoreInteractions(mqttClient);

        verifyNoInteractions(mqttOptions);
    }
}
