package su.toor.topical.component.mqtt;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatIllegalArgumentException;
import static org.assertj.core.api.Assertions.fail;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.function.Consumer;
import java.util.function.Function;
import org.eclipse.paho.mqttv5.common.MqttMessage;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;
import su.toor.topical.application.setting.mqtt.AbstractStatus;
import su.toor.topical.application.setting.mqtt.ReportStatusSetting;
import su.toor.topical.application.setting.mqtt.StatusSettingsArgumentsProvider;
import su.toor.topical.shared.Qos;

class MessageComposerTest {

    private static String nullMessage() {
        return null;
    }

    private static Function<String, byte[]> nullConvert() {
        return nil -> fail("should not be called");
    }

    private static void verifyMessage(
            final MqttMessage message, final Qos qos, final boolean retained, final Consumer<byte[]> check) {
        assertThat(message).isNotNull();
        assertThat(message.getProperties()).isNull();
        assertThat(message.getQos()).isEqualTo(qos.ordinal());
        assertThat(message.isRetained()).isEqualTo(retained);

        check.accept(message.getPayload());
    }

    @Test
    void nullContent() {
        final var nullMessage = nullMessage();
        final var nullConvert = nullConvert();

        assertThatIllegalArgumentException()
                .isThrownBy(() -> MessageComposer.make(nullMessage, null, null, nullConvert));
    }

    @Test
    void nullMetadata() {
        final var raw = new byte[] {0x01, 0x03, 0x01, 0x02};

        final var message = MessageComposer.make(raw, null, null, pay -> pay);

        verifyMessage(
                message, MessageComposer.DEFAULT_QOS, MessageComposer.DEFAULT_RETAINED, payload -> assertThat(payload)
                        .isEqualTo(raw));
    }

    @Test
    void string() {
        final var text = "🤯";
        final var qos = Qos.ZERO;
        final var retained = false;

        final var message = MessageComposer.make(text, qos, retained);

        verifyMessage(message, qos, retained, payload -> assertThat(payload)
                .isEqualTo(text.getBytes(StandardCharsets.UTF_8)));
    }

    @Test
    void integer() {
        final var number = 1337;
        final var qos = Qos.TWO;
        final var retained = true;

        final var message = MessageComposer.make(number, qos, retained);

        verifyMessage(message, qos, retained, payload -> assertThat(
                        ByteBuffer.wrap(payload).asIntBuffer().get())
                .isEqualTo(number));
    }

    @Test
    void fraction() {
        final var number = 23.42d;
        final var qos = Qos.ONE;
        final var retained = false;

        final var message = MessageComposer.make(number, qos, retained);

        verifyMessage(message, qos, retained, payload -> assertThat(
                        ByteBuffer.wrap(payload).asDoubleBuffer().get())
                .isEqualTo(number));
    }

    @ParameterizedTest
    @ArgumentsSource(value = StatusSettingsArgumentsProvider.class)
    <T extends AbstractStatus<T>> void status(
            final String ignoredDefaultTopic, final String ignoredDefaultMessage, final AbstractStatus<T> setting) {

        final var content = "Some message";
        final var qos = Qos.ONE;
        final var retained = true;

        setting.setMessage(content).setQos(qos).setRetained(retained);

        final var message = MessageComposer.make(setting);
        verifyMessage(message, qos, retained, payload -> assertThat(payload)
                .isEqualTo(content.getBytes(StandardCharsets.UTF_8)));
    }

    @Test
    void statusReport() {
        final var content = "some message content";
        final var qos = Qos.ONE;
        final var retained = false;

        final var setting = new ReportStatusSetting()
                .setMessage("some different message")
                .setQos(qos)
                .setRetained(retained);
        final var message = MessageComposer.make(setting, content);

        verifyMessage(message, qos, retained, payload -> assertThat(payload)
                .isEqualTo(content.getBytes(StandardCharsets.UTF_8)));
    }
}
