package su.toor.topical.application.config;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatNoException;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.bean.override.mockito.MockitoBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import su.toor.topical.FakeBean;

@ExtendWith(SpringExtension.class)
class RestTemplateConfigurationTest {

    @MockitoBean
    private RestTemplateBuilder restTemplateBuilder;

    private static final FakeBean FAKE_BEAN = new FakeBean();
    private static final RestTemplateConfiguration CONFIGURATION = new RestTemplateConfiguration();

    @Test
    void restTemplate() {
        when(restTemplateBuilder.defaultHeader(eq(HttpHeaders.USER_AGENT), anyString()))
                .thenAnswer(invocation -> {
                    final var arg = invocation.getArgument(1, String.class);
                    assertThat(arg).isEqualTo(FakeBean.APPLICATION_NAME + " - " + FakeBean.APPLICATION_VERSION);
                    return restTemplateBuilder;
                });

        final var buildProperties = FAKE_BEAN.buildProperties();
        assertThatNoException().isThrownBy(() -> CONFIGURATION.restTemplate(buildProperties, restTemplateBuilder));

        verify(restTemplateBuilder, times(1)).defaultHeader(anyString(), anyString());
        verify(restTemplateBuilder, times(1)).build();
        verifyNoMoreInteractions(restTemplateBuilder);
    }
}
