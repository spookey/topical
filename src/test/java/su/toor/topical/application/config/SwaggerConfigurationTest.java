package su.toor.topical.application.config;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import su.toor.topical.FakeBean;

class SwaggerConfigurationTest {

    private static final FakeBean FAKE_BEAN = new FakeBean();
    private static final SwaggerConfiguration SWAGGER_CONFIGURATION = new SwaggerConfiguration();

    @Test
    void openAPI() {
        final var buildProperties = FAKE_BEAN.buildProperties();
        final var openAPI = SWAGGER_CONFIGURATION.openAPI(buildProperties);
        assertThat(openAPI).isNotNull();

        final var info = openAPI.getInfo();
        assertThat(info.getTitle()).isEqualTo(FakeBean.APPLICATION_NAME);
        assertThat(info.getVersion()).isEqualTo(FakeBean.APPLICATION_VERSION);
        assertThat(info.getDescription()).isEqualTo(FakeBean.APPLICATION_DESCRIPTION);
    }
}
