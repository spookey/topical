package su.toor.topical.application.setting;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.core.style.DefaultValueStyler;
import org.springframework.core.style.ValueStyler;

class BasicSettingTest {

    private static final ValueStyler STYLER = new DefaultValueStyler();

    private static void verifySetting(
            final BasicSetting setting,
            final Integer expectedKeepDays,
            final Integer expectedRecentHours,
            final Integer expectedPaginationSize,
            final String expectedTimeFormat) {
        assertThat(setting.getKeepDays()).isEqualTo(expectedKeepDays);
        assertThat(setting.getRecentHours()).isEqualTo(expectedRecentHours);
        assertThat(setting.getPaginationSize()).isEqualTo(expectedPaginationSize);
        assertThat(setting.getTimeFormat()).isEqualTo(expectedTimeFormat);
    }

    @Test
    void empty() {
        final var setting = new BasicSetting();

        verifySetting(
                setting,
                BasicSetting.DEFAULT_KEEP_DAYS,
                BasicSetting.DEFAULT_RECENT_HOURS,
                BasicSetting.DEFAULT_PAGINATION_SIZE,
                BasicSetting.DEFAULT_TIME_FORMAT);
    }

    @Test
    void timeFormat() {
        assertThat(new BasicSetting().getTimeFormat()).isEqualTo(BasicSetting.DEFAULT_TIME_FORMAT);
        assertThat(new BasicSetting().setTimeFormat("").getTimeFormat()).isEqualTo(BasicSetting.DEFAULT_TIME_FORMAT);
        assertThat(new BasicSetting().setTimeFormat("\t").getTimeFormat()).isEqualTo(BasicSetting.DEFAULT_TIME_FORMAT);

        final var timeFormat = "yyyy-MM-dd";
        assertThat(new BasicSetting().setTimeFormat(timeFormat).getTimeFormat()).isEqualTo(timeFormat);
    }

    @Test
    void some() {
        final var keepDays = 5;
        final var recentHours = 42;
        final var paginationSize = 23;
        final var timeFormat = "yyyy";

        final var setting = new BasicSetting()
                .setKeepDays(keepDays)
                .setRecentHours(recentHours)
                .setPaginationSize(paginationSize)
                .setTimeFormat(timeFormat);

        verifySetting(setting, keepDays, recentHours, paginationSize, timeFormat);

        assertThat(setting.toString())
                .isNotBlank()
                .contains(STYLER.style(keepDays))
                .contains(STYLER.style(recentHours))
                .contains(STYLER.style(paginationSize))
                .contains(timeFormat);
    }
}
