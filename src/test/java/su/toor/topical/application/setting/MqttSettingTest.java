package su.toor.topical.application.setting;

import static org.assertj.core.api.Assertions.assertThat;

import java.nio.charset.StandardCharsets;
import org.junit.jupiter.api.Test;
import org.springframework.core.style.DefaultValueStyler;
import org.springframework.core.style.ValueStyler;

class MqttSettingTest {

    private static final ValueStyler STYLER = new DefaultValueStyler();

    private static void verifyConnection(
            final MqttSetting setting,
            final String expectedServerUri,
            final String expectedClientId,
            final Boolean cleanStart,
            final Boolean autoReconnect) {
        final var serverUri = expectedServerUri == null ? MqttSetting.DEFAULT_SERVER_URI : expectedServerUri;
        assertThat(setting.getServerUri()).isEqualTo(serverUri);
        if (expectedClientId == null) {
            assertThat(setting.getClientId())
                    .isNotBlank()
                    .startsWith(MqttSetting.DEFAULT_CLIENT_ID_PREFIX)
                    .containsPattern("^.+-[0-9]{3}$");
        } else {
            assertThat(setting.getClientId()).isEqualTo(expectedClientId);
        }
        assertThat(setting.isCleanStart()).isEqualTo(cleanStart);
        assertThat(setting.isAutoReconnect()).isEqualTo(autoReconnect);
    }

    private static void verifyCredentials(
            final MqttSetting setting, final String expectedUsername, final String expectedPassword) {
        assertThat(setting.getUsername()).isEqualTo(expectedUsername);
        if (expectedPassword == null) {
            assertThat(setting.getPassword()).isNull();
            assertThat(setting.getPasswordBytes()).isNull();
            assertThat(setting.hasCredentials()).isFalse();
        } else {
            assertThat(setting.getPassword()).isEqualTo("*".repeat(expectedPassword.length()));
            assertThat(setting.getPasswordBytes()).isEqualTo(expectedPassword.getBytes(StandardCharsets.UTF_8));
            assertThat(setting.hasCredentials()).isEqualTo(expectedUsername != null);
        }
    }

    @Test
    void empty() {
        final var setting = new MqttSetting();

        verifyConnection(
                setting,
                MqttSetting.DEFAULT_SERVER_URI,
                null,
                MqttSetting.DEFAULT_CLEAN_START,
                MqttSetting.DEFAULT_AUTO_RECONNECT);
        verifyCredentials(setting, null, null);
    }

    @Test
    void connection() {
        final var clientId = "client";
        final var serverUri = "tcp://mqtt.example.org:1883";

        verifyConnection(
                new MqttSetting(),
                MqttSetting.DEFAULT_SERVER_URI,
                null,
                MqttSetting.DEFAULT_CLEAN_START,
                MqttSetting.DEFAULT_AUTO_RECONNECT);
        verifyConnection(
                new MqttSetting().setClientId(""),
                MqttSetting.DEFAULT_SERVER_URI,
                null,
                MqttSetting.DEFAULT_CLEAN_START,
                MqttSetting.DEFAULT_AUTO_RECONNECT);
        verifyConnection(
                new MqttSetting().setServerUri(""),
                MqttSetting.DEFAULT_SERVER_URI,
                null,
                MqttSetting.DEFAULT_CLEAN_START,
                MqttSetting.DEFAULT_AUTO_RECONNECT);
        verifyConnection(
                new MqttSetting().setServerUri("\t"),
                MqttSetting.DEFAULT_SERVER_URI,
                null,
                MqttSetting.DEFAULT_CLEAN_START,
                MqttSetting.DEFAULT_AUTO_RECONNECT);
        verifyConnection(
                new MqttSetting().setClientId(clientId),
                MqttSetting.DEFAULT_SERVER_URI,
                clientId,
                MqttSetting.DEFAULT_CLEAN_START,
                MqttSetting.DEFAULT_AUTO_RECONNECT);
        verifyConnection(
                new MqttSetting().setServerUri(serverUri),
                serverUri,
                null,
                MqttSetting.DEFAULT_CLEAN_START,
                MqttSetting.DEFAULT_AUTO_RECONNECT);
    }

    @Test
    void credentials() {
        final var username = "some-user";
        final var password = "security-1337!";

        verifyCredentials(new MqttSetting(), null, null);
        verifyCredentials(new MqttSetting().setUsername(""), null, null);
        verifyCredentials(new MqttSetting().setPassword(""), null, null);
        verifyCredentials(new MqttSetting().setUsername(username), username, null);
        verifyCredentials(new MqttSetting().setPassword(password), null, password);
        verifyCredentials(new MqttSetting().setUsername(username).setPassword(password), username, password);
    }

    @Test
    void some() {
        final var serverUri = "tcp://mqtt.example.net:1883";
        final var clientId = "test-client";
        final var cleanStart = true;
        final var autoReconnect = true;
        final var username = "user123";
        final var password = "pass456";

        final var setting = new MqttSetting()
                .setServerUri(serverUri)
                .setClientId(clientId)
                .setCleanStart(cleanStart)
                .setAutoReconnect(autoReconnect)
                .setUsername(username)
                .setPassword(password);

        verifyConnection(setting, serverUri, clientId, cleanStart, autoReconnect);
        verifyCredentials(setting, username, password);

        assertThat(setting.toString())
                .isNotBlank()
                .contains(serverUri)
                .contains(clientId)
                .contains(STYLER.style(cleanStart))
                .contains(STYLER.style(autoReconnect))
                .contains(username)
                .contains("*".repeat(password.length()));
    }
}
