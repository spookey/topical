package su.toor.topical.application.setting.mqtt;

import java.util.stream.Stream;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;

public class StatusSettingsArgumentsProvider implements ArgumentsProvider {

    @Override
    public Stream<? extends Arguments> provideArguments(ExtensionContext context) {
        return Stream.of(
                Arguments.of(
                        AbstractStatus.DEFAULT_STATUS_TOPIC,
                        AbstractStatus.DEFAULT_BIRTH_MESSAGE,
                        new BirthStatusSetting()),
                Arguments.of(
                        AbstractStatus.DEFAULT_REPORT_TOPIC,
                        AbstractStatus.DEFAULT_REPORT_MESSAGE,
                        new ReportStatusSetting()),
                Arguments.of(
                        AbstractStatus.DEFAULT_STATUS_TOPIC,
                        AbstractStatus.DEFAULT_WILL_MESSAGE,
                        new WillStatusSetting()));
    }
}
