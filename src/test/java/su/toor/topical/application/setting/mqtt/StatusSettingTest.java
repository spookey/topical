package su.toor.topical.application.setting.mqtt;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatNoException;
import static su.toor.topical.shared.misc.RequireAssert.assertThatRequireException;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.springframework.core.style.DefaultValueStyler;
import org.springframework.core.style.ValueStyler;
import su.toor.topical.shared.Qos;

class StatusSettingTest {

    private static final ValueStyler STYLER = new DefaultValueStyler();

    private static <T extends AbstractStatus<T>> void verifySetting(
            final AbstractStatus<T> setting,
            final Boolean expectedEnabled,
            final String expectedTopic,
            final Qos expectedQos,
            final Boolean expectedRetained,
            final String expectedMessage) {
        assertThat(setting.isEnabled()).isEqualTo(expectedEnabled);
        assertThat(setting.getTopic()).isEqualTo(expectedTopic);
        assertThat(setting.getQos()).isEqualTo(expectedQos);
        assertThat(setting.isRetained()).isEqualTo(expectedRetained);
        assertThat(setting.getMessage()).isEqualTo(expectedMessage);
    }

    @ParameterizedTest
    @ArgumentsSource(value = StatusSettingsArgumentsProvider.class)
    <T extends AbstractStatus<T>> void empty(
            final String defaultTopic, final String defaultMessage, final AbstractStatus<T> setting) {

        verifySetting(
                setting,
                AbstractStatus.DEFAULT_ENABLED,
                defaultTopic,
                AbstractStatus.DEFAULT_QOS,
                AbstractStatus.DEFAULT_RETAINED,
                defaultMessage);

        verifySetting(
                setting.setMessage(""),
                AbstractStatus.DEFAULT_ENABLED,
                defaultTopic,
                AbstractStatus.DEFAULT_QOS,
                AbstractStatus.DEFAULT_RETAINED,
                defaultMessage);
    }

    @ParameterizedTest
    @ArgumentsSource(value = StatusSettingsArgumentsProvider.class)
    <T extends AbstractStatus<T>> void some(
            final String ignoredDefaultTopic, final String ignoredDefaultMessage, final AbstractStatus<T> setting) {
        final var enabled = true;
        final var topic = "some/topic";
        final var qos = Qos.ONE;
        final var retained = true;
        final var message = "Some message";

        setting.setEnabled(enabled)
                .setTopic(topic)
                .setQos(qos)
                .setRetained(retained)
                .setMessage(message);

        verifySetting(setting, enabled, topic, qos, retained, message);

        assertThat(setting.toString())
                .isNotBlank()
                .contains(STYLER.style(enabled))
                .contains(topic)
                .contains(STYLER.style(qos))
                .contains(STYLER.style(retained))
                .contains(message);
    }

    @ParameterizedTest
    @ArgumentsSource(value = StatusSettingsArgumentsProvider.class)
    <T extends AbstractStatus<T>> void topics(
            final String ignoredDefaultTopic, final String ignoredDefaultMessage, final AbstractStatus<T> setting) {
        final var topic = "some/topic";

        assertThatNoException().isThrownBy(() -> setting.setTopic(null));
        assertThat(setting.getTopic()).isNotNull();

        assertThatNoException().isThrownBy(() -> setting.setTopic(topic));
        assertThat(setting.getTopic()).isEqualTo(topic);

        assertThatRequireException().isThrownBy(() -> setting.setTopic("c++"));
        assertThatRequireException().isThrownBy(() -> setting.setTopic("c#"));
    }

    @Test
    void report() {
        final var intervalSeconds = 42;
        final var empty = new ReportStatusSetting().setIntervalSeconds(null);
        final var some = new ReportStatusSetting().setIntervalSeconds(intervalSeconds);

        assertThat(empty.getIntervalSeconds()).isEqualTo(ReportStatusSetting.DEFAULT_INTERVAL_SECONDS);
        assertThat(some.getIntervalSeconds()).isEqualTo(intervalSeconds);

        assertThat(some.toString()).isNotBlank().contains(STYLER.style(intervalSeconds));
    }

    private static class DefaultNullValuesSink extends AbstractStatus<DefaultNullValuesSink> {
        DefaultNullValuesSink(final String defaultTopic, final String defaultMessage) {
            super(defaultTopic, defaultMessage);
        }

        @Override
        protected DefaultNullValuesSink get() {
            return this;
        }
    }

    @ParameterizedTest
    @NullAndEmptySource
    void defaultNullValues(final String issue) {
        assertThatNoException().isThrownBy(() -> new DefaultNullValuesSink("text", "text"));
        assertThatRequireException().isThrownBy(() -> new DefaultNullValuesSink(issue, issue));

        assertThatRequireException().isThrownBy(() -> new DefaultNullValuesSink(issue, "text"));
        assertThatRequireException().isThrownBy(() -> new DefaultNullValuesSink("text", issue));
    }
}
