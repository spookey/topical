<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <parent>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-parent</artifactId>
    <version>3.4.3</version>
    <relativePath />
  </parent>

  <groupId>su.toor</groupId>
  <artifactId>topical</artifactId>
  <version>0.0.16-SNAPSHOT</version>

  <name>Topical</name>
  <description>MQTT listening history</description>
  <url>http://localhost</url>

  <licenses>
    <license>
      <name>MIT</name>
      <url>https://opensource.org/licenses/MIT</url>
    </license>
  </licenses>
  <developers>
    <developer />
  </developers>

  <scm>
    <developerConnection>scm:git:ssh://gitlab.com/spookey/topical.git</developerConnection>
    <tag>HEAD</tag>
    <url>https://gitlab.com/spookey/topical</url>
  </scm>

  <properties>
    <java.version>21</java.version>
    <source.encoding>UTF-8</source.encoding>
    <project.build.sourceEncoding>${source.encoding}</project.build.sourceEncoding>
    <project.reporting.outputEncoding>${source.encoding}</project.reporting.outputEncoding>

    <palantir-java-format.version>2.55.0</palantir-java-format.version>
    <require-maven.version>3.6.3</require-maven.version>

    <!-- dependencies -->
    <bulma.version>1.0.3</bulma.version>
    <ecs-logging-java.version>1.6.0</ecs-logging-java.version>
    <jsr305.version>3.0.2</jsr305.version>
    <hibernate-validator.version>8.0.2.Final</hibernate-validator.version>
    <logstash-logback-encoder.version>8.0</logstash-logback-encoder.version>
    <micrometer.version>1.14.4</micrometer.version>
    <mqttv5-client.version>1.2.5</mqttv5-client.version>
    <mysql.version>9.2.0</mysql.version>
    <springdoc.version>2.8.5</springdoc.version>
    <webjars-locator-lite.version>1.0.1</webjars-locator-lite.version>

    <!-- tests -->
    <testcontainers.version>1.20.5</testcontainers.version>

    <!-- plugins -->
    <jacoco-maven-plugin.version>0.8.12</jacoco-maven-plugin.version>
    <maven-compiler-plugin.version>3.14.0</maven-compiler-plugin.version>
    <maven-enforcer-plugin.version>3.5.0</maven-enforcer-plugin.version>
    <spotless-maven-plugin.version>2.44.3</spotless-maven-plugin.version>

    <!-- patches -->
    <hibernate.version>6.5.3.Final</hibernate.version>
  </properties>

  <dependencies>
    <!-- spring -->
    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-devtools</artifactId>
      <scope>runtime</scope>
      <optional>true</optional>
    </dependency>
    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-configuration-processor</artifactId>
      <optional>true</optional>
    </dependency>
    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-web</artifactId>
    </dependency>
    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-actuator</artifactId>
    </dependency>
    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-aop</artifactId>
    </dependency>
    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-jdbc</artifactId>
    </dependency>
    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-data-jpa</artifactId>
    </dependency>
    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-thymeleaf</artifactId>
    </dependency>

    <!-- monitoring -->
    <dependency>
      <groupId>io.micrometer</groupId>
      <artifactId>micrometer-registry-prometheus</artifactId>
      <version>${micrometer.version}</version>
    </dependency>

    <!-- logging -->
    <dependency>
      <groupId>net.logstash.logback</groupId>
      <artifactId>logstash-logback-encoder</artifactId>
      <version>${logstash-logback-encoder.version}</version>
    </dependency>
    <dependency>
      <groupId>co.elastic.logging</groupId>
      <artifactId>logback-ecs-encoder</artifactId>
      <version>${ecs-logging-java.version}</version>
    </dependency>

    <!-- documentation -->
    <dependency>
      <groupId>org.springdoc</groupId>
      <artifactId>springdoc-openapi-starter-webmvc-ui</artifactId>
      <version>${springdoc.version}</version>
    </dependency>

    <!-- utils -->
    <dependency>
      <groupId>com.google.code.findbugs</groupId>
      <artifactId>jsr305</artifactId>
      <version>${jsr305.version}</version>
      <scope>provided</scope>
    </dependency>

    <!-- database -->
    <dependency>
      <groupId>com.mysql</groupId>
      <artifactId>mysql-connector-j</artifactId>
      <version>${mysql.version}</version>
    </dependency>

    <!-- validation -->
    <dependency>
      <groupId>org.hibernate.validator</groupId>
      <artifactId>hibernate-validator</artifactId>
      <version>${hibernate-validator.version}</version>
    </dependency>

    <!-- assets -->
    <dependency>
      <groupId>org.webjars</groupId>
      <artifactId>webjars-locator-lite</artifactId>
      <version>${webjars-locator-lite.version}</version>
    </dependency>
    <dependency>
      <groupId>org.webjars.npm</groupId>
      <artifactId>bulma</artifactId>
      <version>${bulma.version}</version>
    </dependency>

    <!-- mqtt -->
    <dependency>
      <groupId>org.eclipse.paho</groupId>
      <artifactId>org.eclipse.paho.mqttv5.client</artifactId>
      <version>${mqttv5-client.version}</version>
    </dependency>

    <!-- testing -->
    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-test</artifactId>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>org.testcontainers</groupId>
      <artifactId>testcontainers</artifactId>
      <version>${testcontainers.version}</version>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>org.testcontainers</groupId>
      <artifactId>mysql</artifactId>
      <version>${testcontainers.version}</version>
      <scope>test</scope>
    </dependency>
  </dependencies>

  <build>
    <plugins>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-compiler-plugin</artifactId>
        <version>${maven-compiler-plugin.version}</version>
        <configuration>
          <source>${java.version}</source>
          <target>${java.version}</target>
          <encoding>${source.encoding}</encoding>
        </configuration>
      </plugin>

      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-enforcer-plugin</artifactId>
        <version>${maven-enforcer-plugin.version}</version>
        <executions>
          <execution>
            <id>enforce-maven-3</id>
            <goals>
              <goal>enforce</goal>
            </goals>
            <configuration>
              <rules>
                <requireMavenVersion>
                  <version>${require-maven.version}</version>
                </requireMavenVersion>
              </rules>
              <fail>true</fail>
            </configuration>
          </execution>
        </executions>
      </plugin>

      <plugin>
        <groupId>org.jacoco</groupId>
        <artifactId>jacoco-maven-plugin</artifactId>
        <version>${jacoco-maven-plugin.version}</version>
        <executions>
          <execution>
            <id>default-prepare-agent</id>
            <goals>
              <goal>prepare-agent</goal>
            </goals>
          </execution>
          <execution>
            <id>default-report</id>
            <goals>
              <goal>report</goal>
            </goals>
          </execution>
        </executions>
      </plugin>

      <plugin>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-maven-plugin</artifactId>
        <executions>
          <execution>
            <id>build-info</id>
            <goals>
              <goal>build-info</goal>
            </goals>
            <configuration>
              <additionalProperties>
                <description>${project.description}</description>
              </additionalProperties>
            </configuration>
          </execution>
        </executions>
      </plugin>

      <plugin>
        <groupId>com.diffplug.spotless</groupId>
        <artifactId>spotless-maven-plugin</artifactId>
        <version>${spotless-maven-plugin.version}</version>
        <configuration>
          <pom>
            <includes>
              <include>pom.xml</include>
              <include>src/main/resources/**/*.xml</include>
            </includes>
            <sortPom>
              <encoding>${source.encoding}</encoding>
              <expandEmptyElements>false</expandEmptyElements>
              <lineSeparator>\n</lineSeparator>
              <nrOfIndentSpace>2</nrOfIndentSpace>
              <spaceBeforeCloseEmptyElement>true</spaceBeforeCloseEmptyElement>
            </sortPom>
            <trimTrailingWhitespace />
            <endWithNewline />
          </pom>
          <java>
            <encoding>${source.encoding}</encoding>
            <includes>
              <include>src/**/*.java</include>
            </includes>
            <trimTrailingWhitespace />
            <endWithNewline />
            <toggleOffOn />
            <removeUnusedImports />
            <importOrder />
            <cleanthat />
            <palantirJavaFormat>
              <style>PALANTIR</style>
              <version>${palantir-java-format.version}</version>
              <formatJavadoc>true</formatJavadoc>
            </palantirJavaFormat>
            <formatAnnotations />
          </java>
        </configuration>
        <executions>
          <execution>
            <goals>
              <goal>apply</goal>
            </goals>
            <phase>generate-sources</phase>
          </execution>
        </executions>
      </plugin>
    </plugins>
    <sourceDirectory>src/main/java</sourceDirectory>
    <testSourceDirectory>src/test/java</testSourceDirectory>
  </build>
</project>
